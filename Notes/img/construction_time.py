# -*- coding: utf-8 -*-
"""
Created on Mon May  9 11:44:53 2016

@author: nicolas

Numerical calculations of the wavefunctions by exact diagonalization
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../../Tilings') # prepend the path to the Tiling package
""" import the other stuff """
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

import Tilings as tl
import QuantumGraph as QGraph

from timeit import default_timer as timer # speed test

from scipy.stats import linregress


"""
Geometry
"""

# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]

# start with a square 
tiling = tl.A5(square0(np.zeros(4), e[0], e[2]))

"""
speed tests
"""

def diagonalize(hamiltonian, method):
    start = timer()
    method(hamiltonian)
    end = timer()
    return end - start
        
sparse_hams = []
hams = []
sizes = []
ninfs = 7
construction_time = []
for i in range(ninfs):
    start = timer()
    tiling.it_sub(1)
    nx.to_scipy_sparse_matrix(tiling._graph, dtype=float)
    end = timer()
    construction_time.append(end - start)
    sizes.append(len(tiling._graph))
    
plt.plot(np.log(sizes), np.log(construction_time), 'o')

logS = np.log(sizes)
logT = np.log(construction_time)

slope, intercept, r_value, p_value, std_err = linregress(logS,logT)

fig, ax = plt.subplots()
# plot numerics
ax.plot(logS, logT, 'og', label=r'numerics')
# plot fit
x = np.linspace(min(logS), max(logS), num = 50)
y = intercept + slope*x
ax.plot(x, y, 'r--', label='fit, scaling = ' + str(np.around(slope,4)))

legend = ax.legend(loc='lower left', shadow=False)
plt.xlabel(r"$\log(N)$ (log number of sites)")
plt.ylabel(r"$log(C(N)) $ (log construction time)")
plt.title("Time to construct the graph and the Hamiltonian.")
plt.savefig("construction_time.png", dpi=200)
plt.close()
