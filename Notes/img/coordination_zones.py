# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 11:47:52 2016

@author: nicolas

This script plots the kth coordination zones (ie the kth neighbors) of sites on the AB tiling.
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../../Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the other stuff """
import networkx as nx
import QuantumGraph as QGraph
import AB_envs as envs
import matplotlib.pylab as plt

square = tl.A5(envs.squareCanonical)

square.it_sub(6)
graph = square._graph
o = square.o
i=5
p0 = (o(i), o(i-1)+o(i), o(i), 0)
#p1 = list(QGraph.k_neighbors(graph, p0, 7))[3]

neigh = QGraph.k_neighbors(graph, p0, 50) #| QGraph.k_neighbors(graph, p1, 15)
node_size = []
for p in graph.nodes():
    if p in neigh or p == p0:
        node_size.append(5.)
    else:
        node_size.append(0)
        
pos = nx.get_node_attributes(graph, "para")
nx.draw_networkx(graph, pos, with_labels = False, node_size = node_size, width = 0.2)
plt.axes().set_aspect('equal')
plt.savefig("plot_coordination_zone.png", dpi=200)

#QGraph.plot(graph, s=node_size, savename = "plot_coordination_zone")