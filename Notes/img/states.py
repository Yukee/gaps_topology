# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 10:41:50 2016

@author: nicolas

This file to plot the numerical ground state, and the variational one.
"""

import numpy as np
import math
import cmath
import matplotlib.pyplot as plt
import networkx as nx
""" import the Tilings package... """
import sys
sys.path.insert(0, '../../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" construct the tiling (useful for plots) """
# start with a square 
sq2 = math.sqrt(2)
# start with a square
square0 = []
# half-length of the square
r = 1.
# first half-square
C = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, -3*math.pi/4)
B = cmath.rect(r, -5*math.pi/4)
square0.append((r, (A, B, C)))
# second half-square
C = cmath.rect(r, -5*math.pi/4)
B = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, math.pi/4)
square0.append((r, (A, C, B)))
square = tl.A5(square0)

# scatter plot where pos is a list of tuples (x, y) and field is A(x,y)
def listplot(pos, field, ptsize=1.1, savename=None):
    x, y = np.array(pos).T
    color = field
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=ptsize, cmap = 'viridis')
    plt.colorbar(sc)
    plt.axes().set_aspect('equal')
#    dark = plt.cm.viridis(0)
#    plt.gca().set_axis_bgcolor(dark)
    if(savename):
        plt.savefig(savename+'.png', dpi=200)
        plt.savefig(savename+'.pdf')
    plt.close()

""" numerical ground state """

ninf = 6
wf_num = np.load('data/gs_wf_gen_'+str(ninf)+'.npy').flatten()

# construct the tiling
square.it_sub(ninf) # inflations
pg = QGraph.periodize(square._graph) # periodic tiling

# node posititions
lift, pts = square.lift_tiling(0.) # lift in 4D
proj = square.perp_proj() # projection matrix from physical to internal space
perp_space = np.array([np.dot(proj, lift[pt]) for pt in pg]) # node positions in internal space
real_space = np.array([(pt.real, pt.imag) for pt in pg]) # node positions in real space

# plots
listplot(real_space, wf_num, 1.1, "wf_num_realspace_infl_"+str(ninf)) # realspace
listplot(perp_space, wf_num, 1.1, "wf_num_perpspace_infl_"+str(ninf)) # perpspace

""" variational ground state """

# determine the local environments on the tiling
zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}
# return a dictionary pts to environment, given a graph
def pt_to_env(graph):
    # D1 and D2 are distinguished by their local environment
    ptToEnv = {}
    for pt in graph:
        n = graph.neighbors(pt)
        z = len(n)
        # if pt is a D type site, distinguish between D1 and D2
        if z == 5:
            # D2 sites have in their neighbourhood 2 F sites
            if 3 in [len(graph.neighbors(pn)) for pn in n]:
                ptToEnv[pt] = 'D2'
            else:
                ptToEnv[pt] = 'D1'
        else:
            ptToEnv[pt] = zToEnv[z]
    return ptToEnv

beta = 0.907397
coef = {'A':0.469686, 'B':0.443274, 'C':0.416863, 'D1':0.391146, 'D2':0.351229, 'E':0.286719, 'F':0.224864} # coeff for beta = 0.91

# compute the arrow potential
wg = square.integrate_arrow_field(B)
ptToPot = nx.get_node_attributes(wg,'weight')
pot = np.array([ptToPot[pt] for pt in pg])

# construct variational wavefunction
ptToEnv = pt_to_env(pg) # create a pt -> local env dictionary
wf_var = np.array([coef[ptToEnv[pt]]*beta**ptToPot[pt] for pt in pg]) # create wavefunction
norm = math.sqrt(np.sum(wf_var**2))
wf_var /= norm

# plots
listplot(real_space, wf_var, 1.1, "wf_var_realspace_infl_"+str(ninf)) # realspace
listplot(perp_space, wf_var, 1.1, "wf_var_perpspace_infl_"+str(ninf)) # perpspace
