# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 17:19:22 2016

@author: nicolas

This file to show the four nonequivalent squares of the AB tiling, their respective inflation and the link with the arrow field
"""
""" import the Tilings package... """
import sys
sys.path.insert(0, '../../Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the other stuff """
import math
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx # graph tools
import QuantumGraph as QGraph

sq2 = math.sqrt(2)
lb = sq2 - 1.
pi = math.pi
e = np.exp(1j*pi*np.arange(4)/4)

def square(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not math.isclose(np.vdot(ea, eb).real, 0, abs_tol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]

# first orientation
square0 = tl.A5(square(0,e[0],e[2]))
# second orientation
square1 = tl.A5(square(e[0],-e[0],e[2]))

def arrowed_graph(tiling, l, width):
    """
    plot the arrowed graph, after l inflations
    """
    # inflate
    tiling.it_sub(l)
    # construct and copy graph with arrows
    tiling.__construct_arrowed_graph__()
    arrG = tiling._arrowed_graph.copy()
    
    # keep only edges that are directed in the positive direction
    dictW = nx.get_edge_attributes(arrG,'arrow')
    for ed in dictW:
        if dictW[ed] == -1:
            arrG.remove_edge(*ed)
    
    # plot the arrowed graph
    QGraph.plot(arrG, weights=width)
            
    return None
    
arrowed_graph(square0, 0, 1.)
arrowed_graph(square0, 1, 0.5)
plt.savefig("square_arrows_inflation_orientation0.pdf")
plt.close()

arrowed_graph(square1, 0, 1.)
arrowed_graph(square1, 1, 0.5)
plt.savefig("square_arrows_inflation_orientation1.pdf")
plt.close()