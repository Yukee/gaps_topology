# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 11:17:22 2016

@author: nicolas

This script studies convergence of the energy and width of the main gap, for different types of approximants
"""

import matplotlib.pyplot as plt
import numpy as np

"""
load the results of the computations
(see the script "gap_convergence.py" in the phasons folder for the details)
Note that here Pavel's unit cell is the 8 fold system that has 4 times more atoms that the canonical unit cell.
"""
# number of inflations
ns = range(4,8)
# mean gap energy and width of the gap for the canonical unit cell
meansCanon = [-1.9326888638629725, -1.8936718341577778, -1.8791049353004352, -1.8752506917454201]
widthsCanon = [0.12514245768351917, 0.054920803622802383, 0.024427611624361356, 0.016934973213575955]
# same for Pavel's unit cell
meansPavel = [-1.8749030548652055, -1.8744007269169414, -1.8743706457748419, -1.8743563785319135]
widthsPavel = [0.0093774091724390107, 0.014141899761630272, 0.014937213610751066, 0.015082277149182977]

fig, ax = plt.subplots()
ax.plot(ns, meansCanon, 'ro', label=r'$\overline{E}$ for canonical unit cell')
ax.plot(ns, meansPavel, 'o', label=r"$\overline{E}$ for Pavel's unit cell")
plt.xlim([ns[0]-.2, ns[-1]+.2])
#plt.ylim([1.8, 2.])
plt.xlabel("Number of inflations")
plt.ylabel("Mean energy of the gap")
legend = ax.legend(loc='lower right', shadow=False)
plt.savefig("gap_mean_energy_scaling.pdf")
plt.show()
plt.close()

fig, ax = plt.subplots()
ax.plot(ns, widthsCanon, 'ro', label=r'Gap width for canonical unit cell')
ax.plot(ns, widthsPavel, 'o', label=r"Gap width for Pavel's unit cell")
plt.xlim([ns[0]-.2, ns[-1]+.2])
plt.xlabel("Number of inflations")
plt.ylabel("Gap width")
legend = ax.legend(loc='upper right', shadow=False)
plt.savefig("gap_width_scaling.pdf")
plt.show()