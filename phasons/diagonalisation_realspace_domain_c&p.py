# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 13:59:59 2016

@author: nicolas

Diagonalization of the one electron problem using the cut and project construction,
on an arbitrary shaped real space domain... like a heart-shaped one!
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import QuantumGraph as QGraph
import CP as CP

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

import scipy.linalg as lin


def proj_graph(graph):
    """ 
    project the graph onto the perp space
    """
    pg = g.copy()
    for p in graph.nodes():
        pp = np.dot(proj, p)
        pg.node[p]['proj'] = pp
        
    return pg
    
def plot(pgraph, lims = None):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'para')
    plt.axes().set_aspect('equal')
    
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
    
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 0)      
    plt.savefig("cp.pdf")
    plt.show()
    plt.close()
    
def plot_field(graph, field, edges, lims = None, savename = None, title = None):
    """
    Plot a field on a tiling, using 
    """
    # retrieve the positions
    pos = np.array([p[1]['para'] for p in graph.nodes(data=True)])
    x, y = np.array(pos).T
#    field = np.array([ptToField[p] for p in graph])
    
#    # plot the edges
#    edges_x, edges_y = edges
#    plt.plot(edges_x, edges_y, linestyle='-', color = "black", linewidth=0.1, alpha = 1., zorder = 1)
    
    # plot the field
    color = field
    size = 300*np.abs(field)
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=size, cmap = 'viridis', zorder = 2)
    if title:
        plt.title(title)
    plt.colorbar(sc)
    

    plt.axes().set_aspect('equal')
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])

    if savename:
        plt.savefig(savename + '.png', dpi = 100)
#    plt.show()
    plt.close()
    
def edges(graph):
    topara = nx.get_node_attributes(graph, 'para')
    start_pts_abs = []
    start_pts_ord = []
    end_pts_abs = []
    end_pts_ord = []
    for e in graph.edges():
        x1, y1 = topara[e[0]]
        x2, y2 = topara[e[1]]
        start_pts_abs.append(x1)
        start_pts_ord.append(y1)
        end_pts_abs.append(x2)
        end_pts_ord.append(y2)
    return ([start_pts_abs, end_pts_abs], [start_pts_ord, end_pts_ord])

"""
construction
"""

def heart_region(p):
    x, y = p/15.
    return (x**2 + y**2 - 1)**3 - x**2*y**3 <= 0

r = CP.AB()
r.in_region = heart_region
g = r.compute_graph(np.array([0,0,0,0]))

proj = r._para

#pg = proj_graph(g)
plot(g, ((-20,20), (-16,20)))  

"""
diagonalisation
"""
h = -1.*nx.to_numpy_matrix(g)
val, vec = lin.eigh(h)

# plots
#ptToVec = [dict(zip(g.nodes(), wf)) for wf in vec.T]
lims = ((-20,20), (-16,20))
edges = edges(g)

#plot_field(g, vec[:,-40], edges, lims)

vec = vec.T
for i in range(int(len(vec)/2)):
    plot_field(g, vec[i], edges, lims, str(i), str(val[i]))