# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 10:50:24 2016

@author: nicolas

Compute the IPR
"""

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

from scipy.stats import linregress

k = 0.
kx = ky = k
which = "larger"
E = 1.87

# multifractal q
q = 10.
# list storing the IPRs
iprs = []
# list storing the system sizes
sizes = []
for n in range(2, 8):
    en, g = np.load('data/gap/' +  'gen_' + str(n) + '_en_' + str(E) + "_" + which + '_kx_' + str(kx) + '_ky_' + str(ky) + '.npy')
    # get module of the wf
    toAmp = nx.get_node_attributes(g, "state")
    wf = np.array([abs(toAmp[pt]) for pt in g])

    # compute the ipr
    ipr = np.sum(wf**(2.*q))/np.sum(wf**2.)**q
    iprs.append(ipr)
    # system size
    sizes.append(len(wf))
    
logIprs = np.log(iprs)
logS = np.log(sizes)

# fit
slope, intercept, r_value, p_value, std_err = linregress(logS,logIprs)

fig, ax = plt.subplots()
# plot numerics
ax.plot(logS, logIprs, 'og', label=r'numerics')
# plot fit
x = np.linspace(min(logS), max(logS), num = 50)
y = intercept + slope*x
ax.plot(x, y, 'r--', label='fit, scaling = ' + str(np.around(slope,4)))

legend = ax.legend(loc='lower left', shadow=False)
plt.xlabel(r"$\log(N)$ (log number of sites)")
plt.ylabel(r"$\log \chi_q (\psi, N)$")
plt.title(r"Scaling of the $\chi_q$, q = " + str(q) + ", state idos = $1-1/\lambda^2 + 0^+$")
plt.savefig("exponent_numerical_" + which + ".png", dpi=200)
plt.show()
plt.close()
