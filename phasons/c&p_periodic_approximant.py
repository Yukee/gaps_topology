# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 14:17:01 2016

@author: nicolas

Construct periodic approximants using the cut and project method.
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import CP as CP

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

import networkx as nx

def plot(pgraph, savename = None, lims = None, pgraph0 = None):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'para')
    dict_pos = {node:dict_pos[node]-np.dot(r._para, e[2]-e[3]-e[1]) for node in dict_pos}
    plt.axes().set_aspect('equal')
    
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
    
    # draw the nodes of the initial graph, if provided
    if pgraph0:
        nx.draw_networkx(pgraph0, nx.get_node_attributes(pgraph0, 'para'), with_labels = False, node_size= 30)      
    # draw the current graph
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 0)      
    if savename:
        plt.savefig(savename + ".pdf", dpi=150)
#    plt.show()
    plt.close()
    
def o(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + 2*b
    return a

def norm(n):
    return np.sqrt(8.*o(n)**2+2.*(-1)**2)
    
def window(n):
    """
    return the eight vertices of the window
    """
    o1 = o(n)
    o2  =o(n+1)
    return np.array([[o1,o2],[-o1,o2],[-o2,o1],[-o2,-o1],[-o1,-o2],[o1,-o2],[o2,-o1],[o2,o1]])/norm(n)    
    
def draw_window(ax, n, center):
    """
    Draw on axis ax the window of the nth approximant
    """
    verts = list(window(n) - center) + [[0,0]]

    codes = [Path.MOVETO] + [Path.LINETO for x in range(len(verts)-2)] + [Path.CLOSEPOLY]
    path = Path(verts, codes)

    patch = patches.PathPatch(path, facecolor='orange', lw=1, alpha=0.5)
    ax.add_patch(patch)
    ax.set_xlim(-1.1 - center[0],1.1 - center[0])
    ax.set_ylim(-1.1 - center[1],1.1 - center[1])
    ax.set_aspect('equal')    
    
def plotp(pgraph, n, orig, savename = None, lims = None):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'perp')
    plt.axes().set_aspect('equal')
    
    # draw the initial window
    draw_window(plt.axes(), n, -c(n)) 
    # draw the current window
    draw_window(plt.axes(), n, orig)

    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
    
    
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 2., width = 0.)      
    if savename:
        plt.savefig(savename + ".png", dpi=150)
    plt.show()
    plt.close()


"""
construction
"""

def a(n):
    """
    Perp space lattice spacing of the nth approximant
    """
    return 1./np.sqrt(4*o(n)**2+(-1)**n)
    
def c(n):
    """
    distance between the (0,0,0,0) lattice point and the center of the window in standard position
    """    
    return np.array([o(n),o(n-1)])/np.sqrt(8*o(n)**2+2*(-1)**n)

"""
Phason shifts!
"""

n = 2 # number of inflations

Lx = 8.
Ly = 8.
def square_region(p):
    x, y = p
    return  abs(x) <= 0.5*Lx and abs(y) <= 0.5*Ly

shifts = np.arange(0.,1.8, a(n)/4.)

label = 0 # plot label
p0 = np.array([0,0,0,0]) # initial first tiling point
e = np.identity(4, dtype=int)

# initial tiling
r = CP.ABP(n, c(n)- np.array([0.,a(n)/4.]))
r.in_region = square_region
g0 = r.compute_graph((0,0,0,0))
# shifted tilings
for shift in shifts:
    orig = c(n) - np.array([0.,shift]) # origin of the window
    r = CP.ABP(n, orig) # initialization of the tiling
    r.in_region = square_region

    # update p0 so that it falls inside the window
    p0perp = np.dot(r._perp, p0)
    while not r.in_window(p0perp):
        p0 += e[2]
        p0perp = np.dot(r._perp, p0)

    g = r.compute_graph(p0)
    plot(g, "para" + str(n) + 'shift' + str(label), lims = ((-5,5),(-4,4)), pgraph0 = g0)
    plotp(g, n, -orig, "perp" + str(n) + 'shift' + str(label), lims=((-.8,1.6),(-2.5,1.1)))
    label += 1
    