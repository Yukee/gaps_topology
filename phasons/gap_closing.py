# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 11:24:20 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

from scipy.sparse.linalg import eigsh
import scipy.sparse as sparse
from scipy.linalg import eigh
from multiprocessing import Pool # parallel computing of the spectra

"""
construct the tiling and the Hamiltonian (periodic boundary conditions)
"""

# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
# compute a given groundstate
n = 7
square = tl.A5(square0(np.zeros(4),e[0],e[2]))
square.it_sub(n)

# construct the non diagonal part of the Hamiltonian
pg = QGraph.periodize(square._graph)
hjump = nx.to_scipy_sparse_matrix(pg, dtype='float')

# construct the diagonal part
# determine the local environments on the tiling
zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}
# potential on each environment
delta = 0.25
pot = {'A':8., 'B':7., 'C':6., 'D1':5.+delta, 'D2':5.-delta, 'E':4, 'F':3.}
onsitepots = []
for pt in pg:
    n = pg.neighbors(pt)
    z = len(n)
    # if pt is a D type site, distinguish between D1 and D2
    if z == 5:
        # D2 sites have in their neighbourhood 2 F sites
        if 3 in [len(pg.neighbors(pn)) for pn in n]:
            onsitepots.append(pot['D2'])
        else:
            onsitepots.append(pot['D1'])
    else:
        onsitepots.append(pot[zToEnv[z]])
        
L = len(pg)
ind = range(L)
hs = sparse.csr_matrix((onsitepots, (ind, ind)), (L, L))

def ham(t):
    return -t*hjump + (1.-t)*hs
    
"""
diagonalize!
"""

def diag_around(hmat, en):
    """
    find the two states closest to en, return them
    """
    # state below
    en0 = eigsh(hmat, k=1, sigma = en, which='SA', return_eigenvectors=False)
    # state above
    en1 = eigsh(hmat, k=1, sigma = en, which='LA', return_eigenvectors=False)
    
    return en0[0], en1[0]
    
def diag_bunch(hmat, e, k):
    """
    find a bunch of k states around en
    """    
    return eigsh(hmat, k=k, sigma=e, return_eigenvectors=False, which="BE")
    
def diag(hmat):
    return eigh(hmat.todense(), eigvals_only=True)

ts = np.linspace(1., 0.85, 70.)
E = 1.87
#E = 5.
ens = []  
#midgap_ens = [E]
#for t in ts:
#    hmat = ham(t)
#    e0, e1 = diag_around(hmat, E)
#    ens.append([e0, e1])
#    E = 0.5*(e0+e1)
#    midgap_ens.append(E)

ens = [] 
k = 10   
#for t, e in zip(ts, midgap_ens):
#    hmat = ham(t)
#    es = diag_bunch(hmat, e, k)
#    ens.append(es)
    
def spec(args):
    t, e = args
    print(e)
    hmat = ham(t)
    return diag_bunch(hmat, e, k)

#midgap_ens = np.load("/home/nicolas/git/gaps_topology/phasons/data/midgap_energies.npy") 
midgap_ens = [1.87-2.*(t-1.) for t in ts]
if __name__ == '__main__':        
    p = Pool(processes=6) # lauch kernels
    ens = p.map(spec, zip(ts, midgap_ens))

    
#ts = np.linspace(1., 0.9, 10.)
#E = 1.87
#ens = []
#for t in ts:
#    hmat = ham(t)
#    es = diag(hmat)
#    ens.append(es)
    
"""
Plot!
"""

#ens0, ens1 = np.asarray(ens).T
#tts = ts[:len(ens0)]
#plt.plot(tts, ens0, 'o')
#plt.plot(tts, ens1, 'o')
#plt.savefig("gap_closing_upper.eps")
#plt.show()

for e in np.asarray(ens).T:
    plt.plot(ts, e, 'r+', markersize=5.)
plt.plot(ts, midgap_ens)
plt.xlabel("t")
plt.ylabel("E")
plt.savefig("states_around_gap_n_7_1.pdf")
plt.show()