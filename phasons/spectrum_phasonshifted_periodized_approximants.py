# -*- coding: utf-8 -*-
"""
Created on Thu Sep 29 15:50:54 2016

@author: nicolas

"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import Tilings as tl
import QuantumGraph as QGraph

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import scipy
from scipy.sparse.linalg import eigsh

n = 2 # number of inflations

# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
square = tl.A5(square0(np.zeros(4),e[0],e[2]))
square.it_sub(n)

g = square._graph.copy()
g = nx.convert_node_labels_to_integers(square._graph, label_attribute = 'pos')

def flip_and_plot(graph, axis):
    g = graph.copy()
    eds = square.phason_flip(g, axis)
    
    # plot in perp and para spaces
    fig, ax = plt.subplots(1, 2)
    # node size
    node_size = 20.*(np.sqrt(2)-1.)**(2*n)
    # marker size (used in plot)
    markersize = 2.
    
    perp_pos = nx.get_node_attributes(g, 'perp')
    ax[0].set_aspect('equal')
    nx.draw_networkx(g, perp_pos, with_labels = False, node_size = node_size, width = 0., ax = ax[0])
    perppts = np.array([g.node[n]['perp'] for n in eds])
    ax[0].plot(*(perppts.T), 'o', markersize = markersize)
    
    para_pos = nx.get_node_attributes(g, 'para')
    ax[1].set_aspect('equal')
    nx.draw_networkx(g, para_pos, with_labels = False, node_size = node_size, width = .5, ax = ax[1])
    parapts = np.array([g.node[n]['para'] for n in eds])
    ax[1].plot(*(parapts.T), 'o', markersize = markersize)

    return g
    
def sparse_spec(h, e, nlevels):
    """
    Return the nlevels energy levels around energy e
    /!\ h has to be a sparse matrix
    """
    return eigsh(h, k=nlevels, sigma=e, return_eigenvectors=False, which="BE")

    
def flip_and_diag(graph, axis, eright, nstates):
    square.phason_flip(g, axis)
#    pg = QGraph.periodize(g, 'v')
    hper = -1.*nx.to_scipy_sparse_matrix(g, dtype="float")
    #specper = scipy.linalg.eigh(hper, eigvals_only=True)
    specper = sparse_spec(hper, eright, nstates)
    
    return g, specper

def iter_graph(init_graph, nit):
    g = init_graph.copy()
    # list of the successive directions of jump
    axlist = [2,1,2,3]
    # store the spectra
    specs = []
    for i in range(nit):
        ax = axlist[i % 4]
        g, spec = flip_and_diag(g, ax)
        specs.append(spec)
    return specs    

def dos(spec, epsilon, z):
    """
    Compute DoS at energy z with lorenzian broadening epsilon, for spectrum spec
    """
    peaks = epsilon/(((spec - z)**2 + epsilon**2)*np.pi)
    return np.sum(peaks)/len(peaks)    
    
        
"""
large-scale test
"""

n = 6
square = tl.A5(square0(np.zeros(4),e[0],e[2]))
square.it_sub(n)
g = nx.convert_node_labels_to_integers(square._graph, label_attribute = 'pos')

# total number of flips
nflips = 100

# spectral broadening
eps = 0.0001

# energy range
N = 3*len(square._graph)

eright = 1.9 # approximative energy of the gap
nstates = 200 # number of states around the gap

# list of the successive directions of jump
axlist = [2,1,2,3]
# store the spectra
specs = []
for i in range(nflips):
    ax = axlist[i % 4]
    g, spec = flip_and_diag(g, ax, eright, nstates)
    specs.append(spec)

    delt = 0.
    erange = np.linspace(min(spec) - delt, max(spec) + delt, N)

    print("computing dos " + str(i))
    dost = np.array([dos(spec, eps, e) for e in erange])
    plt.plot(erange, dost, alpha = 0.5)
    plt.ylim((0, 60.))
    plt.xlim((1.82, 1.95))
    plt.savefig("dosfree_"+str(i)+".png", dpi = 150)
    plt.close()

