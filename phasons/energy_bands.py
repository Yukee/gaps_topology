# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 11:38:47 2016

@author: nicolas

Plot the energy bands of an approximant, in an energy interval
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import Tilings as tl
import QuantumGraph as QGraph
import AB_envs as envs

import numpy as np
from multiprocessing import Pool
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

#square = tl.A5(envs.squareCanonical)  
square = tl.A5(envs.doubledSquaresPavel)      
n = 6
square.it_sub(n)
    
cell = QGraph.SquareCell(square._graph)
# hopping 
hop = -1.
"""
whole spectrum
"""
def spec1D(kx):
    energies = cell.spectrum(hop, (kx,0.))[0]
    return list(map(lambda en: [kx, en], energies))
def spec(k):
    energies = cell.spectrum(hop, k)[0]
    return list(map(lambda en: [k, en], energies))
    
"""
Part of the spectrum
"""
# number of energy states to compute
nstates = 7
def part_spec(E, k):
    energies = cell.states_around(hop, k, E, nstates)[0]
    return list(map(lambda en: [k[0], k[1], en], energies))
def part_spec1D(E, kx):
    energies = cell.states_around(hop, (kx, 0.), E, nstates)[0]
    return list(map(lambda en: [kx, en], energies))
    
def idos(spec, E):
    """
    count the number of states below energy E
    """
    ct = 0
    for e in spec:
        if e < E:
            ct +=1
    return ct

    
"""
1D
"""
#    # number of k points
#    N = 50
#    ks = np.linspace(0., np.pi, N)
##    p = Pool(6)
##    energies = p.map(spec1D, ks)
#    energies = [part_spec1D(-2.2, kx) for kx in ks]
#    kk, ens = np.transpose(energies)
#    for k, es in zip(kk, ens):
#        plt.plot(k, es, 'bo')
#    plt.savefig("around_gap_n_" + str(n) + " .png", dpi=200)
#    plt.show()
        
"""
2D
"""
if __name__ == "__main__":

    Nx = 30
    Ny = 10
    kkx = np.linspace(0., np.pi, Nx)
    kky = np.linspace(0., np.pi, Ny)
    ks = np.array([[kx, ky] for kx in kkx for ky in kky])
    
    ksens = [part_spec(-1.888, k) for k in ks]
#    # parallelized computation
#    p = Pool(7)
#    def compute(k):
#        return part_spec(-1.9, k)
#    ksens = p.map(compute, ks)
    kx, ky, energies = np.asarray(ksens).T
    
    # 3D plot
    #    fig = plt.figure()
    #    ax = fig.add_subplot(111, projection='3d')
    #    ax.scatter(kx, ky, energies)
    #    plt.show()
    
#    # projection onto 2D
#    plt.scatter(kx, energies, c=ky, cmap="viridis", edgecolor='')
#    plt.show()
#    plt.savefig("around_gap_n_" + str(n) + "_2D.png", dpi=200)
#    #
#    np.save('/home/nicolas/git/gaps_topology/phasons/data/' +  'energies_around_negative_gap_gen_' + str(n), ksens)
    
    plt.scatter(kx, energies, c=ky, cmap="viridis", edgecolor='')
    plt.show()
    plt.savefig("around_gap_pavel_n_" + str(n) + "_2D.png", dpi=200)
    #
    np.save('/home/nicolas/git/gaps_topology/phasons/data/' +  'energies_around_negative_gap_pavel_gen_' + str(n), ksens)
