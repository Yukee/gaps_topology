# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 11:36:27 2016

@author: nicolas

A simple data analysis algorithm to find the candidate edge states,
by looking at how much spectral weight dwells on the sites close to the edges.
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import CP as CP

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

import scipy.linalg as lin

def plot(pgraph, lims = None):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'para')
    plt.axes().set_aspect('equal')
    
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
    
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size = 0)      
    plt.savefig("para.pdf")
    plt.show()
    plt.close()
    
def plotp(pgraph, lims = None):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'perp')
    plt.axes().set_aspect('equal')
    
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
    
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 2., width = 0.)      
    plt.savefig("perp.pdf")
    plt.show()
    plt.close()
    
def plot_field(graph, field, edges, lims = None, savename = None, title = None):
    """
    Plot a field on a tiling, using 
    """
    # retrieve the positions
    pos = np.array([p[1]['para'] for p in graph.nodes(data=True)])
    x, y = np.array(pos).T
#    field = np.array([ptToField[p] for p in graph])
    
#    # plot the edges
#    edges_x, edges_y = edges
#    plt.plot(edges_x, edges_y, linestyle='-', color = "black", linewidth=0.1, alpha = 1., zorder = 1)
    
    # plot the field
    color = field
    size = 300*np.abs(field)
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=size, cmap = 'viridis', zorder = 2)
    if title:
        plt.title(title)
    plt.colorbar(sc)
    

    plt.axes().set_aspect('equal')
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])

    if savename:
        plt.savefig(savename + '.png', dpi = 100)
    plt.show()
    plt.close()

    

"""
construction
"""

# linear size of the square region
lb = 1+np.sqrt(2)
L = 50.

r = CP.AB()
# set the size of the square region
r._L = L

# origin
v = np.array([0,0,0,0])
vp = np.dot(r._para, v)

# compute the tiling!
g = r.compute_graph(v)

"""
diagonalisation
"""
h = -1.*nx.to_numpy_matrix(g)
val, vec = lin.eigh(h)

"""
edge to bulk
"""
def edge_sites(tiling, th):
    """
    return a list of sites which are at distance th from the edge of the square domain
    """
    para = tiling._para
    es = [p for p in tiling._g if not tiling.in_rect(L-th, np.dot(para, p))] # list of edge sites
    return es

def edge_bulk(tiling, es, vec):
    return np.sum(vec[p]**2 for p in es)

pToVec = [dict(zip(g.nodes(), wf)) for wf in vec.T]
es = edge_sites(r, 3.)
ebs = [edge_bulk(r, es, v) for v in pToVec]

plt.plot(ebs)

i = 1033
plt.plot(i, .5, 'o')

rg = 110
i = 177 + rg
plt.plot(i, .3, 'o')
plt.show()
    
