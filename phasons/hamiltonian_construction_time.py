# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 15:25:11 2016

@author: nicolas

Speed tests of periodic Hamiltonian construction
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import Tilings as tl
import QuantumGraph as QGraph

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from scipy.sparse import csr_matrix

# timing analysis
from timeit import default_timer as timer
from scipy.stats import linregress

# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]

square = tl.A5(square0(np.zeros(4),e[0],e[2]))
square.it_sub(1)

start = timer()

pg = square._graph
nx.set_edge_attributes(pg, 'h', 0.)
nx.set_edge_attributes(pg, 'v', 0.)
QGraph.periodize_along(pg, 'h')
QGraph.periodize_along(pg, 'v')

k = np.array([0.,0.])
tx_r, ty_r = np.exp(1j*k)
tx_l, ty_l = np.exp(-1j*k)


indices = {"h":{1:[],-1:[]},"v":{1:[],-1:[]},"hv":{1:[],-1:[]}}
bulk_indices = []
index = dict(zip(pg.nodes(), range(len(pg))))
pos = nx.get_node_attributes(pg, 'para')
for e in pg.edges(data=True):
    p1, p2, attr = e
    i = (index[p1], index[p2])
    # if e is an edge jump
    if attr['h'] or attr['v']:
        # the hopping changes depending on the direction we cross the edge
        dx, dy = np.sign(pos[p2] - pos[p1])

        if attr['h'] and attr['v']:
            # check that in that case dx == dy
            if dx != dy: raise RuntimeError("ioups!")
            indices["hv"][dx].append(i)
        elif attr['h']:
            indices["h"][dx].append(i)
        else:
            indices["v"][dy].append(i)
    # if e is a bulk jump
    else:
        bulk_indices.append(i)

L = len(pg)
Hedge_up = {"h":{}, "v":{}, "hv":{}} # will later be organized as data, but with sparse matrices instead of lists
# construct the edge Hamiltonians
for name in indices:
    # right movers
    for sign in (1,-1):
        if len(indices[name][sign])>0:
            rows, cols = np.asarray(indices[name][sign]).T
            data = np.ones(len(cols))
            Hedge_up[name][sign] = csr_matrix((data, (rows, cols)), shape=(L,L))
        else:
            Hedge_up[name][sign] = csr_matrix((L,L))
    
# construct the bulk Hamiltonian
rows, cols = np.asarray(bulk_indices).T
data_bulk = np.ones(len(cols))
Hbulk_up = csr_matrix((data_bulk, (rows, cols)), shape=(L,L))
Hbulk = Hbulk_up + Hbulk_up.H

stop = timer()
elapsed = stop - start

# construct the whole Hamiltonian
whole_edge_up = tx_r*Hedge_up["h"][1] + ty_r*Hedge_up["v"][1] + tx_r*ty_r*Hedge_up["hv"][1] + tx_l*Hedge_up["h"][-1] + ty_l*Hedge_up["v"][-1] + tx_l*ty_l*Hedge_up["hv"][-1]

Hedge = whole_edge_up + whole_edge_up.H

"""
Speedtests
"""
# construction times with the above method
times = [0.0026710490019468125, 0.0069610370010195766, 0.043561981998209376, 0.15392449199862313, 0.9736255790012365, 5.785619091999251, 37.53986609200001]
logT = np.log(times)[2:]
# lengths
Ls = [square.o(2*n)+square.o(2*n+1) for n in range(3,8)]
logL = np.log(Ls)

slope, intercept, r_value, p_value, std_err = linregress(logL,logT)

fig, ax = plt.subplots()
# plot numerics
ax.plot(logL,logT, 'og', label=r'numerics')
# plot fit
x = np.linspace(min(logL), max(logL), num = 50)
y = intercept + slope*x
ax.plot(x, y, 'r--', label='fit, scaling = ' + str(np.around(slope,4)))

legend = ax.legend(loc='lower right', shadow=False)
plt.xlabel(r"$\log(N)$ (log number of sites)")
plt.ylabel(r"$log(T)$ (log construction time)")
plt.title(r"Scaling of the construction time of the periodic Hamiltonian $H(k)$.")
plt.savefig("construction_time_periodic_hamiltonian.png", dpi=200)
plt.show()
#plt.close()