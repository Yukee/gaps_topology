# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 13:39:36 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph
import AB_envs as envs

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
    
def listplot(pos, field, ptsize=1, savename=None):
    x, y = np.array(pos).T
    color = field
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=ptsize, cmap = 'viridis')
    plt.colorbar(sc)
    plt.axes().set_aspect('equal')
    dark = plt.cm.viridis(0)
    plt.gca().set_axis_bgcolor(dark)
    if(savename):
        plt.savefig(savename+'.png', dpi=300)
    plt.show()
    #fig.canvas.draw()
    
pavel = tl.A5(envs.doubledSquaresPavel)
n = 5
pavel.it_sub(n)
cell = QGraph.SquareCell(pavel._graph)
# diagonalize at energy E
E = -1.875
((en0, en1), (vec0, vec1)) = cell.states_surround(-1., (0.,0.), E)    
## associate to each node the corresponding amplitude, and save the result
#ptsToGs = {pt:amp for pt, amp in zip(pg, wf)}
#nx.set_node_attributes(g, "state", ptsToGs)
#np.save('data/gap/' +  'gen_' + str(n) + '_pavel_en_' + str(E) + "_" + '_k00', (en, g))
#
#E = 1.87

# load a given state
#en, g = np.load('data/gap/' +  'gen_' + str(n) + '_en_' + str(E) + "_" + which + '_kx_' + str(kx) + '_ky_' + str(ky) + '_phason_flipped'  + '.npy')
## get the groundstate amplitude data
#toAmp = nx.get_node_attributes(g, "state")
#amp = [abs(toAmp[pt]) for pt in g]

amp = np.abs(vec1)

plt.axes().set_aspect('equal')
backgrdcolor = plt.get_cmap('viridis')(0)
plt.axes().set_axis_bgcolor(backgrdcolor)
pos = nx.get_node_attributes(cell._graph, 'para')
nx.draw_networkx(cell._graph, 
                pos,
                with_labels = False, node_size = 10., width = 0., cmap = 'viridis', node_color = amp)
plt.show()