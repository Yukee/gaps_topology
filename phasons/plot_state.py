# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 13:57:49 2016

@author: nicolas
"""

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
    
def o(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + 2*b
    return a

def L(n):
    """ length of the unit cell of the nth approximant """
    return np.sqrt((o(n)+o(n-1))**2 + 2*o(n)**2)    
    
def listplot(pos, field, ptsize=1, title=None, savename=None):
    x, y = np.array(pos).T
    color = field
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=ptsize, cmap = 'viridis')
    cbar = plt.colorbar(sc)
    #cbar.set_label('Electron density', rotation=0)
    cbar.ax.set_title('Electron density', rotation=0)
    plt.axes().set_aspect('equal')
    if title:
        plt.title(title)
    # length of the system (in physical space)
    l = L(n)
    plt.xlim([0., l])
    plt.ylim([0., l])
    plt.xlabel("length (lattice spacing set to 1)")
    dark = plt.cm.viridis(0)
    plt.gca().set_axis_bgcolor(dark)
    if(savename):
        plt.savefig(savename+'.png', dpi=400)
    plt.show()
    
# number of inflations
n = 7
# wavevectors
k = 0.
kx, ky = k, k
# reference energy
E = 1.87
# state at energy larger or smaller than reference energy
which = "smaller"
# in which substace to plot
proj = 'para'
# import a tiling
try:
    en, g = np.load('data/gap/' +  'gen_' + str(n) + '_en_' + str(E) + "_" + which + '_kx_' + str(kx) + '_ky_' + str(ky) + '.npy')
except FileNotFoundError:
    print("Groundstate data does not exist. Run exact_diagonalization_groundstate.py to generate it.")
    raise
# get the groundstate amplitude data
toAmp = nx.get_node_attributes(g, "state")
amp = [abs(toAmp[pt]) for pt in g]
# get the node positions
toPos = nx.get_node_attributes(g, proj)
pos = [toPos[pt] for pt in g]
# plot everything
listplot(pos, amp, .4, 'energy ' + str(en[0]), 'state_' + which + '_' + proj + '_gen_'+str(n))