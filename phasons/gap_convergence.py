# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 17:18:49 2016

@author: nicolas

This script studies convergence of the energy and width of the main gap, for different types of approximants
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph
import AB_envs as envs

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

"""
computation of the gap width and mean value
"""
#cellType = "canonical"
##system = tl.A5(envs.doubledSquaresPavel)
#system = tl.A5(envs.squareCanonical)
#
## list of energies that are inside the gap (Pavel)
##GapEnergy = {4:-1.875,5:-1.87,6:-1.87,7:-1.87,8:-1.87}
## list of energies that are inside the gap (Canonical)
#GapEnergy = {3:-2.,4:-1.9,5:-1.88,6:-1.87,7:-1.87,8:-1.87}
#
#means = []
#widths = []
#for n in range(4,8):
#    system.it_sub(n)
#    cell = QGraph.SquareCell(system._graph)
#    E = GapEnergy[n]
#    (en0, en1) = cell.states_surround(-1., (0.,0.), E)[0]
#    mean = (en0+en1)*0.5
#    width = en1-en0
#    means.append(mean)
#    widths.append(width)
#    
#np.save('/home/nicolas/git/gaps_topology/phasons/data/' +  'gap_mean_and_width_' + cellType, (means, widths))

"""
import the results of the computation
"""
ns = range(4,8)
meansCanon = [-1.9326888638629725, -1.8936718341577778, -1.8791049353004352, -1.8752506917454201]
widthsCanon = [0.12514245768351917, 0.054920803622802383, 0.024427611624361356, 0.016934973213575955]
meansPavel = [-1.8749030548652055, -1.8744007269169414, -1.8743706457748419, -1.8743563785319135]
widthsPavel = [0.0093774091724390107, 0.014141899761630272, 0.014937213610751066, 0.015082277149182977]
#cellType = "canonical"
#meansCanon, widthsCanon = np.load('/home/nicolas/git/gaps_topology/phasons/data/' +  'gap_mean_and_width_' + cellType + ".npy")
#
#cellType = "pavel"
#meansPavel, widthsPavel = np.load('/home/nicolas/git/gaps_topology/phasons/data/' +  'gap_mean_and_width_' + cellType + ".npy")
## add the last point, calculated by Arnaud's computer
#meansPavel = list(meansPavel)
#meansPavel.append(-1.87435638)
#widthsPavel = list(widthsPavel)
#widthsPavel.append(0.01508228)
#
"""
Plots!
"""
fig, ax = plt.subplots()
ax.plot(ns, meansCanon, 'ro', label=r'$\overline{E}$ for canonical unit cell')
ax.plot(ns, meansPavel, 'o', label=r"$\overline{E}$ for Pavel's unit cell")
plt.xlim([ns[0]-.2, ns[-1]+.2])
plt.xlabel("Number of inflations")
plt.ylabel("Mean energy of the gap")
legend = ax.legend(loc='lower right', shadow=False)
plt.savefig("gap_mean_energy_scaling.pdf")
plt.show()
plt.close()

fig, ax = plt.subplots()
ax.plot(ns, widthsCanon, 'ro', label=r'Gap width for canonical unit cell')
ax.plot(ns, widthsPavel, 'o', label=r"Gap width for Pavel's unit cell")
plt.xlim([ns[0]-.2, ns[-1]+.2])
plt.xlabel("Number of inflations")
plt.ylabel("Gap width")
legend = ax.legend(loc='upper right', shadow=False)
plt.savefig("gap_width_scaling.pdf")
plt.show()
#plt.plot(ns, widthsCanon, 'ro', ns, widthsPavel, 'o')