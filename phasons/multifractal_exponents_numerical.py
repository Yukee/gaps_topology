# -*- coding: utf-8 -*-
"""
Created on Mon Oct 17 18:19:13 2016

@author: nicolas
"""

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

from scipy.stats import linregress
from scipy.optimize import curve_fit

k = 0.
kx = ky = k
which = "larger"
E = 1.87
nmin = 2
nmax = 7

# list containing the loaded wavefunctions
wfs = []
# load the wavefunctions
for n in range(nmin, nmax+1):
    en, g = np.load('data/approximant_groundstate/' +  'gen_' + str(n) + '_kx_' + str(kx) + '_ky_' + str(ky) + '.npy')
    toAmp = nx.get_node_attributes(g, "groundstate") 
#    en, g = np.load('data/gap/' +  'gen_' + str(n) + '_en_' + str(E) + "_" + which + '_kx_' + str(kx) + '_ky_' + str(ky) + '.npy')
#    toAmp = nx.get_node_attributes(g, "state")
    wf = np.array([abs(toAmp[pt]) for pt in g])
    wfs.append(wf)

def tau(wfs, q):
    """
    Return the exponent tau(q)
    """
    
    # list storing the IPRs
    iprs = []
    # list storing the system sizes
    sizes = []
    for wf in wfs:
        # compute the ipr
        ipr = np.sum(wf**2.)**q/np.sum(wf**(2.*q))
        iprs.append(ipr)
        # system size
        sizes.append(len(wf))
    
    logIprs = np.log(iprs)
    logS = np.log(sizes)
    
    # fit
    slope, intercept, r_value, p_value, std_err = linregress(logS,logIprs)
    
    return slope
    
def om(z):
    return (4.+9.*z+4.*z**2+2.*np.sqrt(2)*np.sqrt(2.+9.*z+14.*z**2+9.*z**3+2.*z**4))/z
vom = np.vectorize(om)
# the normalization in the theoretical expression of the exponent
norm = om(1.)

def tau_theo(q, betasq):
    """
    The function to fit
    """
    return np.log(vom(betasq)**q/vom(betasq**q))/np.log(norm)
    
def d_theo(q, betasq):
    return np.log(vom(betasq)**q/vom(betasq**q))/((q-1.)*np.log(norm))
    
# approximate q=1    
q1 = 2.
# approximate tau(q=1)
tau1 = tau(wfs, q1)
# list of q indices we want to compute the scaling for
qlist = np.arange(q1, 30, 2.)
taulist = []
dlist = []
for q in qlist:
    tauq = tau(wfs, q)
    taulist.append(tauq)
    dq = (tauq)/(q-1.)
    dlist.append(dq)
    
# fit
betasqfit, err = curve_fit(d_theo, qlist, dlist)
betasqfit = betasqfit[0]

fig, ax = plt.subplots()
# plot numerics
ax.plot(qlist, dlist, 'og', label=r'numerics')
# plot theory
betasquare = betasqfit #1.35809
x = np.linspace(min(qlist), max(qlist), num = 50)
y = np.log(vom(betasquare)**x/vom(betasquare**x))/((x-1.)*np.log(norm))
ax.plot(x, y, 'r--', label='prediction')

legend = ax.legend(loc='lower left', shadow=False)
plt.xlabel(r"$q$")
plt.ylabel(r"$D_q$")
plt.title(r"Fractal exponents, groundstate")
plt.savefig("fractal_exponents_groundstate.png", dpi=200)
#plt.title(r"Fractal exponents, state idos = $1-1/\lambda^2 + 0^+$")
#plt.savefig("fractal_exponents_" + which + "_pi" + ".pdf")
plt.show()
plt.close()
