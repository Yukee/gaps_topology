# -*- coding: utf-8 -*-
"""
Created on Mon Oct  3 14:54:11 2016

@author: nicolas

k-space structure:
We look at form of a given energy state in k-space.
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import Tilings as tl
import QuantumGraph as QGraph

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from scipy.sparse.linalg import eigsh

# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
square = tl.A5(square0(np.zeros(4),e[0],e[2]))

def groundstate(n, hopping, kx, ky):
    """
    Construct a tiling, return the graph and the grounstate.
    """
    # boundary jump amplitude twist
    twistx = np.exp(1j*kx)
    twisty = np.exp(1j*ky)
    
    """
    construct the Hamiltonian
    """
    # construct the tiling
    square.it_sub(n)
    # periodize
    pg = QGraph.periodize_along(square._graph, 'h', hopping, twistx)
    pg = QGraph.periodize_along(pg, 'v', hopping, twisty)
    # construct the matrix
    hmat = nx.to_scipy_sparse_matrix(pg, weight='hopping')
    
    """
    compute or load the ground state
    """
    # try loading the ground state
#    try:
#        en, gs = np.load('data/approximant_groundstate/periodic/' +  'gen_' + str(n) + '_kx_' + str(kx) + '_ky_' + str(ky)  + '.npy')
#    # otherwise compute it
#    except FileNotFoundError:
    en, vec = eigsh(hmat, k=1, which='SA')
    gs = vec[:,0] # remove the unecesseray list
#    np.save('data/approximant_groundstate/periodic/' +  'gen_' + str(n) + '_kx_' + str(kx) + '_ky_' + str(ky), (en, gs))
    
    return en, gs    

"""
Plot
"""
if __name__ == '__main__':
    en, gs = groundstate(2, -1., np.pi/2., np.pi/2.)    
    
    plt.axes().set_aspect('equal')
    backgrdcolor = plt.get_cmap('viridis')(0)
    plt.axes().set_axis_bgcolor(backgrdcolor)
    nx.draw_networkx(pg, 
                    nx.get_node_attributes(pg, 'perp'),
                    with_labels = False, node_size = 20, width = .5, cmap = 'viridis', node_color = np.abs(gs))
    