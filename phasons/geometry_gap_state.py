# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 11:25:50 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

from scipy.sparse.linalg import eigsh

# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]


n = 4
square = tl.A5(square0(np.zeros(4),e[0],e[2]))
square.it_sub(n)
g = QGraph.periodize(square._graph)

color = []
for p in g:
    if len(g[p]) == 8:
        color.append('red')
    else:
        color.append('black')

nx.set_node_attributes(g, "color", 'black')        
a = e[0]
b = e[1] - e[3]    

toPos = nx.get_node_attributes(g, 'para')
pos = [toPos[pt] for pt in g]

x, y = np.array(pos).T
sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=20., cmap = 'viridis')
plt.axes().set_aspect('equal')

dark = plt.cm.viridis(0)
plt.gca().set_axis_bgcolor(dark)
plt.savefig("drawing.eps")