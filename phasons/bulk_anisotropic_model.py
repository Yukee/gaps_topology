# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 15:11:13 2016

@author: nicolas

This script constructs a periodic approximant and diagonalizes an anisotropic hopping model.
"""

import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package

import Tilings as tl
import QuantumGraph as QGraph

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

import scipy
from scipy.sparse.linalg import eigsh # Lanczos algorithm

from multiprocessing import Pool # parallel computing of the spectra

sq2 = np.sqrt(2)
lb = sq2 - 1.
pi = np.pi
e = np.exp(1j*pi*np.arange(4)/4)

def square(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
"""
Utility functions
"""

def couplings(graph, lift):
    aniG = graph.copy()
    # couplings affectation
    couplings = {(1,0,0,0):1,(0,1,0,0):2,(0,0,1,0):1,(0,0,0,1):2}
    
    def c1(edge):
        hop = 0.
        if couplings[tuple(abs(lift[edge[1]]-lift[edge[0]]))] == 1:
             hop = 1.
        return hop
        
    def c2(edge):
        hop = 0.
        if couplings[tuple(abs(lift[edge[1]]-lift[edge[0]]))] == 2:
             hop = 1.
        return hop
    
    # anisotropic connectivity graph
    hop1 = {e:c1(e) for e in aniG.edges()}
    hop2 = {e:c2(e) for e in aniG.edges()}

    nx.set_edge_attributes(aniG, "hop1", hop1)
    nx.set_edge_attributes(aniG, "hop2", hop2)
    
    # periodic graph
    aniGp = QGraph.periodize(aniG)
    
    return aniGp
    
def mats(graph, lift):
    """
    Compute the couplings and return the anisotropy matrices
    """
    aniGp = couplings(graph, lift)   
    return (nx.to_numpy_matrix(aniGp, weight = "hop1", dtype="float"), nx.to_numpy_matrix(aniGp, weight = "hop2", dtype="float"))

def sparse(graph, lift):
    """
    Compute the couplings and return the anisotropy sparse matrices
    """
    aniGp = couplings(graph, lift)   
    return (nx.to_scipy_sparse_matrix(aniGp, weight="hop1", dtype="float"), nx.to_scipy_sparse_matrix(aniGp, weight="hop2", dtype="float"))

def complete_spec(h1, h2, ratio):
    """
    return anisotropic hamiltonian with an anisotropy between horizontal/vertial and diagonal bonds
    h1 is the connectivity matrix of the horizontal/vertical part
    h2 is the connectivity matrix of the diagonal part
    """    
    return scipy.linalg.eigh(-h1 -ratio*h2, eigvals_only=True)
    
def sparse_spec(h1, h2, ratio, e, nlevels):
    """
    Return the nlevels energy levels around energy e
    /!\ h1 and h2 have to be sparse matrices
    """
    return eigsh(-h1-ratio*h2, k=nlevels, sigma=e, return_eigenvectors=False, which="BE")
        
def plot(specs, ratios, savename=None, title=None):
    y = np.array(specs)
    x = np.array([ratios for i in range(len(y[0]))]).T
    
    plt.xlabel("anisotropy")
    plt.ylabel("energy")
    if title:        
        plt.title(title)
    plt.plot(x, y, '_',  c="blue", alpha=0.5,markersize = 10.) 
#    plt.axis([xmin, xmax, ymin, ymax])
#    plt.axes().set_aspect('equal')

    if savename:
        plt.savefig(savename+'.png', dpi = 200)
    plt.show()
    plt.close()
    

"""
Construction of the approximant and of the anisotropic hamiltonian
"""
n = 5 # number of inflations

# create a tiling whose basic tile is a square
square = tl.A5(square(0,e[0],e[2]))
square.it_sub(n)
g = square._graph

lift = square.lift(0)


"""
Varying anisotropy
"""

h1, h2 = sparse(g, lift)

dri = -0.2
drf = 0.2
ratio = np.linspace(1. + dri, 1. + drf, 40)

e0 = -2. # we look at energy levels around this energy
nlevels0 = 80 # the number of levels we want to compute


#specs = np.array([spec_ani_ham(h1, h2, rho) for rho in ratio])

slope = -.8
def spec(rho):
    return sparse_spec(h1, h2, rho, slope*(rho-1.)+e0, nlevels0)

if __name__ == '__main__':        
    p = Pool(processes=6) # lauch kernels
    specs = p.map(spec, ratio)
    
plot(specs, ratio, savename="anisotropic_variation_iter_"+str(n), title="Levels around the main gap, for the approximant "+str(n))

"""
Color testing
"""
#ratio = 0.5
#couplings = {(1,0,0,0):-1.,(0,1,0,0):-ratio,(0,0,1,0):-1.,(0,0,0,1):-ratio}
#aniG = QGraph.ani_graph(g, couplings, lift)
#aniGp = QGraph.periodize(aniG)
#colorfunc = {-1.:'r',-ratio:'g'}
#    
#colorlist = [colorfunc[e[2]['hopping']] for e in aniGp.edges(data = True)]
#
#list_pos = [(v.real, v.imag) for v in g]
#dict_pos = dict(zip(g.nodes(), list_pos))
#
#
#nx.draw_networkx(aniGp, dict_pos, with_labels = False, edge_color = colorlist, node_size = 0.)
#xmin, xmax, ymin, ymax = -0.2, 1.2, -0.2, 1.2
#plt.axis([xmin, xmax, ymin, ymax])
#plt.axes().set_aspect('equal')