# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 11:00:46 2016

@author: nicolas

Study the kspace structure
"""

import groundstate as gs
import numpy as np
from multiprocessing import Pool
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

en, wf = gs.groundstate(4, -1., np.pi/2., np.pi/2.)

# create a grid of k points
N = 20
L = np.pi
kkx = np.linspace(-L, L, N)
kky = np.linspace(-L, L, N)
ks = np.array([[[kx, ky] for kx in kkx] for ky in kky])

# number of inflations 
n = 5
hopping = -1.
def diag(k):
    return gs.groundstate(n, hopping, k[0], k[1])[0]

if __name__ == '__main__':
    #diag = lambda k: groundstate(n, hopping, k[0], k[1])[1]

    p = Pool(7)
    energies = np.array([p.map(diag, ks[i]) for i in range(N)])
    energies = energies[:,:,0]
#    energies = p.map(diag, ks)
#    # reshape
#    energies = np.asarray(energies)[:,0]
#    energies = energies.reshape(N, N)
    # plot    
    KX, KY = np.meshgrid(kkx, kky)
    
#    plt.axes().set_aspect('equal')
#    plt.contourf(KX, KY, energies, 30, cmap='viridis')
    
    fig = plt.figure()
#    ax = fig.add_subplot(111, projection='3d')
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    surf = ax.contourf(KX, KY, energies, 10, rstride=1, cstride=1, cmap='viridis')
    fig.colorbar(surf)
    plt.title('The lowest energy band (approximant n = ' + str(n) + ')')
    plt.set_xlabel(r'$k_x$')
    plt.set_ylabel(r'$k_y$')
    
    plt.savefig('dispersion_lowest.png', dpi=150)
    plt.show()
    
    