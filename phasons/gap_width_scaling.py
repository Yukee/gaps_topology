# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 14:31:32 2016

@author: nicolas

This script plots approximant size dependance of the energy of the states supposedly above and below the main gap.
For small approximants, the states are not necesarily the right ones, and this script helps spotting that.
"""
import numpy as np
import matplotlib.pyplot as plt

k = 0.
kx = ky = k
which = "smaller"
E = 1.87

# lists storing the energies
elow = []
ehig = []
# list storing the gap widths
widths = []
for n in range(1, 8):
    el = np.load('data/gap/' +  'gen_' + str(n) + '_en_' + str(E) + "_" + "smaller" + '_kx_' + str(kx) + '_ky_' + str(ky) + '.npy')[0]
    elow.append(el)
    eh = np.load('data/gap/' +  'gen_' + str(n) + '_en_' + str(E) + "_" + "larger" + '_kx_' + str(kx) + '_ky_' + str(ky) + '.npy')[0]
    ehig.append(eh)
    # system size
    widths.append(eh - el)

plt.plot(elow, 'o')
plt.plot(ehig, 'o')