# -*- coding: utf-8 -*-
"""
Created on Sun Jul 31 16:41:09 2016

@author: nicolas

A simple script displaying the perp space structure of the AB tiling.
"""

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

# octonacci numbers
def o(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + 2*b
    return a

# inflation
n = 2

# ratio of two consecutive octonacci number (this will be frequently used)
r = o(n-1)/o(n)

# norm of the vectors spanning the physical space
npara = np.sqrt(4+(-1)**n/o(n)**2)
# nomr of the vectors spanning the internal space
nperp = np.sqrt(2)*npara

# vectors spanning the physical space
ex = np.array([1+r,1,0,-1])/npara
ey = np.array([0,1,1+r,1])/npara
# projection matrix onto Epara
mpara = np.array([ex, ey])

# vectors spanning the internal space
exp = np.array([2,-1-r,0,1+r])/nperp
eyp = np.array([0,1+r,-2,1+r])/nperp
# projection matrix onto Eperp
mperp = np.array([exp,eyp])

# porjection onto perpspace for the AB tiling
sq = np.sqrt(2)
mperpAB = 0.5*np.array([[sq, -1, 0, 1], [0, 1, -sq, 1]])

"""
Project the unit cube
"""
def proj_graph(proj, graph):
    """ 
    project the graph onto the perp space
    """
    pg = g.copy()
    for p in graph.nodes():
        pp = np.dot(proj, p)
        pg.node[p]['proj'] = pp
        
    return pg

def plot(pgraph, w):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'proj')
    plt.axes().set_aspect('equal')
    nx.draw_networkx(pgraph, dict_pos, with_labels = True, node_size= 30, width=w)        


N = 2
g = nx.grid_graph([N,N,N,N])
pg = proj_graph(mperp, g)
plot(pg, 1)

#pgAB = proj_graph(mperpAB, g)
#plot(pgAB, 2)