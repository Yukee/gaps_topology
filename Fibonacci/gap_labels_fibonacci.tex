\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english, french]{babel} %français
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{mathtools} %dcases
\usepackage{braket} %quantum mechanics
\usepackage[colorlinks=true, linkcolor=black, citecolor=black]{hyperref} % hyperlinks
\usepackage{tikz} % drawing in LaTeX

% the equal sign I use to define something
\newcommand{\define}{\ensuremath{ \overset{\text{def}}{=} }}

% differential element
\renewcommand{\d}[1]{\mathrm{d}#1}

% trace
\DeclareMathOperator{\tr}{tr}

% multiplicative commutator
\usepackage{physics}

% theorems
\newtheorem{theo}{Theorem}

\newcommand{\gv}{\ensuremath{\mathbf{g}}}
\newcommand{\sub}{\ensuremath{\Omega}}

\title{\textbf{Gap labels}}
\author{}
\date{}
\begin{document}

\selectlanguage{english}

\maketitle

In the gaps, the integrated density of states can only take values in the set $\omega \mathbb{Z} + \mathbb{Z} \cap [0,1]$.
This is the well known gap labelling theorem, applied to the Fibonacci chain.
Said differently, we can write the idos in a gap in the form
\begin{equation}
	g(q) = \omega q \mod 1 = \omega q + p
\end{equation}
were $q$ is an integer, labelling the gap in which the idos takes the value $g$.

For the n$^\text{th}$ approximant, the gap labelling becomes
\begin{equation}
	g_n(q) = \omega_n q + p
\end{equation}
with $\omega_n = F_{n-1}/F_ n$, and $q \in [1, F_n)$.
As we have seen, in the strong modulation limit the spectrum has a hierarchical, ternary tree structure, and therefore the gaps have this structure as well.
Indeed, if we call $G_{n}$ the set of gap values for the n$^\text{th}$ approximant, then we have respectively for the set of gap values in the bonding/atomic/antibonding clusters:
\begin{align}
\label{eq:recur}
	G_n^- &= d_-(G_{n-2}) = \frac{F_{n-2}}{F_n} G_{n-2} \sim \omega^2 G_{n-2} \\
	G_n^0 &= d_0(G_{n-3}) = \frac{F_{n-3}}{F_n}G_{n-3} + \frac{F_{n-2}}{F_n} \sim \omega^3 G_{n-3} + \omega^2 \\
	G_n^+ &= d_+(G_{n-2}) = \frac{F_{n-2}}{F_n} G_{n-2} + \frac{F_{n-1}}{F_n} \sim \omega^2 G_{n-2} + \omega
\end{align}
These relations have a geometrical interpretation that can be best seen if we replace the gap $g_n(q) = \omega_n q + p$ by the vector $\gv = (p, q)$. Then the above relations are replaced by the affine transformations
\begin{align}
	d_-(\gv) &= \sub^{-2} \gv \\
	d_0(\gv) &= \sub^{-3} \gv + (1,-1) \\
	d_+(\gv) &= \sub^{-2} \gv + (0, 1)
\end{align}
where $\sub$ is nothing but the substitution matrix
\begin{equation}
	\sub = 
	\begin{bmatrix}
		1 & 1\\
		1 & 0\\
	\end{bmatrix}
\end{equation}
that generates the Fibonacci sequence by acting repetitively of the letters $A$ and $B$.

Some of the $F_n -1$ gaps of the n$^\text{th}$ approximant are \emph{transient}: they will close in the quasiperiodic limit $n \to \infty$.
The gap at $E = 0$ zero that appears when $F_n$ is even is an example of such a transient gap.
The non-transient gaps we call \emph{stable}. Once they appear, they stay open all the way to the quasiperiodic limit. 
The two main gaps that separate the molecular clusters from the atomic cluster are examples of such stable gaps.
Let us stress that although the value of the idos inside a given stable gap varies with the size of the approximant (but converges in the quasiperiodic limit), its gap label $(p,q)$ is \emph{independant of the size of the approximant}, and equal to the value we would have found for the infinite quasiperiodic system. 
On the contrary, the gap label of a transient gap varies with the size of the approximant. 
Also note that whereas the width of the transient gaps goes to zero in the quasiperiodic limit, their fraction stays finite. It converges to $(4 + 3 \omega)/(18+11\omega) \simeq 0.24$.

From the recursive construction of the gap labels \eqref{eq:recur}, it is apparent that the stable gaps are the iterates of the two main gaps, while the transient gaps are the iterates of the $E = 0$ gap.
The number of transient gaps obey the recurrence relation
\begin{align}
	t_n = 2 t_{n-2} + t_{n-3} \\
	t_1 = 1,~t_2 = 0,~t_3 = 2.
\end{align}
While for the number of stable gaps we have
\begin{align}
	s_n+1 = 2 (s_{n-2}+1) + s_{n-3}+1 \\
	s_1 = 0,~s_2 = 2,~s_3 = 2.
\end{align}
Asymptotically, $t_n/(s_n + t_n) \to (4 + 3 \omega)/(18+11\omega) \simeq 0.236068$: almost one fourth of the gaps are transient in the quasiperiodic limit.

TODO: 
\begin{itemize}
	\item Peut-être peut-on calculer quelques quantités explicitement ? Par exemple le nombre de gaps transitoires et stables à l'étape $n$, ou alors leur distribution en fonction de $q$ (je m'attends à ce que les gaps stables soient pour $q$ petit et les gaps transitoires pour $q$ grand, peut être même y a-t-il un $q$ critique (augmentant avec $n$) au-delà duquel tous les gaps sont transitoires ?).
	\item On peut sans doute faire de jolies figures, comme celle ci-dessous, que je ne sais pas comment interpréter...
\end{itemize}
\begin{figure}[htp]
\centering
\includegraphics[scale=0.70]{img/q_gq.pdf}
\caption{The $(q, g_n(q))$ plane for an approximant ($\rho = 0.5$). The dots actually form a square lattice tilted by $\omega_n$. The size of a dot is proportionnal to the $\log$ of the width of the corresponding gap. Blue: stable gaps, red: transient gaps.}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=1.5]{img/gap_labels.pdf}
\caption{A numerically computed approximant spectrum, with its gaps labelled. Blue: stable gaps, red: transient gaps.}
\label{}
\end{figure}
\end{document}