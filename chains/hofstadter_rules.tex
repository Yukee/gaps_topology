\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english, french]{babel} %français
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{mathtools} %dcases
\usepackage{braket} %quantum mechanics
\usepackage[colorlinks=true, linkcolor=black, citecolor=black]{hyperref} % hyperlinks
\usepackage{tikz} % drawing in LaTeX

% the equal sign I use to define something
\newcommand{\define}{\ensuremath{ \coloneqq }}

% differential element
\renewcommand{\d}[1]{\mathrm{d}#1}
% partial derivative
\newcommand{\rond}[1]{\ensuremath{\partial_{#1}}}

% trace
\DeclareMathOperator{\tr}{tr}

% multiplicative commutator
\usepackage{physics}

% shorcut letters
\newcommand{\lb}{\ensuremath{\lambda}}
\newcommand{\om}{\ensuremath{\omega}}
\newcommand{\eps}{\ensuremath{\varepsilon}}
\newcommand{\Zn}[1]{\ensuremath{\mathbb{Z}^{#1}}}
\renewcommand{\alph}{\ensuremath{\mathcal{A}}}

% theorems
\newtheorem{df}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{rk}{Remark}
\newtheorem{ex}{Example}
\newtheorem{proof}{Proof}

\title{\textbf{Hofstadter rules}}
\author{Nicolas Macé}
\date{September 12 2016}
\begin{document}

\selectlanguage{english}

\maketitle

\textbf{Introduction:} the Hofstadter rules have been introduced by Douglas Hofstadter to account for the fractality of his butterfly.
They state that the gaps of the spectrum of flux $\phi$ organize in three energy clusters, and each of the three clusters is again organized in the same fashion, but corresponds to another effective flux.
Here we wonder how this rules may apply to canonical cut and project chains, which are very similar in structure to the Harper model.

\section{Cut and project words, circle sequences and the Harper model}

\begin{df}[Canonical cut and project method for chains]
The canonical cut and project method of slope $\alpha$ and intercept $\rho$ is the $2 \to 1$ cut and project method such that the window is the projection of a unit cube. We choose the open-closed interval by convention.
\end{df}

The cut and project word $w = \dots w_0 w_1 \dots$ of slope $\alpha$ and of intercept $\rho$ can be constructed by the \emph{circle sequence}:
\begin{equation}
\label{eq:circlemap}
	w_n = \lfloor \frac{n+1 - \rho}{1+\alpha} \rfloor - \lfloor \frac{n - \rho}{1+\alpha} \rfloor
\end{equation}
Which we can equivalently write
\begin{equation}
\label{eq:cosinemap}
	w_n = \left( \text{Sign}\left( \chi_n(\alpha) \right) + 1 \right) /2
\end{equation}
with the convention that $\text{Sign}(0) = 0$, and with 
\begin{equation}
	\chi_n(\alpha) = \cos(2\pi\frac{n-\rho}{1+\alpha} + \frac{\pi}{1+\alpha}) - \cos(\frac{\pi}{1+\alpha}).
\end{equation}

\begin{proof}
For simplicity we assume $\rho = 0$.
A point of the lattice writes $p = (n_x, n_y)$ in the canonical basis of $\mathbf{Z}^2$. 
Let $n = n_x+n_y$.
$p$ is in the selected band iff $n_x(1+\alpha) \leq n < (n_x+1)(1+\alpha)$, with $n = n_x+n_y$.
This is equivalent to $n_x = \lfloor n/(1+\alpha) \rfloor$.
Note that $n$ can serve as a labeling of the sites on the chain. 
It is the standard (real space) labeling.
When $n \to n+1$, then either $n_x$ or $n_y$ increases by 1. 
This translates into the previous function \eqref{eq:circlemap} taking value 1 ($n_x$ has increased) or 0 ($n_y$ has increased).
\end{proof}

Consider the Harper model of flux $\phi$.
The potential takes the value
\begin{equation}
	V_n(\phi;0) = \lambda \cos( \phi n + \delta)
\end{equation}
On the other hand, for the cut and project on-site model, the potential takes the value
\begin{equation}
	V_n(\alpha;\infty) = 2 \lambda (w_n - 1/2)
\end{equation}
Considering \eqref{eq:cosinemap} is it evident that the potential 
\begin{equation}
	V_n(\alpha;\beta) = \tanh(\beta \chi_n(\alpha))
\end{equation}
interpolates between the two models, provided that $\alpha = (1-\phi)/\phi$.

\section{The Hofstadter rules}
The Hofstadter rules apply to the Harper model, whose potential is given above.
They state that the gaps of the spectrum of flux $\phi$ organize in three energy clusters, and each of the three clusters is again organized in the same fashion, but corresponds to another effective flux.

Consider a flux $0 \leq \phi \leq 1$.
The new fluxes are given by
\begin{align}
	R_s(\phi < 1/2) & = \left\{ \frac{1}{\phi}\right\} \\
	R_s(\phi > 1/2) & = R_s(1-\phi)
\end{align}
for the side clusters, and by
\begin{align}
	R_c(\phi < 1/2) & = \left\{ \frac{\phi}{1-2\phi}\right\} \\
	R_c(\phi > 1/2) & = R_c(1-\phi)
\end{align}
for the central cluster.

The rules are reminiscent of the Gauss application (actually $\phi < 1/2 \to R_s(\phi)$ \emph{is} the Gauss application), used to compute the coefficients of the continued fraction of a number. 
It is thus natural to try writing the rules in terms of the continued fraction expansion of the flux.
Let $[a_1, a_2,\dots]$ be the continued fraction expansion of $\phi$, ie $\phi = 1/(a_1 + 1/(a_2 + \dots))$ (we omit the $a_0$ term since we assume $\phi \leq 1$).
In terms of the continued fraction, \textbf{assuming it is infinite ie the flux is irrational}, the rules write
\begin{align}
\label{eq:continuedrules}
	R_s & = 
	\begin{dcases*}
		[a_2,\dots] & if $a_1 > 1$ \\
		[a_3,\dots] & if $a_1 = 1$
	\end{dcases*} \\
	R_c & = 
	\begin{dcases*}
		[a_1-2,\dots] & if $a_1 > 2$ \\
		[a_3,\dots] & if $a_1 = 2$ \\
		[a_2-1,\dots] & if $a_1 = 1$ and $a_2 > 1$ \\
		[a_4,\dots] & if $a_1 = 1$ and $a_2 = 1$
	\end{dcases*}
\end{align}

\section{The rules applied to quasiperiodic chains and their approximants}
Consider first the Fibonacci chain, ie $\alpha = \omega = (\sqrt{5}-1)/2$.
We have $\phi = 1/(1+\alpha) = \omega$.
Since, $\phi = \omega = [\underline{1}]$, $R_c(\phi) = R_s(\phi) = \phi$. 
This case is a fixed point of the RG.
The Hofstadter rules predict the cluster will have the Fibonacci structure again. This is indeed what we observe for the Fibonacci quasiperiodic chain.

The only other fixed point of the RG is $\phi = [\underline{2}]$ ie $\phi = \sqrt{2} -1$.
Consider the slope $\alpha$ such that $\phi = \sqrt{2} -1$, ie $\alpha = \sqrt{2}$.
We predict this slope will yield quasiperiodic chains of perfect scale-invariant clusters.

Going back to the Fibonacci case, we now consider approximants 
\begin{equation}
	\alpha_l = \frac{F_{l-2}}{F_{l-1}}
\end{equation}
We have thus $\phi_l = F_{l-1}/F_l$.
$\phi_l > 1/2$ for $l$ large enough, and $1-\phi_l = F_{l-2}/F_l$.
Thus $R_s(\phi_l) = F_{l-3}/F_{l-2}$, which is expected.
In the same way, $R_c(\phi_l) = F_{l-4}/F_{l-3}$, also as expected.
\textbf{Can we conclude that the rules extend to the case of the approximants, in general?}
Further tests are needed.

\end{document}