\documentclass[11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english, french]{babel} %français
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{mathtools} %dcases
\usepackage{braket} %quantum mechanics
\usepackage[colorlinks=true, linkcolor=black, citecolor=black]{hyperref} % hyperlinks
\usepackage{tikz} % drawing in LaTeX

% the equal sign I use to define something
\newcommand{\define}{\ensuremath{ \coloneqq }}

% differential element
\renewcommand{\d}[1]{\mathrm{d}#1}
% partial derivative
\newcommand{\rond}[1]{\ensuremath{\partial_{#1}}}

% trace
\DeclareMathOperator{\tr}{tr}

% multiplicative commutator
\usepackage{physics}

% shorcut letters
\newcommand{\lb}{\ensuremath{\lambda}}
\newcommand{\om}{\ensuremath{\omega}}
\newcommand{\eps}{\ensuremath{\varepsilon}}
\newcommand{\Zn}[1]{\ensuremath{\mathbb{Z}^{#1}}}
\renewcommand{\alph}{\ensuremath{\mathcal{A}}}

% theorems
\newtheorem{df}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{rk}{Remark}
\newtheorem{ex}{Example}

\title{\textbf{Aperiodic chains}}
\author{Nicolas Macé}
\date{June 24 2016}
\begin{document}

\selectlanguage{english}

\maketitle

\textbf{Motivation:} comparing behavior of periodic and aperiodic systems (with a focus on quasiperiodic ones), to have a general view of the situation and gain some insights.

\section{Complexity and sturmian words}

\begin{df}[Complexity function]
	Let $\om$ be an (infinite) word. Its complexity function is 
	\begin{equation}
		P_\om(n) \define \#\{\text{subwords of length}~n \in \om \}
	\end{equation}
\end{df}
Obviously the complexity function cannot grow faster than $C^n$ if $C$ is the number of letters in the alphabet, but how slowly can it grow?
To answer the question, we consider an infinite (or bi-infinite) word, and we construct the \emph{tree of words}: a tree graph constructed as follows.
Vertices at level $n$ are the subwords of length $n$. A word $v$ of the level $n-1$ shares an edge with a word $w$ of the level $n$ if $w$ can be constructed by adding one letter to the right of $v$.

\begin{figure}[htp]
\centering
	\begin{tikzpicture}[scale=.35,
    grow=down,
  	level 1/.style={sibling distance=8cm, level distance=3cm}, 
    level 2/.style={sibling distance=4cm, level distance=3cm},
    level 3/.style={sibling distance=3.5cm, level distance=3cm}]
    	 
    % style of the vertices
    \tikzstyle{vertex}=[circle,fill,scale=0.4]
    	 
	\node [vertex, label=right:$\emptyset$] {}
		child {node [vertex, label=right:A] {}
			child {node [vertex, label=right:AA] {}
				child {node [vertex, label=right:AAB] {} }}
			child {node [vertex, label=right:AB] {}
				child {node [vertex, label=right:ABA] {} }}}
		child {node [vertex, label=right:B] {}
			child {node [vertex, label=right:BA] {}
				child {node [vertex, label=right:BAA] {} }
				child {node [vertex, label=right:BAB] {} }}};
    		
	\end{tikzpicture}
	\caption{The first levels of the Fibonacci tree of words.}
\end{figure}

Obviously, each vertex has at least one child.
Now let $\om$ be an infinite word, and consider its tree.
Suppose at level $n$ ($n=0$ is the root of the tree), each vertex has only one child. This means there is only one way of adding a letter to the right of a subword of the length $n$.
But then, there is also only one way to add a letter to the right of a subword of the length $n+1$. 
By recursion, this is true at each level $m \geq n$, and the tree does not grow starting from level $n$. Thus, $\om$ is periodic of period $n$.
Conclusion:
\begin{theorem}
A word $\om$ is aperiodic iff its complexity function grows at least linearly. More precisely, $P_\om(n) \geq n+1$.
\end{theorem}
Now, can we characterize the words which are the closest to being periodic, ie whose complexity function has the slowest growth? These are called the \emph{Sturmian words}, and it turns out that
\begin{theorem}
The Sturmian words, ie the words such that $P_\om(n) = n+1$ are exactly the canonical cut and project words with irrational slope.
\end{theorem}
By canonical cut and project, we mean cut and project from $\Zn{2}$ to $\mathbb{R}$, whose window is the unit cube of $\Zn{2}$.

It is easy to see that the canonical cut and project words have indeed linear growth.
Take an irrational number, and consider the word produced by canonical cut and project. 
Let us set the origin of our chain at the (unique) point where $\Zn{2}$ intersects the slope.
To enumerate the possible subwords of length $n$, we translate the slope along its normal direction, and consider the set of words of length $n$ that appear on the right of the point we initially defined as our origin. 
This set is exactly the set of words of length $n$, because translating the slope while keeping the origin fixed amounts to translating the origin while keeping the slope fixed.

When we translate the slope, we pass from one word to the next by a \emph{phason flip}. 
Two phasons flips can never occur at the same time, since the slope is irrational.
After having translated the slope along one unit cube, we are back to the original setup, and in the interval of $n$ letters we consider, $n+1$ phason flips have occured, yielding at most $n+1$ different words of length $n$. Thus the complexity is at most $n+1$. It cannot be less, because the word is aperiodic. We conlude it is exactly $n+1$.

\begin{rk}
Among the $n+1$ words of length $n$ of a Sturmian word, $n$ share the same number of $A$ and $B$ letters (because they are linked to each other by phason flips \emph{inside} the interval of $n$ letters).
One has one less $A$ and one more $B$ (or the contrary) than the others (because it is linked to the others by a phason flip at the boundary of the interval of $n$ letters).
I think this peculiar word is one of the two words which are branching from a single parent in the tree of words. Said differently, it is the word responsible for the aperiodicity of the Sturmian word, the one which would not appear in a periodic approximant.
\end{rk}

Conversely, if a word is Sturmian, then we can compute its slope $\alpha = \# B/ \# A$, and build a cut and project model with this slope outputting this word. 
It remains to show that it is canonical, ie that the window is the unit cube. 
First, the window cannot be thinner than a unit cube. If it is thicker, then the complexity is larger than $n+1$.

\begin{rk}
In two dimensions, the complexity of aperiodic word grows at least quadratically, which seems natural. But surprisingly, in more than two dimensions, it also grows at least quadratically! This is the counterintuive result of \cite{Cassaigne2006}.
\end{rk}

\section{Sturmian words and continued fractions}

\subsection{Generalities on continued fractions}

We introduce the Gauss application:
\begin{equation}
	T(x) \define \left\{ \frac{1}{x} \right\}
\end{equation}
where the brackets means taking the fractional part.
Let $\alpha \in [0,1]$. Define the sequence of integers
\begin{equation}
	a_n \define \left\lfloor \frac{1}{T^n(\alpha)} \right\rfloor
\end{equation}
We are going to show that this sequence is the sequence of coefficients appearing in the continued fraction of $\alpha$: 
\begin{equation}
	\alpha = [0;a_0, a_1, \dots]
\end{equation}
Indeed, since
\begin{equation}
	\frac{1}{\alpha} = \left\lfloor \frac{1}{\alpha} \right\rfloor + \left\{ \frac{1}{\alpha} \right\}
\end{equation}
then 
\begin{equation}
	\alpha = \frac{1}{a_0 + T(\alpha)}
\end{equation}
and the rest follows by induction.

The continued fraction is finite for $\alpha$ rational, and infinite for $\alpha$ irrational. 
It is infinite, but periodic if and only if $\alpha$ is a \emph{reduced quadratic irrational}, ie a quadratic irrational in $[0,1]$ whose algebraic conjugate is lower than $-1$. 
It is infinite, but ultimely periodic, if and only if $\alpha$ is a quadratic irrational.


\subsection{Continued fractions and substitutive words}

Let us start by a simple observation. 
Let $\sigma$ be a substitution over a 2 letters alphabet, and let $M_\sigma$ be the associated substitution matrix.
If
\begin{equation}
	\lambda 
	\begin{pmatrix}
	1 \\ 
	\beta
	\end{pmatrix} 
	=
	M_\sigma
	\begin{pmatrix}
	1 \\ 
	\alpha
	\end{pmatrix},
\end{equation}
then a word of slope $\alpha$ is transformed by the substitution in a word of slope $\beta$. If $\lambda$ is not 1, the intercept may also be modified, but we will not care about that here.

Now, the link between continued fraction and substitutions stems from the fact that the Gauss application can be written in the form
\begin{equation}
	\frac{1}{\alpha} 
	\begin{pmatrix}
	1 \\ 
	\alpha
	\end{pmatrix}
	=
	\begin{bmatrix}
	 a_0 & 1 \\ 
	 1 & 0
	 \end{bmatrix}  
	 \begin{pmatrix}
	1 \\ 
	T(\alpha)
	\end{pmatrix} 
\end{equation}
The matrix appearing here can be interpreted as the substitution matrix $M_0$ of the substitution
\begin{equation}
	\beta(a_0): \left\{\begin{array}{ll} A \to & A^{a_0} B \\ B \to & A
	\end{array}\right.	
\end{equation}
Then, by induction,
\begin{equation}
	\frac{1}{\Pi_{n=0}^{N-1} T^n(\alpha)}
	\begin{pmatrix}
	1 \\ 
	\alpha
	\end{pmatrix}
	=
	\Pi_{n=0}^{N-1} M_n
	\begin{pmatrix}
	1 \\ 
	T^N(\alpha)
	\end{pmatrix} 
\end{equation}
Now, if the continued fraction of $\alpha$ is periodic, say of the form $[\underline{a_0, a_1, \dots a_{k-1}}]$ with the underlined bit repeated, then $T^{N}(\alpha)$ is also periodic of period $k$, and the sturmian word $\alpha$ is the fixed point of the substitution $\beta(a_0) \circ \dots \circ \beta(a_{k-1})$.
We thus conclude that sturmian words whose slope is a reduced quadratic irrational are substitutive.

\begin{ex}
Consider the reduced quadratic sturmian word $\alpha = \sqrt{2} -1$.
Its continued fraction is $[0;\underline{2}]$, and therefore the associated substitution is
\begin{equation}
	\sigma = \beta(2): \left\{\begin{array}{ll} A \to & A A B \\ B \to & A
	\end{array}\right.
\end{equation}
We readily check that the associated cut and project method whose $n^\text{th}$ letter is $\lfloor \frac{n+1}{1+\alpha} \rfloor - \lfloor \frac{n}{1+\alpha} \rfloor$ with 0 identified with $B$ and 1 identified with $A$ produces indeed the same infinite word.
\end{ex}

\begin{ex}
Consider the reduced quadratic sturmian word $\alpha = \sqrt{3} -1$.
Its continued fraction is $[0;\underline{1,2}]$, and therefore the associated substitution is
\begin{equation}
	\sigma = \beta(2) \circ \beta(1): \left\{\begin{array}{ll} A \to & A B A B A \\ B \to & A B
	\end{array}\right..
\end{equation}
\end{ex}

\begin{ex}
Consider the quadratic sturmian word $\alpha = 1/\sqrt{2}$.
Its continued fraction is $[0;1,\underline{2}]$. It is not a reduced quadratic irrational, and as such its continued fraction is ultimely periodic, but not periodic. 
Thus, we cannot build a substitution in the same way as before.
What we can do to generate this word is perform the substitution
\begin{equation}
	\sigma = \beta(2): \left\{\begin{array}{ll} A \to & A A B \\ B \to & A
	\end{array}\right.,
\end{equation}
and then apply once the substitution
\begin{equation}
	\phi = \beta(1): \left\{\begin{array}{ll} A \to & A B \\ B \to & A
	\end{array}\right.
\end{equation}
to the fixed point word. Said differently the morphism $\phi \circ (\sigma)^\infty$ produces the sturmian word $\alpha = 1/\sqrt{2}$. However, it is not a substitution. 
I do believe that there exist a substitution which produces the same word, as $\alpha$ is a Sturm number.  
\end{ex}

\textbf{Read notes by Thomas Haines on sturmian words and the repetitivity function}

\bibliographystyle{plain}
\bibliography{bib}

\section{Some mathematical vocabulary}

A lot of math has been done about aperiodic sequences. Therefore it may be useful to understand some mathematical definitions.

\subsection{Shifts and subshifts}

\begin{df}[Full shift]
Let $\alph$ be a finite alphabet (a finite collection of letters). 
The full shift associated to this alphabet is the collection of infinite words (noted $\mathbb{Z}^\alph$), or semi-infinite words (noted $\mathbb{N}^\alph$) together with a topology (see below).
\end{df}

To full shift comes with the topology of ``approximate match'':
Two words are close if their letters agree on a large distance from the origin.

\begin{df}[Topology of the full shift]
Let $\Omega = \mathbb{N}^\alph$. Let $\om_1,~\om_2 \in \Omega$.
Let $n$ be the maximum number of letters $\om_1$ and $\om_2$ have in common, starting from their leftmost letter. 
Then the distance between the two words is $d(\om_1, \om_2) = 2^{-n}$.
This defines the topology of the full shift. Alternatively, one can define it as the product topology of the discrete topology on $\alph$.
One can similarly define a distance and a topology in the case $\Omega = \mathbb{Z}^\alph$.
\end{df}

With a topology, we have a notion of convergence. For example the sequence $(01)^n$ (the $01$ bit is repeated $n$ times) converges to $(01)^\infty$ as $n \to \infty$.
It is with this convergence in mind that mathematicians say that the Fibonacci word is the fixed point of the Fibonacci substitution.

\begin{df}[Subshift]
\end{df}

\begin{df}[Subshift of finite type (SFT)]
\end{df}

\begin{df}[Sofic shift]
\end{df}

\subsection{Words}

\begin{df}[Sturmian word]
A Sturmian word $\om$ is an infinite word on a two letters alphabet, which has one of the following equivalent properties:
\begin{enumerate}
	\item Its complexity function is $P_\om(n) = n + 1$.
	\item It is built from the canonical cut and project method (by canonical we mean: $\mathbb{Z}^2 \to \mathbb{R}$ with the window being the unit square).
	\item It is built from a circle sequence, ie the $n^\text{th}$ letter is given by $\lfloor \frac{n+1 - \rho}{1+\alpha} \rfloor - \lfloor \frac{n - \rho}{1+\alpha} \rfloor$ with $\alpha$ and $\rho$ being the slope and intercept of the cut and project method.
\end{enumerate}
\end{df}

The sturmian words are thus the aperiodic sequences with the lowest possible complexity. 
There are closely related to substitutive sequences, via the concept of Sturm numbers:

\begin{df}[Sturm number]
	$\alpha$ is a Sturm number if it is a quadratic irrational, such that $\alpha^* \notin [0,1]$.
\end{df}

\begin{theorem}[Substitutive sturmian words]
Let $\om$ be a sturmian word, and let $\alpha$ be its slope (ie the slope of the associated cut and project method, ie the ratio of the frequencies of letters $\# B / \# A$ in $\om$).
$\om$ is \emph{substitutive} (ie is the fixed point of a substitution) if and only if $\alpha$ is a Sturm number, and its intercept is in the field $\mathbb{Q}(\alpha)$.
\end{theorem}

\begin{ex}[A sturmian word with non-Sturm slope is not substitutive]
Consider the following minimal polynomial over rationals:
\begin{equation}
	P(X) = 8 X^2 - 8X +1
\end{equation}
The two roots are $\alpha, \alpha^* = \frac{2 \pm \sqrt{2}}{4}$. There are in the $[0,1]$ interval, and are therefore not Sturm numbers. Thus the associated cut and project words are not substitutive.
\end{ex}

It is however wrong that all substitutive words are sturmian. 
\begin{ex}[A substitutive non-sturmian word]
Consider the substitution
\begin{equation}
	\sigma: \left\{\begin{array}{ll} A \to & ABBB \\ B \to & A
	\end{array}\right.	
\end{equation}
The slope is $\alpha = \# A / \# B = (1+\sqrt{13})/2$, whose algebraic conjugate is $\alpha^* = (1-\sqrt{13})/2 < 0$. So indeed, $\alpha$ is a Sturm number. However, since it is not a Pisot number, the substitution yields an infinite word of complexity growing faster than linearly. It is thus not a sturmian word. 
\end{ex}

\end{document}