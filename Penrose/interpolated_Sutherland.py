# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 19:10:42 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
# interpolation of irregularly spaced points
from scipy.interpolate import griddata
from matplotlib import colors
from colour import Color


sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]

# fat rhombus
shapes0 = loz(0j, e[0], e[3])
## thin rombus
#shapes0 = loz(0j, e[0], e[1])

# Create a fivefold flower of fat rhombuses
shapes0 = []
for i in range(5):
    C = 0j
    A = np.exp(1j*2*i*np.pi/5)
    B = np.exp(1j*2*i*np.pi/5)+np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))
    A = np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))

# construct and iterate the tiling
sys = tl.Penrose(shapes0)
n = 7
sgn = (-1)**n # sign of the arrows
sys.it_sub(n)
# weighted graph whose weight if the height field
hGraph = sys.integrate_arrow_field(0j, arrow_field="double_arrow")
height = nx.get_node_attributes(hGraph, "weight")
heights = [height[p] for p in hGraph]
# wavefunction
rho = .8
wf = np.array([rho**(sgn*height[p]) for p in hGraph])
norm = np.sum(wf**2.)
wf /= norm

def plotpts(bounds):
    ((x0, y0), L) = bounds
    def inreg(px, py):
        return x0-L/2 <= px <= x0+L/2 and y0-L/2 <= py <= y0+L/2
    # select points inside the region
    x = []
    y = []
    z = []
    for p, coeff in zip(hGraph, wf):
        px = p.real
        py = p.imag
        if inreg(px, py):
            x.append(px)
            y.append(py)
            z.append(coeff)    
    return x, y, z

def plot(bounds, x, y, z, savename, colorscheme):
    ((x0, y0), L) = bounds
    # number of points in each direction for interpolation
    N = int(400*L)
#    print(N)
#    N = 300
    # define grid.
    xi = np.linspace(x0-L/2,x0+L/2,N)
    yi = np.linspace(y0-L/2,y0+L/2,N)
    # grid the data.
    zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')
    # contour the gridded data, plotting dots at the randomly spaced data points.
    vmin = np.min(z)
    vmax = 0.8*np.max(z)
    eps = .1 # take a bit less for the interpolation to work well
    
    background = plt.figure()
    ax_back = background.add_subplot(111)
    ax_back.contourf(xi,yi,zi,15,cmap=colorscheme, vmin=vmin, vmax=vmax)
        
    edges = sys._graph.edges()
    ei_x = []
    ei_y = []
    ef_x = []
    ef_y = []
    for ei, ef in np.array(edges):
        eix = ei.real
        eiy = ei.imag
        efx = ef.real
        efy = ef.imag
        ei_x.append(eix)
        ei_y.append(eiy)
        ef_x.append(efx)
        ef_y.append(efy)
    pos = np.array([[p.real, p.imag] for p in sys._graph])
    plt.plot((ei_x, ef_x), (ei_y, ef_y), linestyle='-', color='white', linewidth=1., alpha = .6)
#    plt.plot(*(pos.T),'o', ms=.5, color="white", alpha = 1.)
    plt.scatter(*(pos.T), c='white', edgecolor='', marker='o', s=10.)
    
#    # set limits
    plt.xlim([x0-L/2+eps, x0+L/2-eps])
    plt.ylim([y0-L/2+eps, y0+L/2-eps])
    # equal aspect ratio, no axes
    plt.axes().set_aspect('equal')
    plt.axis('off')
    
    plt.savefig(savename + ".svg", dpi=200, transparent = True)
    plt.close()
    
#def plot_graph():
#    c = 'white'
#    pos = [(p.real, p.imag) for p in sys._graph]
#    node_pos = dict(zip(sys._graph, pos))
#    nx.draw_networkx(sys._graph, node_pos, node_size = 10., width = 1., node_color=c, edge_color=c, with_labels=False)
    
#def plot_graph():
#    edges = sys._graph.edges()
#    edges_x = []
#    edges_y = []
#    for ei, ef in np.array(edges):
#        eix = ei.real
#        eiy = ei.imag
#        efx = ef.real
#        efy = ef.imag
#        edges_x.append((eix, efx))
#        edges_y.append((eiy, efy))
#    pos = np.array([[p.real, p.imag] for p in sys._graph])
#    plt.plot(edges_x, edges_y, linestyle='-', color='white', alpha = 1.)
#    plt.plot(*(pos.T), 'o', color="white", alpha = 1.)
#

"""
custom cm
"""

yellow = Color("yellow")
black = Color("black")
cols = list(black.range_to(yellow ,100))
cols = [c.rgb for c in cols]
YB = colors.ListedColormap(cols, name='YB', N=None)

#reef = Color((184, 221, 137))
reef = Color("#B8DD89")
cols = list(black.range_to(reef ,100))
cols = [c.rgb for c in cols]
Reef = colors.ListedColormap(cols, name='Reef', N=None)

"""
plots!
"""



rf = np.array([0., 0.])
Lf = 1.5
bounds = (rf, Lf)

x, y, z = plotpts(bounds)
plot(bounds, x, y, z, "tim", plt.cm.viridis)