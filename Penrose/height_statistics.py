# -*- coding: utf-8 -*-
"""
Created on Wed May 18 17:14:57 2016

@author: nicolas
"""

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import collections


""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph


sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]
    
class HeightDistribution:
    """
    A container class to store the height distribution of the AB tiling.
    It should be initialized with a dictionary whose keys are the initial heights, and values the corresponding frequencies (not necessarily normalized).
    
    The freq(h) method returns an array whose ith components is proportional to the frequency of the event "env i with height h".
    NOTE: you must normalize the output of freq(h) to get the frequencies!!
    
    Environments are ordered by increasing distance from the center of the window: {0:A, 1:B, 2:C, 3:D1, ...7:F}.
    The evolve method updates the height distribution as if two inflations steps were performed.
    """
    def __init__(self, heightToFreq0):
        self._htf = heightToFreq0 # keys:heights, values: np array of frequencies having this height
        h0 = heightToFreq0.keys()
        self._hMin = min(h0)
        self._hMax = max(h0)
        
        self._a0 = np.array([[1,0],[2,0]])
        self._a1 = np.array([[3,1],[1,2]])
        self._a2 = np.array([[1,2],[0,0]])

        self._parity = 0
        
    def freq(self, height):
        freq = np.zeros(2)
        if self._hMin <= height <= self._hMax:
            freq = self._htf[height]
            
        return freq
            
    def evolve_twice(self):
        newhtf = dict()
        for h in range(self._hMin, self._hMax+3):
            newhtf[h] = (np.dot(self._a0, self.freq(h)) + np.dot(self._a1, self.freq(h-1)) + np.dot(self._a2, self.freq(h-2)))
        self._htf = newhtf
        self._hMin -= 0
        self._hMax += 2
        
        return None
        
def hstat(tiling):
    """
    Compute the statistics of heights on the labeled sites (ie on the tiles, see Sutherland's article) on a given tiling
    """
    # compute the double and single arrow potentials
    wg_single_arrow = tiling.integrate_arrow_field(e[3], arrow_field='single_arrow')
    single_arrow = nx.get_node_attributes(wg_single_arrow, 'weight')
    wg_double_arrow = tiling.integrate_arrow_field(e[3], arrow_field='double_arrow')
    double_arrow = nx.get_node_attributes(wg_double_arrow, 'weight')
    
    # the list of double arrow potentials
    hvals = []
    for p in wg_double_arrow:
        # the labeled sites are the ones on which the single arrow potential is zero
        if single_arrow[p] == -1:
            hvals.append(double_arrow[p])
    
    return dict(collections.Counter(hvals))
    

# fat rhombus
shapes0 = loz(0j, e[0], e[3])

# construct and iterate the tiling
half_infs = 6
tiling = tl.Penrose(shapes0)

hvals = []
tiling.it_sub(0)
stat = hstat(tiling)
hvals.append(stat)
#tiling.it_sub(1)
for i in range(half_infs):   
    # inflate once the previous tiling
    tiling.it_sub(2)
    stat = hstat(tiling)
    hvals.append(stat)
    
initial_distro = {0:[1,0]}
hd = HeightDistribution(initial_distro)
nsites = []
diffs = []
for i in range(half_infs):
    hd.evolve_twice()
    distro = hd._htf
    nsites.append(sum(sum(distro[h]) for h in distro))
    
    stats = {h:np.sum(hd._htf[h]) for h in hd._htf}
    diff = sum((stats[h]-hvals[i+1][-h])**2 for h in stats)/sum(stats.values())**2
    diffs.append(diff)
