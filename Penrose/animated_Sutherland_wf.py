# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 13:39:44 2016

@author: nicolas

This script to generate an animated zoom on Sutherland's wavefunction, for Timothée's video.
"""


""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
# interpolation of irregularly spaced points
from scipy.interpolate import griddata
 
sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]

# fat rhombus
shapes0 = loz(0j, e[0], e[3])
## thin rombus
#shapes0 = loz(0j, e[0], e[1])

# Create a fivefold flower of fat rhombuses
shapes0 = []
for i in range(5):
    C = 0j
    A = np.exp(1j*2*i*np.pi/5)
    B = np.exp(1j*2*i*np.pi/5)+np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))
    A = np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))

# construct and iterate the tiling
sys = tl.Penrose(shapes0)
n = 10
sgn = (-1)**n # sign of the arrows
sys.it_sub(n)
# weighted graph whose weight if the height field
hGraph = sys.integrate_arrow_field(0j, arrow_field="double_arrow")
height = nx.get_node_attributes(hGraph, "weight")
heights = [height[p] for p in hGraph]
# wavefunction
rho = .8
wf = np.array([rho**(sgn*height[p]) for p in hGraph])
norm = np.sum(wf**2.)
wf /= norm

def plotpts(bounds):
    ((x0, y0), L) = bounds
    def inreg(px, py):
        return x0-L/2 <= px <= x0+L/2 and y0-L/2 <= py <= y0+L/2
    # select points inside the region
    x = []
    y = []
    z = []
    for p, coeff in zip(hGraph, wf):
        px = p.real
        py = p.imag
        if inreg(px, py):
            x.append(px)
            y.append(py)
            z.append(coeff)    
    return x, y, z

def plot(bounds, x, y, z, savename):
    ((x0, y0), L) = bounds
    # number of points in each direction for interpolation
    N = int(400*L)
#    print(N)
#    N = 300
    # define grid.
    xi = np.linspace(x0-L/2,x0+L/2,N)
    yi = np.linspace(y0-L/2,y0+L/2,N)
    # grid the data.
    zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')
    # contour the gridded data, plotting dots at the randomly spaced data points.
    vmin = np.min(z)
    vmax = np.max(z)
    eps = .06 # take a bit less for the interpolation to work well
    plt.contourf(xi,yi,zi,15,cmap=plt.cm.viridis, vmin=vmin, vmax=vmax)
        
#    # set limits
    plt.xlim([x0-L/2+eps, x0+L/2-eps])
    plt.ylim([y0-L/2+eps, y0+L/2-eps])
    # equal aspect ratio, no axes
    plt.axes().set_aspect('equal')
    plt.axis('off')
    
    plt.savefig(savename + ".png", dpi=200, transparent = True)
    plt.close()

ri = np.array([-0.19, 0.136])
Li = 0.4
rf = np.array([0., 0.])
Lf = 1.65

def bounds(t):
    r = (1.-t)*ri + t*rf
    L = (1.-t)*Li + t*Lf
    return (r, L)
    
ts = np.linspace(0., 1., 200.)
count = 0
for t in ts:
    x, y, z = plotpts(bounds(t))
    plot(bounds(t), x, y, z, str(count))
    count += 1

# For video output: mencoder "mf://%d.png" -mf fps=12:type=png -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=7000 -vf scale=1600:1200 -o invariance_echelle.avi


# region to plot
#x0 = -.6
#y0 = 0.22
#L = 0.8
#x0 = -.38
#y0 = -.37
#L = .8

