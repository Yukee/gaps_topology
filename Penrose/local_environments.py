# -*- coding: utf-8 -*-
"""
Created on Tue May 17 15:42:36 2016

@author: nicolas

Drawing Penrose's local environments
"""

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph


sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]
        
def weighted_graph(tiling, l, width, with_labels):
    """
    plot the arrowed graph, after l inflations
    """
    # inflate
    tiling.it_sub(l)
    wg = tiling.integrate_arrow_field(0j, arrow_field='double_arrow')
    
    # keep only edges that are directed in the positive direction
    if with_labels:        
        double = nx.get_node_attributes(wg,'weight')
#        wg_simple = tiling.integrate_arrow_field(0j, arrow_field='single_arrow')
#        simple = nx.get_node_attributes(wg_simple,'weight')
        labels = {node:double[node] for node in wg}
    else:
        labels = None
        
    # plot the weighted graph
    QGraph.plot(wg, weights = width, labels = labels)#, lims = ((-L,L),(-L,L)))
            
    return None

def env():
    shapes = []
    shapes += loz(-e[1], e[1], e[0])
    shapes += loz(0j, e[0], e[3])
    shapes += loz(0j, -e[1], e[3])
    
    return shapes    
    
shapes = [loz(0j, e[0], e[1]), loz(0j, e[0], e[3])]
names = ["thin", "fat"]

for shape, name in zip(shapes, names):
    tiling = tl.Penrose(shape)
    weighted_graph(tiling, 0, 1., True)
    weighted_graph(tiling, 2, 0.1, False)
    plt.savefig("env_"+name+"_infl.pdf")
    plt.close()
