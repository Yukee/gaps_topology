# -*- coding: utf-8 -*-
"""
Created on Sun Nov 27 17:22:57 2016

@author: nicolas

This script to plot Sutherland's wavefunction
Primary use: for Timothée's video
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
 
sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]

# fat rhombus
shapes0 = loz(0j, e[0], e[3])
## thin rombus
#shapes0 = loz(0j, e[0], e[1])

# Create a fivefold flower of fat rhombuses
shapes0 = []
for i in range(5):
    C = 0j
    A = np.exp(1j*2*i*np.pi/5)
    B = np.exp(1j*2*i*np.pi/5)+np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))
    A = np.exp(1j*2*(i+1)*np.pi/5)
    shapes0.append((1 , (A, B, C)))

# construct and iterate the tiling
sys = tl.Penrose(shapes0)
n = 6
sys.it_sub(n)
# weighted graph whose weight if the height field
hGraph = sys.integrate_arrow_field(0j, arrow_field="double_arrow")
height = nx.get_node_attributes(hGraph, "weight")
heights = [height[p] for p in hGraph]
# wavefunction
rho = .8
wf = np.array([rho**height[p] for p in hGraph])
norm = np.sum(wf**2.)
wf /= norm

# drawing the tiling
pos = {p:(p.real, p.imag) for p in hGraph}
nx.draw_networkx(hGraph, pos, with_labels=False, node_size=80, node_color=wf, cmap="viridis")
plt.axes().set_aspect("equal")