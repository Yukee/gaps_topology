# -*- coding: utf-8 -*-
"""
Created on Fri Nov 25 16:57:04 2016

@author: nicolas

This script colors a bipartite patch of the Penrose tiling, and counts the number of nodes in each of the two subgraphs.
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from networkx.algorithms import bipartite

sq5 = np.sqrt(5)
phi = (sq5 - 1.)/2.
pi = np.pi
e = np.exp(1j*pi*np.arange(5)/5)

def loz(orig, a, b):
    """ 
    create a lozenge with edges along a and b
    If thin (resp fat) lozenge is wanted, then (a, b) should be the acute (resp obtuse) angle of the lozenge
    """
    alpha = abs(np.angle(b/a))
    if np.around(alpha - pi/5, 2) == 0:
        color = 0
    elif np.around(alpha - 3*pi/5, 2) == 0:
        color = 1
    else:
        raise RuntimeError("The specified edges do not form an angle of pi/5 or 3*pi/5")

    A = orig
    B = orig + a
    C = orig + b

    return [(color, (A, B, C)), (color, (B + C - A, B, C))]

# fat rhombus
shapes0 = loz(0j, e[0], e[3])
# thin rombus
shapes0 = loz(0j, e[0], e[1])

# construct and iterate the tiling
sys = tl.Penrose(shapes0)
n = 5
sys.it_sub(n)
print(bipartite.is_bipartite(sys._graph))
Ga, Gb = bipartite.sets(sys._graph)
print((len(Ga) - len(Gb))/(len(Ga)+len(Gb)))

# drawing the tiling
pos = {p:(p.real, p.imag) for p in sys._graph}
nx.draw_networkx(sys._graph, pos, with_labels=False, node_size=0)
plt.axes().set_aspect("equal")