# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 11:35:42 2016

@author: nicolas

Plot the tiling resulting of the decimation of everything but one type of site.
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the other stuff """
import math, cmath
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx # graph tools
import QuantumGraph as QGraph

# construct square approximants to A5
sq2 = math.sqrt(2)
# start with a square
square0 = []
# half-length of the square
r = 1.
# first half-square
C = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, -3*math.pi/4)
B = cmath.rect(r, -5*math.pi/4)
square0.append((r, (A, B, C)))
# second half-square
C = cmath.rect(r, -5*math.pi/4)
B = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, math.pi/4)
square0.append((r, (A, C, B)))
square = tl.A5(square0)

"""
Label the sites according to their first neighbors environment
"""
# determine the local environments on the tiling
zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}
# return a dictionary pts to environment, given a graph
def pt_to_env(graph):
    # D1 and D2 are distinguished by their local environment
    ptToEnv = {}
    for pt in graph:
        n = graph.neighbors(pt)
        z = len(n)
        # if pt is a D type site, distinguish between D1 and D2
        if z == 5:
            # D2 sites have in their neighbourhood 2 F sites
            if 3 in [len(graph.neighbors(pn)) for pn in n]:
                ptToEnv[pt] = 'D2'
            else:
                ptToEnv[pt] = 'D1'
        elif 3 <= z <= 8:
            ptToEnv[pt] = zToEnv[z]
        else: # this can happen at the edges (eg if free bc are used we have sites of coordinance 2)
            ptToEnv[pt] = None
    return ptToEnv
    
""" 
Generate a new tiling containing only sites of type t, connected if they are nth nearest neighbors
"""
square.it_sub(5)
graph = square._graph
t = 'F'
k = 6

decimG = nx.Graph() # graph of the tiling after decimation over everyone except sites of type t
ptToEnv = pt_to_env(graph) # dict associating a node to its env
for p in graph.nodes():
    if ptToEnv[p] == t:
        neigh = QGraph.k_neighbors(graph, p, k)
        edges = [(p, n) for n in neigh if ptToEnv[n] == t]
        decimG.add_edges_from(edges)
        
QGraph.plot(decimG, savename = "env_"+t+"_k_"+str(k), dpi = 400)
"""
tests
"""

#p0 = (-0.05025253169416735+0.050252531694167296j)
#p1 = list(QGraph.k_neighbors(graph, p0, 20))[-4]
#
#neigh = QGraph.k_neighbors(graph, p0, 30) | QGraph.k_neighbors(graph, p1, 30)
#node_size = []
#for p in graph.nodes():
#    if p in neigh:
#        node_size.append(1)
#    else:
#        node_size.append(0)
#QGraph.plot(graph, s=node_size, savename = "plot_coordination_zone", dpi = 400)