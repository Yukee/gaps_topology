# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 11:24:06 2016

@author: nicolas

Plot of the numerical groundstate
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
    
def o(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + 2*b
    return a

def L(n):
    """ length of the unit cell of the nth approximant """
    return np.sqrt((o(n)+o(n-1))**2 + 2*o(n)**2)    
    
def listplot(pos, field, ptsize=1,title = None, savename=None):
    x, y = np.array(pos).T
    color = field
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=ptsize, cmap = 'viridis')
    cbar = plt.colorbar(sc)
    #cbar.set_label('Electron density', rotation=0)
    cbar.ax.set_title('Electron density', rotation=0)
    plt.axes().set_aspect('equal')
    if title:
        plt.title(title)
    # length of the system (in physical space)
#    l = L(n)
#    plt.xlim([0., l])
#    plt.ylim([0., l])
#    plt.xlabel("length, in Ångströms (lattice spacing set to 1 Å)")
    dark = plt.cm.viridis(0)
    plt.gca().set_axis_bgcolor(dark)
    if(savename):
        plt.savefig(savename+'.png', dpi=400)
    plt.show()
    
# number of inflations
n = 7
# wavevectors
kx, ky = 0., 0.
# in which substace to plot
proj = 'perp'
# import a tiling
try:
    en, g = np.load('data/groundstate/' +  'gen_' + str(n) + '_kx_' + str(kx) + '_ky_' + str(ky)  + '.npy')
except FileNotFoundError:
    print("Groundstate data does not exist. Run exact_diagonalization_groundstate.py to generate it.")
    raise
# get the groundstate amplitude data
toAmp = nx.get_node_attributes(g, "groundstate")
amp = [abs(toAmp[pt]) for pt in g]
# get the node positions
toPos = nx.get_node_attributes(g, proj)
pos = [toPos[pt] for pt in g]
# plot everything
listplot(pos, amp, .3, 'energy ' + str(en[0]), 'groundstate_' + proj + '_gen_'+str(n))