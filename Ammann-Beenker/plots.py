# -*- coding: utf-8 -*-
"""
Created on Mon Feb 29 17:38:01 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the other stuff """
import math, cmath
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx # graph tools
import QuantumGraph as QGraph
import scipy

# construct square approximants to A5
sq2 = math.sqrt(2)
# start with a square
square0 = []
# half-length of the square
r = 1.
# first half-square
C = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, -3*math.pi/4)
B = cmath.rect(r, -5*math.pi/4)
square0.append((r, (A, B, C)))
# second half-square
C = cmath.rect(r, -5*math.pi/4)
B = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, math.pi/4)
square0.append((r, (A, C, B)))
square = tl.A5(square0)

# number of inflations 
ninf = 4
square.it_sub(ninf)

def spec_PHD(t, delta):
    pg = QGraph.periodize(square.PHD_graph(t, delta))
    ham = nx.to_numpy_matrix(pg)
    return scipy.linalg.eigh(ham, eigvals_only=True)

def spec_Sire(t):
    pg = QGraph.periodize(square._graph)
    ham = QGraph.OS_ham(pg, -t, 1.-t)
    return scipy.linalg.eigh(ham, eigvals_only=True)
    
def plot(specs, ts, aspect_ratio = 'equal', alpha=0.5, s=1, savename=None):
    xs = np.array(specs)
    L = len(xs[0])
    ys = np.array([t*np.ones(L) for t in ts])
    x = xs.flatten()
    y = ys.flatten()
    plt.axes().set_aspect(aspect_ratio, 'datalim')
    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)
    plt.axis([xmin, xmax, ymin, ymax])
    plt.scatter(x, y, s=s, c="gray", alpha=alpha, edgecolor='') 
    if savename:
        plt.savefig(savename+'.pdf')

def plot_en(num, val, idos):
    plt.plot(val, idos, 'o', alpha=0.2, markersize=5)
    plt.plot(val[num], idos[num], 'ro')

def plot_ens(specs, ts, aspect_ratio, ens, ms = 1, s = 10, alpha=0.1, savename=None):
    xs = np.array(specs)
    L = len(xs[0])
    ys = np.array([t*np.ones(L) for t in ts])
    x = xs.flatten()
    y = ys.flatten()
    plt.axes().set_aspect(aspect_ratio, 'datalim')
    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)
    plt.axis([xmin, xmax, ymin, ymax])
    plt.scatter(x, y, s=s, c="gray", alpha=alpha, edgecolor='')
    for en in ens:
        plt.plot(en, ts, 'ro', markersize=ms, alpha=0.2)
    if savename:
        plt.savefig(savename+'.pdf')

        
#ts = np.linspace(0, 1, 100)
#specs = np.array([spec_PHD(t, 1-t) for t in ts])
#
#o = square.o # octonacci numbers
#m = 2*o(2*ninf-2)
#M = o(2*ninf-2)+o(2*ninf-1)
#ens_left = np.transpose(specs)[m:M]
#
#L = o(2*ninf+1)+o(2*ninf)
#m = L-2*o(2*ninf-2)
#M = L-(o(2*ninf-2)+o(2*ninf-1))
#ens_right = np.transpose(specs)[M:m]
#
#plot_ens(specs, ts, 10, list(ens_left)+list(ens_right), ms = 1, s = 10, alpha = 0.01, savename = "idos_PH_gen_"+str(ninf))
#plt.close()
#
#ts = np.linspace(0, 1, 100)
#specs = np.array([spec_Sire(t) for t in ts])
## select all points that in the first energy cluster in the on-site potentials model
#ens = np.transpose(specs)[:o(2*ninf+1)-o(2*ninf)]
#
#plot_ens(specs, ts, 10, ens, ms = 1, s = 10, alpha = 0.01, savename = "idos_Sire_gen_"+str(ninf))

#val1 = spec_PHD(1., 0.)
#val0 = spec_PHD(0., 1.)
#idos = np.linspace(0, 1, len(val1))

#plt.plot(val1, idos, 'o', alpha=0.2, markersize=5)
#plt.savefig("idos_PH_gen_"+str(ninf)+".pdf")
#plt.close()
#
#plt.plot(val0, idos, 'o', alpha=0.2, markersize=5)
#plt.savefig("idos_PHD_t=0_gen_"+str(ninf)+".pdf")        
# plot(specs, ts, 10, 0.01, 12, "PHD_gen_"+str(ninf))
        
graph = square.PHD_graph(0.2, 1)
weights = QGraph.weights(graph)
QGraph.plot(graph, weights=weights, savename="dkjf")