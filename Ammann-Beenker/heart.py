#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 17:54:22 2017

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import CP as cp
import QuantumGraph as QGraph

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from scipy.sparse.linalg import eigsh

def heart_region(p):
    x, y = p/15.
    return (x**2 + y**2 - 1)**3 - x**2*y**3 <= 0

r = cp.AB()
r.in_region = heart_region
g = r.compute_graph(np.array([0,0,0,0]))

proj = r._para

def proj_graph(graph):
    """ 
    project the graph onto the perp space
    """
    pg = g.copy()
    for p in graph.nodes():
        pp = np.dot(proj, p)
        pg.node[p]['proj'] = pp
        
    return pg
    
def plot(pgraph, data = False, lims = None):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'proj')
    plt.axes().set_aspect('equal')
    
    if lims:
        plt.xlim(lims[0])
        plt.ylim(lims[1])
#    if not data:
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 5, node_color = list(data), cmap = "viridis")      
#        nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 0)      
    plt.savefig("cp.png", dpi=300)

t = 1.
ham = QGraph.sparse_OS_ham(g, t, 1.-t)
en, wf = eigsh(ham, k=20)

pg = proj_graph(g)
plot(pg, data = wf[:,7], lims = ((-20,20), (-16,20)))  
