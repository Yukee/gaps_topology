# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 10:45:31 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the other stuff """
import math, cmath
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx # graph tools
import QuantumGraph as QGraph

sq2 = math.sqrt(2)
lb = sq2 - 1.
pi = math.pi
e = np.exp(1j*pi*np.arange(4)/4)

def pt_to_env(graph):
    """
    Label the sites according to their first neighbors environment
    """
    # determine the local environments on the tiling
    zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}

    # D1 and D2 are distinguished by their local environment
    ptToEnv = {}
    for pt in graph:
        n = graph.neighbors(pt)
        z = len(n)
        # if pt is a D type site, distinguish between D1 and D2
        if z == 5:
            # D2 sites have in their neighbourhood 2 F sites
            if 3 in [len(graph.neighbors(pn)) for pn in n]:
                ptToEnv[pt] = 'D2'
            else:
                ptToEnv[pt] = 'D1'
        elif 3 <= z <= 8:
            ptToEnv[pt] = zToEnv[z]
        else: # this can happen at the edges (eg if free bc are used we have sites of coordinance 2)
            ptToEnv[pt] = None
    return ptToEnv

def loz(orig, i, j):
    """ 
    create a lozenge with edges along ei and ej
    """
    if abs(i-j)==1:
        A = orig
        B = orig + e[i]
        C = orig + e[i] + e[j]
        D = orig + e[j]
    elif abs(i-j)==3:
        A = orig + e[i]
        B = orig + e[i] + e[j]
        C = orig + e[j]
        D = orig
    else:
        raise RuntimeError("The specified edges cannot be used to create a lozenge")

    return [(0, (A, B, C, D))]
    
def square(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not math.isclose(np.vdot(ea, eb).real, 0, abs_tol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
        
def tlA():
    """
    return an A5 Tiling initialized with an 8-fold flower
    return the point at the center of the flower
    """
    wheel0 = []
    A = 0j
    for i in range(8):
        B = cmath.rect(1, (i) * math.pi / 4)
        D = cmath.rect(1, (i + 1.) * math.pi / 4)
        C = B + D
        wheel0.append((0, (A, B, C, D)))
            
    return wheel0
    
def tlB():
    shapes = []
    shapes += loz(0, 1, 2)
    shapes += loz(0, 2, 3)
    shapes += loz(-e[0], 0, 3)
    shapes += loz(-e[1]-e[0], 0, 1)
    shapes += loz(-e[2]-e[1], 1, 2)
    shapes += loz(-e[3]-e[2], 2, 3)
    shapes += square(0, e[1], -e[3])
    
    return shapes
    
def tlC():
    shapes = []
    shapes += square(0, e[1], -e[3])
    shapes += square(0, -e[0], -e[2])
    shapes += loz(0, 1, 2)
    shapes += loz(0, 2, 3)
    shapes += loz(-e[0], 0, 3)
    shapes += loz(-e[3]-e[2], 2, 3)
    
    return shapes
    
def tlD1():
    shapes = []
    shapes += loz(0, 0, 1)
    shapes += loz(-e[0], 0, 3)
    shapes += square(0, e[1], e[3])
    
    shapes += square(0, e[0], -e[2])
    shapes += square(0, -e[0], -e[2])
    
    return shapes

def tlD2():
    shapes = []
    shapes += loz(0, 0, 1)
    shapes += loz(-e[0], 0, 3)
    shapes += square(0, e[1], e[3])
    
    shapes += square(-e[2], e[0], e[2])
    shapes += square(-e[2], -e[0], e[2])
    
    return shapes

def tlE():
    shapes = []
    shapes += loz(0, 1, 2)
    shapes += loz(-e[0]-e[3], 0, 3)
    shapes += square(-e[3], e[1], e[3])
    shapes += square(-e[0], e[0], e[2])
    
    return shapes
    
def tlF():
    shapes = []
    shapes += square(e[1]-e[3], e[3], -e[1])
    shapes += loz(-e[0], 0, 1)
    shapes += loz(-e[3]-e[0], 0, 3)
    
    return shapes

def pt_to_pot(weightedGraph):
    """
    return a dictionary p:m(p) where p is a point and m(p) is the value of the potential at this point
    """
    #tiling._graph = QGraph.periodize(tiling._graph, False)
    return {p[0]:p[1]['weight'] for p in weightedGraph.nodes(data='weight')}
    
def stat_pot(nint, bins):
    nTp = pt_to_pot(nint)
    pots = list(nTp.values())
    return np.histogram(pots, bins=bins)
    
def count(weightedGraph):
    ptToEnv = pt_to_env(weightedGraph)
    ptToPot = pt_to_pot(weightedGraph)
    
    count = {env:{} for env in ('A', 'B', 'C', 'D1', 'D2', 'E', 'F')}
    for p in weightedGraph:
        env = ptToEnv[p]
        if env:
            pot = ptToPot[p]
            if pot in count[env].keys():
                count[env][ptToPot[p]] += 1
            else:
                count[env][ptToPot[p]] = 1
    return count
  
shapes = square(0, e[0], e[2])
#shapes = tlD1()
tiling = tl.A5(shapes)

def draw(name):
    tiling.it_sub(1)
    wg = tiling.integrate_arrow_field(p0)
    labels = {p[0]:str(p[1]['weight']) for p in wg.nodes(data='weight')}
    QGraph.plot(wg, labels=labels, savename = name)
    return None
        
p0 = 0j
l = 1
tiling.it_sub(l)

wg0 = tiling.integrate_arrow_field(p0)
wg0 = QGraph.periodize(wg0)
tiling.it_sub(l+1)
wg1 = tiling.integrate_arrow_field(p0)
wg1 = QGraph.periodize(wg1)

count0 = count(wg0)
count1 = count(wg1)

def get(dic, key):
    value = 0
    if key in dic.keys():
        value = dic[key]
    return value

#comp = [(count1['E'][m], 2*get(count0['D2'],-m-1)+3*get(count0['E'],-m-1)+2*get(count0['F'],-m-1)) for m in count1['E'].keys()]
#comp = [(get(count1['A'],m), get(count0['A'], -m) + get(count0['B'], -m) + get(count0['C'],-m) + get(count0['D1'],-m)) for m in count1['A'].keys()]
comp = [(count1['F'][m], 8*get(count1['A'],m+1)+5*get(count0['D2'],-m-1)+2*get(count0['E'],-m-1)) for m in count1['F'].keys()]
rel_err = [abs(v1-v2)/(v1+v2) for v1, v2 in comp]
#test = [(4*count0['A'][m]+4.25*count0['B'][m]+4.5*count0['C'][m]+4.75*count0['D1'][m]+3*count0['D2'][m]+1.5*count0['E'][m], 2*count1['F'][-m-1]) for m in range(-1,2)]

#tiling.it_sub(3)
graph = tiling._graph
ptToEnv = pt_to_env(graph)

s = [ptToEnv[p] == 'F' for p in graph]
        
QGraph.plot(graph, s=s)

tiling.it_sub(l)
graph = tiling._graph
QGraph.plot(graph, weights = 1., savename="inflation_sites_F", dpi = 200)