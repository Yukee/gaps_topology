# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 15:31:07 2016

@author: nicolas

In this script, we compute the classical conductivity (ie the high input current limit of the quantum conductivity)
for A5 approximants of increasing size. 
The goal is to fit the size dependance of the conductivity by a power-law: g(L) ~ L^d_e
where d_e is the electric dimension. 
For a periodic lattice, it is expected d_e = d-2 where d is the topological dimension (here 2).

Rk: we define the conductivity as g = i/V where i is the input current flowing bewteen 2 leads connected to the sample,
and V is the voltage between these two same leads.
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as til
""" import the other stuff """
import math, cmath
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx # graph tools
import QuantumGraph as QGraph

import scipy.sparse as sparse
import scipy as sp

# start with a square 
sq2 = math.sqrt(2)
# start with a square
square0 = []
# half-length of the square
r = 1.
# first half-square
C = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, -3*math.pi/4)
B = cmath.rect(r, -5*math.pi/4)
square0.append((r, (A, B, C)))
# second half-square
C = cmath.rect(r, -5*math.pi/4)
B = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, math.pi/4)
square0.append((r, (A, C, B)))
square = til.A5(square0)

""" one in/out lead """

## number of inflations 
#ninf = 1
#square.it_sub(ninf)
#tl = square._graph # tiling
##L = 2
##tl = nx.grid_2d_graph(L, L)
#
## leads in and out
#in_lead = A
#out_lead = B
##in_lead = (0,0)
##out_lead = (L-1,L-1)
#
## lead-lead conductivity matrix
#GLL = np.zeros((2,2))
#GLL[0,0] = len(tl.neighbors(in_lead))
#if out_lead in tl.neighbors(in_lead):
#    GLL[0,1] = -1.
#    GLL[1,0] = -1.
#GLL[1,1] = len(tl.neighbors(out_lead))
#
## bulk-bulk conductivity matrix
#bulk = tl.copy() # bulk graph
#bulk.remove_nodes_from([in_lead, out_lead]) # remove the leads from the bulk graph
#
#L = len(bulk) # number of bulk nodes
#keyToId = {key:identifier for key, identifier in zip(bulk, range(L))} # key-to-identifier dict
##GBB = np.zeros((L, L))
#GBB = sparse.lil_matrix((L,L))
#i = 0 # iterator over nodes
#for node in bulk.nodes():
#    neigh = tl.neighbors(node)
#    GBB[i, i] = len(neigh)
#    for n in neigh:
#        if n != in_lead and n != out_lead:
#            GBB[keyToId[n], i] = -1
#    i += 1
#GBB = GBB.tocsc() # convert to CSC format, allowing for efficient linalg inverse
#
## lead-bulk and bulk-lead conductivity matrices
#n_in = tl.neighbors(in_lead) # neighbors of the in lead
#n_out = tl.neighbors(out_lead) # neighbors of the out lead
#GBL = sparse.lil_matrix((len(bulk), 2))
#for node in n_in:
#    GBL[keyToId[node], 0] = -1
#for node in n_out:
#    GBL[keyToId[node], 1] = -1
#GBL = GBL.tocsr()
#GLB = GBL.transpose()
#
#""" several in/out leads """
#
#M = 3
#N = M-1
#tl = nx.grid_2d_graph(M, N) # our electrical network is a square grid
#in_leads = [(0,j) for j in range(N)] # in leads plugged to the left hand side of the grid
#out_leads = [(M-1,j) for j in range(N)] # out leads plugged to the right hand side of the grid
#
#inout_leads = in_leads + out_leads
#Nin = len(in_leads)
#Nout = len(out_leads)
#Nleads = Nin + Nout
#
## bulk-bulk conductivity matrix
#bulk = tl.copy() # bulk graph
#bulk.remove_nodes_from(inout_leads) # remove the leads from the bulk graph
#
#Nbulk = len(bulk) # number of bulk nodes
#keyToId = {key:identifier for key, identifier in zip(bulk, range(Nbulk))} # key-to-identifier dict
#GBB = sparse.lil_matrix((Nbulk, Nbulk))
#i = 0 # iterator over nodes
#for node in bulk.nodes():
#    neigh = tl.neighbors(node)
#    GBB[i, i] = len(neigh)
#    for n in neigh:
#        if n not in inout_leads:
#            GBB[keyToId[n], i] = -1
#    i += 1
#GBB = GBB.tocsc() # convert to CSC format, allowing for efficient linalg inverse
#
## lead-bulk and bulk-lead conductivity matrices
#GBL = sparse.lil_matrix((Nbulk, Nleads))
#for j in range(Nin):
#    n_in = tl.neighbors(in_leads[j]) # neighbors of the in lead
#    for node in n_in:        
#        if node not in in_leads:
#            GBL[keyToId[node], j] = -1
#for j in range(Nout):
#    n_out = tl.neighbors(out_leads[j]) # neighbors of the out lead
#    for node in n_out:
#        if node not in out_leads:
#            GBL[keyToId[node], Nin+j] = -1
#GBL = GBL.tocsr()
#GLB = GBL.transpose()
#
## lead-lead conductivity matrix
#GLL = sparse.lil_matrix((Nleads, Nleads))
#for i in range(Nin):
#    neigh = set(tl.neighbors(in_leads[i])) - set(in_leads)
#    GLL[i,i] = len(neigh)
#for i in range(Nout):
#    neigh = set(tl.neighbors(out_leads[i])) - set(out_leads)
#    GLL[Nin+i,Nin+i] = len(neigh)
#GLL = GLL.tocsr()
#
## reduced conductivity matrix
#Gr_inin = (GLL - GLB * sparse.linalg.inv(GBB) * GBL)[:Nin,:Nin]
## conductivity
#g = Gr_inin.sum()

""" several in/out leads and several resistances """
#def fibo(n):
#    a, b = 0, 1
#    for i in range(n):
#        a, b = b, a + b
#    return b
#
#M = 3
#N = M-1
#
#fib = til.FibonacciProduct([(0,(0,1))])
#fib.it_sub(M,N)
#tl = fib._graph
#in_leads = [(0,j) for j in range(fibo(N)+1)] # in leads plugged to the left hand side of the grid
#out_leads = [(fibo(M),j) for j in range(fibo(N)+1)] # out leads plugged to the right hand side of the grid
#
#tl = nx.grid_2d_graph(M, N) # our electrical network is a square grid
#nx.set_edge_attributes(tl, 'weight', 'S')
#in_leads = [(0,j) for j in range(N)] # in leads plugged to the left hand side of the grid
#out_leads = [(M-1,j) for j in range(N)] # out leads plugged to the right hand side of the grid
#
#inout_leads = in_leads + out_leads
#Nin = len(in_leads)
#Nout = len(out_leads)
#Nleads = Nin + Nout
#
#rho = 3.
#res = {'L':1, 'S':rho} # weight to res dictionary
#
## bulk-bulk conductivity matrix
#bulk = tl.copy() # bulk graph
#bulk.remove_nodes_from(inout_leads) # remove the leads from the bulk graph
#
#Nbulk = len(bulk) # number of bulk nodes
#keyToId = {key:identifier for key, identifier in zip(bulk, range(Nbulk))} # key-to-identifier dict
#GBB = sparse.lil_matrix((Nbulk, Nbulk))
#i = 0 # iterator over nodes
#for node in bulk.nodes():
#    neigh = tl[node]
#    r_neigh = sum(res[v['weight']] for v in neigh.values()) # total resistance towards the neighboring nodes
#    GBB[i, i] = r_neigh
#    for n in neigh:
#        if n not in inout_leads:
#            GBB[keyToId[n], i] = -res[neigh[n]['weight']]
#    i += 1
#GBB = GBB.tocsc() # convert to CSC format, allowing for efficient linalg inverse
#
## lead-bulk and bulk-lead conductivity matrices
#GBL = sparse.lil_matrix((Nbulk, Nleads))
#for j in range(Nin):
#    in_lead = in_leads[j]
#    n_in = tl[in_lead] # neighbors of the in lead
#    for node in n_in:        
#        if node not in in_leads:
#            GBL[keyToId[node], j] = -res[n_in[node]['weight']]
#for j in range(Nout):
#    out_lead = out_leads[j]
#    n_out = tl[out_lead] # neighbors of the out lead
#    for node in n_out:
#        if node not in out_leads:
#            GBL[keyToId[node], Nin+j] = -res[n_out[node]['weight']]
#GBL = GBL.tocsr()
#GLB = GBL.transpose()
#
## lead-lead conductivity matrix
#GLL = sparse.lil_matrix((Nleads, Nleads))
#for i in range(Nin):
#    in_lead = in_leads[i]
#    neigh = [n for n in tl.neighbors(in_lead) if n not in in_leads]
#    r_neigh = sum(res[tl[in_lead][n]['weight']] for n in neigh)
#    GLL[i,i] = r_neigh
#for i in range(Nout):
#    out_lead = out_leads[i]
#    neigh = [n for n in tl.neighbors(out_lead) if n not in out_leads]
#    r_neigh = sum(res[tl[out_lead][n]['weight']] for n in neigh)
#    GLL[Nin+i,Nin+i] = r_neigh
#GLL = GLL.tocsr()
#
### lead-lead conductivity matrix
##GLL = sparse.lil_matrix((Nleads, Nleads))
##leadToId = {key:identifier for key, identifier in zip(inout_leads, range(Nleads))}
##for i in range(Nin):
##    in_lead = in_leads[i]
##    #neigh = [n for n in tl.neighbors(in_lead) if n not in in_leads]
##    neigh = tl[in_lead]    
##    GLL[i,i] = sum(res[tl[in_lead][n]['weight']] for n in neigh)
##    for node in neigh:        
##        if node in in_leads:
##            GLL[leadToId[node], i] = -res[neigh[node]['weight']]
##for i in range(Nout):
##    out_lead = out_leads[i]
##    #neigh = [n for n in tl.neighbors(out_lead)]# if n not in out_leads]
##    neigh = tl[out_lead]    
##    GLL[Nin+i,Nin+i] = sum(res[tl[out_lead][n]['weight']] for n in neigh)
##    for node in neigh:        
##        if node in out_leads:
##            GLL[leadToId[node], i] = -res[neigh[node]['weight']]
#
#GLL = GLL.tocsr()
#
## reduced conductivity matrix
#Gr_inin = (GLL - GLB * sparse.linalg.inv(GBB) * GBL)[:Nin,:Nin]
## conductivity
#g = Gr_inin.sum()

""" method """
# res: dictionary of conductivity
def conductivity(tl, in_leads, out_leads, res):
    inout_leads = in_leads + out_leads
    Nin = len(in_leads)
    Nout = len(out_leads)
    Nleads = Nin + Nout
        
    # bulk-bulk conductivity matrix
    bulk = tl.copy() # bulk graph
    bulk.remove_nodes_from(inout_leads) # remove the leads from the bulk graph
    
    Nbulk = len(bulk) # number of bulk nodes
    keyToId = {key:identifier for key, identifier in zip(bulk, range(Nbulk))} # key-to-identifier dict
    GBB = sparse.lil_matrix((Nbulk, Nbulk))
    i = 0 # iterator over nodes
    for node in bulk.nodes():
        neigh = tl[node]
        r_neigh = sum(res[v['weight']] for v in neigh.values()) # total resistance towards the neighboring nodes
        GBB[i, i] = r_neigh
        for n in neigh:
            if n not in inout_leads:
                GBB[keyToId[n], i] = -res[neigh[n]['weight']]
        i += 1
    GBB = GBB.tocsc() # convert to CSC format, allowing for efficient linalg inverse
    
    # lead-bulk and bulk-lead conductivity matrices
    GBL = sparse.lil_matrix((Nbulk, Nleads))
    for j in range(Nin):
        in_lead = in_leads[j]
        n_in = tl[in_lead] # neighbors of the in lead
        for node in n_in:        
            if node not in in_leads:
                GBL[keyToId[node], j] = -res[n_in[node]['weight']]
    for j in range(Nout):
        out_lead = out_leads[j]
        n_out = tl[out_lead] # neighbors of the out lead
        for node in n_out:
            if node not in out_leads:
                GBL[keyToId[node], Nin+j] = -res[n_out[node]['weight']]
    GBL = GBL.tocsr()
    GLB = GBL.transpose()
    
    # lead-lead conductivity matrix
    GLL = sparse.lil_matrix((Nleads, Nleads))
    for i in range(Nin):
        in_lead = in_leads[i]
        neigh = [n for n in tl.neighbors(in_lead) if n not in in_leads]
        r_neigh = sum(res[tl[in_lead][n]['weight']] for n in neigh)
        GLL[i,i] = r_neigh
    for i in range(Nout):
        out_lead = out_leads[i]
        neigh = [n for n in tl.neighbors(out_lead) if n not in out_leads]
        r_neigh = sum(res[tl[out_lead][n]['weight']] for n in neigh)
        GLL[Nin+i,Nin+i] = r_neigh
    GLL = GLL.tocsr()
            
    # reduced conductivity matrix
    print("so far, so good")
    print("trying to inverse a matrix of size "+str(len(GBB.nonzero()[0])))
    iGBB = sp.linalg.inv(GBB.todense())
    print("haha!")
    Gr_inin = (GLL - GLB * iGBB * GBL)[:Nin,:Nin]
    print("effective matrix computed")
    # conductivity
    return Gr_inin.sum()
    
""" using lin solve instead of matrix inversion (much faster and accurate!) """
def cond_lin(tl, in_leads, out_leads, res):
    inout_leads = in_leads + out_leads
    Nin = len(in_leads)
        
    # bulk-bulk conductivity matrix
    bulk = tl.copy() # bulk graph
    bulk.remove_nodes_from(inout_leads) # remove the leads from the bulk graph
    
    Nbulk = len(bulk) # number of bulk nodes
    keyToId = {key:identifier for key, identifier in zip(bulk, range(Nbulk))} # key-to-identifier dict
    GBB = sparse.lil_matrix((Nbulk, Nbulk))
    i = 0 # iterator over nodes
    for node in bulk.nodes():
        neigh = tl[node]
        r_neigh = sum(res[v['weight']] for v in neigh.values()) # total resistance towards the neighboring nodes
        GBB[i, i] = r_neigh
        for n in neigh:
            if n not in inout_leads:
                GBB[keyToId[n], i] = -res[neigh[n]['weight']]
        i += 1
    GBB = GBB.tocsc() # convert to CSC format, allowing for efficient linalg inverse
    
    # lead-bulk and bulk-lead conductivity matrices
    GBL = sparse.lil_matrix((Nbulk, Nin))
    for j in range(Nin):
        in_lead = in_leads[j]
        n_in = tl[in_lead] # neighbors of the in lead
        for node in n_in:        
            if node not in in_leads:
                GBL[keyToId[node], j] = -res[n_in[node]['weight']]
    GBL = GBL.tocsr()
    GLB = GBL.transpose()
    
    # lead-lead conductivity matrix
    GLL = sparse.lil_matrix((Nin, Nin))
    for i in range(Nin):
        in_lead = in_leads[i]
        neigh = [n for n in tl.neighbors(in_lead) if n not in in_leads]
        r_neigh = sum(res[tl[in_lead][n]['weight']] for n in neigh)
        GLL[i,i] = r_neigh
    GLL = GLL.tocsr()

    # solve for bulk potentials
    Vin = GBL.sum(axis=1) # vector of the in potentials applied to GBL
    print("starting solving for the bulk potentials...")
    VB = sparse.linalg.spsolve(GBB, -Vin) # bulk potentials
    Ginin = GLL.sum()
    print("in leads sum = ", Ginin)
    return Ginin + (GLB * VB).sum()
    
def cond_unif(tl, in_leads, out_leads):
    inout_leads = in_leads + out_leads
    Nin = len(in_leads)
        
    # bulk-bulk conductivity matrix
    bulk = tl.copy() # bulk graph
    bulk.remove_nodes_from(inout_leads) # remove the leads from the bulk graph
    
    Nbulk = len(bulk) # number of bulk nodes
    keyToId = {key:identifier for key, identifier in zip(bulk, range(Nbulk))} # key-to-identifier dict
    GBB = sparse.lil_matrix((Nbulk, Nbulk))
    i = 0 # iterator over nodes
    for node in bulk.nodes():
        neigh = tl[node]
        r_neigh = len(neigh) # total resistance towards the neighboring nodes
        GBB[i, i] = r_neigh
        for n in neigh:
            if n not in inout_leads:
                GBB[keyToId[n], i] = -1.
        i += 1
    GBB = GBB.tocsc() # convert to CSC format, allowing for efficient linalg inverse
    
    # lead-bulk and bulk-lead conductivity matrices
    GBL = sparse.lil_matrix((Nbulk, Nin))
    for j in range(Nin):
        in_lead = in_leads[j]
        n_in = tl[in_lead] # neighbors of the in lead
        for node in n_in:        
            if node not in in_leads:
                GBL[keyToId[node], j] = -1.
    GBL = GBL.tocsr()
    GLB = GBL.transpose()
    
    # lead-lead conductivity matrix
    GLL = sparse.lil_matrix((Nin, Nin))
    for i in range(Nin):
        in_lead = in_leads[i]
        neigh = [n for n in tl.neighbors(in_lead) if n not in in_leads]
        r_neigh = len(neigh)
        GLL[i,i] = r_neigh
    GLL = GLL.tocsr()

    # solve for bulk potentials
    Vin = GBL.sum(axis=1) # vector of the in potentials applied to GBL
    print("starting solving for the bulk potentials...")
    VB = sparse.linalg.spsolve(GBB, -Vin) # bulk potentials
    Ginin = GLL.sum()
    print("in leads sum = ", Ginin)
    return Ginin + (GLB * VB).sum()
            
""" different graphs """

def fibo(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return b

def square_grid(M, N):
    tl = nx.grid_2d_graph(M, N) # our electrical network is a square grid
    in_leads = [(0,j) for j in range(N)] # in leads plugged to the left hand side of the grid
    out_leads = [(M-1,j) for j in range(N)] # out leads plugged to the right hand side of the grid
    return tl, in_leads, out_leads
    
def ammann_beenker(Ninf):
    square.it_sub(Ninf)
    tl = square._graph # our electrical network is an Ammann-Beenker Ninf times inflated square
    
    pos = [(pt.real, pt.imag) for pt in tl.nodes()]
    # transpose
    x, y = zip(*pos)
    xmin = min(x)
    xmax = max(x)
    ymin = min(y)
    ymax = max(y)
    
    # tolerance is one percent of the average distance between two nodes
    av_dist = min(xmax-xmin, ymax-ymin)/math.sqrt(len(x))    
    tol = 0.01*av_dist
    
    # list nodes that are on the edges of the square
    in_leads = [p for p in tl.nodes() if math.isclose(p.real, xmin, abs_tol=tol)] # in leads plugged to the left hand side of the square
    out_leads = [p for p in tl.nodes() if math.isclose(p.real, xmax, abs_tol=tol)] # out leads plugged to the right hand side of the grid

    return tl, in_leads, out_leads

def fib(M, N, rho):
    fib = til.FibonacciProduct([(0,(0,1))])
    fib.it_sub(M,N)
    tl = fib._graph
    in_leads = [(0,j) for j in range(fibo(N)+1)] # in leads plugged to the left hand side of the grid
    out_leads = [(fibo(M),j) for j in range(fibo(N)+1)] # out leads plugged to the right hand side of the grid

    cond = {'L':1, 'S':rho}
    
    return tl, in_leads, out_leads, cond
    
## conductivity Fibonacci varying rho
#nmin = 3
#nmax = 13
#rhos = np.arange(1.,0.,-0.1)
#beta = []
#s_log = []
#for rho in rhos:
#    fibs = [fib(i, i, rho) for i in range(nmin, nmax+1)]
#    sg = [(math.sqrt(len(t[0])),cond_lin(*t)) for t in fibs]
#    s, g = zip(*sg)
#    beta.append([math.log(g[i+1]/g[i], s[i+1]/s[i]) for i in range(nmax-nmin)])
#    s_log.append([math.log(s[i+1], s[i+1]/s[i]) for i in range(nmax-nmin)])
#for s, b in zip(s_log, beta):    
#    plt.plot(s, b, 'o')
#plt.xlabel(r"$\log_\phi(L)$ (log lin size)")
#plt.ylabel(r"$\beta(L)$ (approximate beta)")
#plt.title("Conductivity scaling for cartesian products of Fibonacci chains, varying rho")
#plt.savefig("classical_conductivity_fibonacci.pdf")
#plt.show()

# conductivity Ammann-Beenker
nmin = 3
nmax = 8
a5 = [ammann_beenker(i) for i in range(nmin,nmax+1)]
sg = [(math.sqrt(len(tl)),cond_unif(tl, in_leads, out_leads)) for tl, in_leads, out_leads in a5]
s, g = zip(*sg)
beta = [math.log(g[i+1]/g[i], s[i+1]/s[i]) for i in range(nmax-nmin)]
s_log = [math.log(s[i+1], s[i+1]/s[i]) for i in range(nmax-nmin)]

plt.plot(s_log, beta, 'o')
plt.xlabel(r"$\log_\lambda(L)$ (log lin size)")
plt.ylabel(r"$\beta(L)$ (approximate beta)")
plt.title("Conductivity scaling for the Ammann-Beenker A5 tiling")
plt.savefig("classical_conductivity_a5.pdf")
plt.show()

#r = 2**np.arange(2,6)
#nr = len(r)
#grids = [square_grid(N,N) for N in r]
#sg = [(len(tl),unif_cond(tl, in_leads, out_leads)) for tl, in_leads, out_leads in grids]
#s, g = zip(*sg)
#beta = [math.log(g[i+1]/g[i], s[i+1]/s[i]) for i in range(nr-1)]
#s_log = [math.log(s[i+1], s[i+1]/s[i]) for i in range(nr-1)]

