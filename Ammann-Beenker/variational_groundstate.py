# -*- coding: utf-8 -*-
"""
Created on Sun Feb 14 10:48:20 2016

@author: nicolas
"""

import numpy as np
import math
import cmath
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import networkx as nx
""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph

## number of inflations 
#ninf = 6

# pickle numerical groundstate data
#wf = np.load('data/gs_wf_gen_'+str(ninf)+'.npy')

""" construct the tiling (useful for plots) """
# start with a square 
sq2 = math.sqrt(2)
# start with a square
square0 = []
# half-length of the square
r = 1.
# first half-square
C = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, -3*math.pi/4)
B = cmath.rect(r, -5*math.pi/4)
square0.append((r, (A, B, C)))
# second half-square
C = cmath.rect(r, -5*math.pi/4)
B = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, math.pi/4)
square0.append((r, (A, C, B)))
square = tl.A5(square0)

# scatter plot where pos is a list of tuples (x, y) and field is A(x,y)
def listplot(pos, field, ptsize=1, savename=None):
    x, y = np.array(pos).T
    color = field
    sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=ptsize, cmap = 'viridis')
    plt.colorbar(sc)
    plt.axes().set_aspect('equal')
    dark = plt.cm.viridis(0)
    plt.gca().set_axis_bgcolor(dark)
    if(savename):
        plt.savefig(savename+'.png', dpi=300)
    plt.show()
    #fig.canvas.draw()
    
def list3Dplot(pos, field, ptsize=1, savename=None):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x, y = np.array(pos).T
    z = field
    ax.scatter(x, y, z, marker='o', s=ptsize)
    
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')
    if(savename):
        plt.savefig(savename+'.pdf')
    plt.show()

# todo : transform list of coordinates into a list of lists (meshgrid)
#def plot(pos, field):
#    fig = plt.figure()
#    x, y = np.array(pos).T
#    ax = plt.axes(projection='3d')
#    
#    ax.plot_surface(x, y, field, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)
#    plt.show()
    
""" variational Ansatz """
# the wf coefficients as a function of the local environment
coef0 = {'A':0.480471, 'B':0.442286, 'C':0.404101, 'D1':0.366701, 'D2':0.349018, 'E':0.299556, 'F':0.25341}
beta = 0.907397
coef = {'A':0.469686, 'B':0.443274, 'C':0.416863, 'D1':0.391146, 'D2':0.351229, 'E':0.286719, 'F':0.224864} # coeff for beta = 0.91

# determine the local environments on the tiling
zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}
# return a dictionary pts to environment, given a graph
def pt_to_env(graph):
    # D1 and D2 are distinguished by their local environment
    ptToEnv = {}
    for pt in graph:
        n = graph.neighbors(pt)
        z = len(n)
        # if pt is a D type site, distinguish between D1 and D2
        if z == 5:
            # D2 sites have in their neighbourhood 2 F sites
            if 3 in [len(graph.neighbors(pn)) for pn in n]:
                ptToEnv[pt] = 'D2'
            else:
                ptToEnv[pt] = 'D1'
        else:
            ptToEnv[pt] = zToEnv[z]
    return ptToEnv
    
""" overlap """    
overlap = []
overlap0 = []
overlapUnif = []
rinfs = range(2,7)
for ninf in rinfs:
    wf_num = np.load('data/gs_wf_gen_'+str(ninf)+'.npy').flatten()

    square.it_sub(ninf) 
    pg = QGraph.periodize(square._graph)
    wg = square.integrate_arrow_field(B)
    
    ptToPot = nx.get_node_attributes(wg,'weight')
    ptToEnv = pt_to_env(pg)
    
    wf_var = np.array([coef[ptToEnv[pt]]*beta**ptToPot[pt] for pt in pg]) # create wavefunction
    norm = math.sqrt(np.sum(wf_var**2))
    wf_var /= norm

    wf_var0 = np.array([coef0[ptToEnv[pt]] for pt in pg]) # create wavefunction
    norm = math.sqrt(np.sum(wf_var0**2))
    wf_var0 /= norm

    L = len(pg)
    wf_varUnif = np.ones(L)/math.sqrt(L)
    
    overlap.append(abs(np.dot(wf_num, wf_var)))
    overlap0.append(abs(np.dot(wf_num, wf_var0)))
    overlapUnif.append(abs(np.dot(wf_num, wf_varUnif)))

fig, ax = plt.subplots()
ax.plot(rinfs, overlapUnif, '*', label=r'$\langle \psi_{num} | \psi_{unif} \rangle$')    
ax.plot(rinfs, overlap0, '*', label=r'$\langle \psi_{num} | \psi_{var} \rangle$')
ax.plot(rinfs, overlap, 'o', label=r'$\langle \psi_{num} | \psi_{var}(\beta) \rangle$')

plt.xlabel(r"Number of inflations ($\simeq \log_\lambda(L)$)")
plt.ylabel("Overlap")
plt.xlim([1.8, 6.2])
plt.title("Overlap between numerical and test groundstates of AB tiling, pure hopping.")

legend = ax.legend(loc='lower left', shadow=False)
plt.savefig("groundstates_overlap.pdf")
plt.show()

""" arrows """
## import numerical wf
#ninf = 6
#wf_num = np.load('data/gs_wf_gen_'+str(ninf)+'.npy').flatten()
#
## construct the tiling
#square.it_sub(ninf) # inflations
#pg = QGraph.periodize(square._graph) # periodic tiling
#
#""" node positions """
#lift, pts = square.lift_tiling(0.) # lift in 4D
#proj = square.perp_proj() # projection matrix from physical to internal space
#pos = np.array([np.dot(proj, lift[pt]) for pt in pg]) # node positions in internal space
#real_space = np.array([(pt.real, pt.imag) for pt in pg]) # node positions in real space
#
## compute the arrow potential
#wg = square.integrate_arrow_field(B)
#ptToPot = nx.get_node_attributes(wg,'weight')
#pot = np.array([ptToPot[pt] for pt in pg])
#
## construct variational wavefunction
#ptToEnv = pt_to_env(pg) # create a pt -> local env dictionary
#wf_var = np.array([coef[ptToEnv[pt]]*beta**ptToPot[pt] for pt in pg]) # create wavefunction
#norm = math.sqrt(np.sum(wf_var**2))
#wf_var /= norm
#
#wf_var0 = np.array([coef0[ptToEnv[pt]] for pt in pg]) # create wavefunction
#norm = math.sqrt(np.sum(wf_var0**2))
#wf_var0 /= norm
#
## divide numerical wavefunction by the local variational coefficients
#lamb = 1.36 # the arrow field parameter as defined by Pavel
#ar = [math.log(wf/c)/math.log(lamb) for wf, c in zip(wf_num, wf_var)]
#
## log of the wf divided by the predicted arrow part; this is supposed to be the local env part
#logloc = [math.log(wf*lamb**v) for wf,v in zip(wf_num,pot)]
#
## roughly simulate the behaviour of the numerical wf
#f_sim = [C*lamb**(-v) for C,v in zip(wf_var,pot)]