# -*- coding: utf-8 -*-
"""
Created on Fri Nov 25 14:18:17 2016

@author: nicolas

This script to test that there is the same number of nodes in both sublattices when the AB graph is bipartite
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph
import AB_envs as envs

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from networkx.algorithms import bipartite

sys = tl.A5(envs.squareCanonical)
n = 4
sys.it_sub(n)
print(bipartite.is_bipartite(sys._graph))
Ga, Gb = bipartite.sets(sys._graph)
print((len(Ga) - len(Gb))/(len(Ga)+len(Gb)))