# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 14:53:18 2016

@author: nicolas

Compute recursively the height distribution for finite-size systems
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
""" import the other stuff """
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import minimize_scalar
from scipy.signal import argrelextrema

lb = 1.+math.sqrt(2)
mu = math.sqrt(2)-1.

def interpolate_maximum(x, y, bounds):
    """
    return the maximum x0 of the function y(x)
    """
    # interpolate the function
    func = interp1d(x, -y, kind='cubic', assume_sorted=True)
    # find the maximum within the bounds
    res = minimize_scalar(func, bounds=bounds)
    if res.success:
        return res.x
    else:
        return None
    
def find_max(x, y):
    pos = argrelextrema(y, np.greater)
    if len(pos) == 1:
        return x[pos]
    else:
        return None

        
class HeightDistribution:
    """
    A container class to store the height distribution of the AB tiling.
    It should be initialized with a dictionary whose keys are the initial heights, and values the corresponding frequencies (not necessarily normalized).
    
    The freq(h) method returns an array whose ith components is proportional to the frequency of the event "env i with height h".
    NOTE: you must normalize the output of freq(h) to get the frequencies!!
    
    Environments are ordered by increasing distance from the center of the window: {0:A, 1:B, 2:C, 3:D1, ...7:F}.
    The evolve method updates the height distribution as if two inflations steps were performed.
    """
    def __init__(self, heightToFreq0):
        self._htf = heightToFreq0 # keys:heights, values: np array of frequencies having this height
        h0 = heightToFreq0.keys()
        self._hMin = min(h0)
        self._hMax = max(h0)
        
        self._m0 = np.zeros((7,7))
        self._m1 = np.zeros((7,7))
        self._m0[0,0] = self._m0[0,1] = self._m0[0,2] = self._m0[0,3] = self._m0[1,4] = self._m0[2,5] = self._m0[3,6] = 1.
        self._m1[4,6] = 1.
        self._m1[5,4] = 2.
        self._m1[5,5] = 3.
        self._m1[5,6] = 2.
        self._m1[6,0] = self._m1[6,1] = self._m1[6,2] = self._m1[6,3] = 8.
        self._m1[6,4] = 5.
        self._m1[6,5] = 2.
        
        self._tl = np.dot(self._m0, self._m1)
        self._t0 = np.dot(self._m0, self._m0) + np.dot(self._m1, self._m1)
        self._tr = np.dot(self._m1, self._m0)
        
        self._parity = 0
        
    def freq(self, height):
        freq = np.zeros(7)
        if self._hMin <= height <= self._hMax:
            freq = self._htf[height]
            
        return freq
            
    def evolve_twice(self):
        newhtf = dict()
        for h in range(self._hMin-1, self._hMax+2):
            # lb**4 is the growth rate of the number of envs. Dividing by it ensures htf values stay bounded.
            newhtf[h] = (np.dot(self._tl, self.freq(h-1)) + np.dot(self._t0, self.freq(h)) + np.dot(self._tr, self.freq(h+1)))/lb**4
        self._htf = newhtf
        self._hMin -= 1
        self._hMax += 1
        
        return None
        
    def evolve(self):
        newhtf = dict()
        newMax = -self._hMin
        newMin = -self._hMax-1
        for h in range(newMin, newMax+1):
            # lb**2 is the growth rate of the number of envs. Dividing by it ensures htf values stay bounded.
            newhtf[h] = (np.dot(self._m0, self.freq(-h)) + np.dot(self._m1, self.freq(-h-1)))/lb**2
        self._htf = newhtf
        self._hMin = newMin
        self._hMax = newMax
        
        return None
        
    
#htf0 = {0:np.array([1,0,0,0,0,0,0])}
fenvs = [mu**4,mu**5,2*mu**4,mu**3,mu**3,2*mu**2,mu]
htf0 = {0:np.array(fenvs)} # start with a "statistically infinite" tiling
distro = HeightDistribution(htf0)

tmax = 400
nsites = []
for t in range(tmax):
    distro.evolve_twice()
        
x = np.array(list(distro._htf.keys()), dtype=float)
x = np.sort(x)
y = np.array([distro._htf[xx] for xx in x])
x /= np.sqrt(2*tmax)
y /= np.sum(y)

L = 7 # number of local environments
freqs = [] # list of the total frequencies of local env
maxs = [] # list of maxima of the Gaussians
shifts = np.array([ 0.,  0.,  1.,  2.,  1.,  0., -1.])
midpt = int(len(x)/2) # number of points below x=0
spread = int(2*np.sqrt(2*tmax)) # typical number of pts we need to correctly interpolate the Gaussian
low = midpt - spread
up = midpt + spread
i = 0
for yy in np.transpose(y):
    """ renormalize the data so that interesting features appear for x, y of order 1 """
    yy *= np.sqrt(np.pi*2*tmax)/fenvs[i] # multiply by sqrt(pi*t), divide by the frequency of the env
    """ find the maximum of the Gaussian """
    xmax = interpolate_maximum(x[low:up], yy[low:up], (-.1,.1)) # interpolate the Gaussian and find its maximum
    maxs.append(xmax)
    """ plot """
    col = plt.cm.viridis(i/(L-1)) # color of the plot
    plt.plot(x-maxs[i], yy, c = col) # we translate the x axis so that the maximum is at x=0
    i += 1
    
# theoretical prediction for the Gaussian function
diff = 1/(3*np.sqrt(2)) # diffusion coefficient
gauss = yy[midpt]*np.exp(-x**2/(2*diff))
plt.plot(x, gauss, c = col)

plt.xlabel(r'$x = h/\sqrt{t}$')
plt.ylabel(r'$\sqrt{\pi t} P_\mu(x + x_\mu)/f_\mu$')
plt.title('Frequencies of local environments, as a function of height')
delta = 2*1e-3
plt.axis([-2, 2, 0-delta, max(yy)+delta])
plt.grid(True)

plt.show()