# -*- coding: utf-8 -*-
"""
Created on Thu May 12 16:20:44 2016

@author: nicolas

Compute numerically the IPR for the (mimic) ground state psi(i) = beta^h(i)
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress

# number of inflations
ninf = 7

# list storing the IPRs
iprs = []
# list storing the system sizes
sizes = []
for i in range(1, ninf):    
    # load the numerical gs
    wf = np.load('data/gs_wf_gen_'+str(i)+'.npy').flatten()
    # compute the ipr
    ipr = np.sum(wf**4)/np.sum(wf**2)**2
    iprs.append(ipr)
    # system size
    sizes.append(len(wf))
    
logIprs = np.log(iprs)
logS = np.log(sizes)

# fit
slope, intercept, r_value, p_value, std_err = linregress(logS,logIprs)

fig, ax = plt.subplots()
# plot numerics
ax.plot(logS, logIprs, 'og', label=r'numerics')
# plot fit
x = np.linspace(min(logS), max(logS), num = 50)
y = intercept + slope*x
ax.plot(x, y, 'r--', label='fit, scaling = ' + str(np.around(slope,4)))

legend = ax.legend(loc='lower left', shadow=False)
plt.xlabel(r"$\log(N)$ (log number of sites)")
plt.ylabel(r"$log IPR(\psi, N)$ (for $\psi = $ ground state)")
plt.title("Scaling of the IPR of the numerical ground state.")
plt.savefig("IPR_numerical_ground_state.png", dpi=200)
plt.close()

