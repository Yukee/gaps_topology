# -*- coding: utf-8 -*-
"""
Created on Tue May 10 20:23:32 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
""" import the other stuff """
import math
import numpy as np
import matplotlib.pyplot as plt

import Tilings as tl
import QuantumGraph as QGraph

e = np.exp(1j*np.pi*np.arange(4)/4)
lb = 1+np.sqrt(2)

def square(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not math.isclose(np.vdot(ea, eb).real, 0, abs_tol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
def unpack(plist):
    xy = [(p.real, p.imag) for p in plist]
    return zip(*xy)

# start with a square 
tiling = tl.A5(square(0j, e[0], e[2]))

# number of inflations
ninf = 7
tiling.it_sub(ninf)

wg = tiling.integrate_arrow_field(0j)

weights = np.array([p[1]['weight'] for p in wg.nodes(data='weight')])

# hist, bin_edges = np.histogram(weights, density=True)
n, bins, patches = plt.hist(weights, 5+int(ninf/2), normed=1, facecolor='green', alpha=0.75)

D = 1./(6*np.sqrt(2))
t = ninf
x = np.arange(-5,5,0.1)
y = np.exp(-x**2/(4*D*t))/np.sqrt(4*np.pi*D*t)
plt.plot(x, y, 'r--', linewidth=1)
plt.savefig("sample_height_distribution.png", dpi=200)