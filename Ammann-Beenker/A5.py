# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 16:50:23 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the other stuff """
import math, cmath
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx # graph tools
import QuantumGraph as QGraph

""" start with a wheel of lozenges """

## construct the wheel
#wheel0 = []
#sq2 = math.sqrt(2)
#A = 0
#for i in range(8):
#    B = cmath.rect(1, (i) * math.pi / 4)
#    D = cmath.rect(1, (i + 1) * math.pi / 4)
#    C = B + D
#    wheel0.append((0, (A, B, C, D)))
#    
#wheel = tl.A5(wheel0)
## create the Tiling object
#
## number of inflations
#ninf = 2
#
#wheel.it_sub(ninf)
#g = wheel._graph
#wheel.draw((1000, 1000), 1)
#wheel.write_to_png("wheel.png")

""" lift and perp space representation """
## compute Octonacci numbers
#def o(n):
#    a, b = 0, 1
#    for i in range(n):
#        a, b = b, a + 2*b
#    return a
#
## projection on the perp plane after n inflations
#def perpn(n):
#    r = o(n-1)/o(n)
#    return np.array([[2, -(1 + r), 0, 1 + r], [0, 1 + r, -2, 1 + r]])/(2.*sq2)
#        
#g = wheel._graph
#lift, pts = wheel.lift_tiling(0.)
#
#perppts = []
#for p in lift:
#    perppts.append(np.dot(perpn(ninf), lift[p]))
#perppts = np.array(perppts)
#
#plt.axes().set_aspect('equal')
#plt.plot(perppts[:,0], perppts[:,1], 'o', markersize=2., c='#00688B')
#
#plt.savefig('perpspace.pdf')

""" spectrum and eigenstates """
#h = nx.to_numpy_matrix(wheel._graph.to_undirected())
#val, vec = scipy.linalg.eigh(h)
#
#L = len(val)
#plt.plot(val, range(L))

""" arrows """
#dg = wheel._graph
#
## retrieve the node positions
#L=1
#list_pos = [(L*v.real, L*v.imag) for v in dg.nodes()]
## keys: nodes, entries: position of the nodes
#dict_pos = dict(zip(dg.nodes(), list_pos))
#
## drawing!
##nx.draw_networkx_edges(dg, dict_pos, width=0.1)
#plt.axes().set_aspect('equal')
#plt.savefig('directed_graph.pdf')
#
## integrate the arrow field
#wg = wheel.integrate_arrow_field(0)
#label_list = { pt:str(wg.node[pt]['weight']) for pt in wg.nodes() }
#
#nx.draw_networkx(wg, dict_pos, width=0.05, with_labels = True, node_size = 0., labels = label_list, font_size = 6)
#L = 1.8*(sq2-1)
#plt.axis([-L, L, -L, L])
#plt.axes().set_aspect('equal')
#plt.tick_params(
#    axis='both',          # changes apply to the x-axis
#    which='both',      # both major and minor ticks are affected
#    bottom='off',      # ticks along the bottom edge are off
#    top='off',         # ticks along the top edge are off
#    labelbottom='off',
#    right='off',
#    left='off',
#    labelleft='off') # labels along the bottom edge are off
#plt.savefig('directed_graph.pdf')

""" arrows in perp space """
# start with a square 
sq2 = math.sqrt(2)
# start with a square
square0 = []
# half-length of the square
r = 1.
# first half-square
C = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, -3*math.pi/4)
B = cmath.rect(r, -5*math.pi/4)
square0.append((r, (A, B, C)))
# second half-square
C = cmath.rect(r, -5*math.pi/4)
B = cmath.rect(r, -math.pi/4)
A = cmath.rect(r, math.pi/4)
square0.append((r, (A, C, B)))
square = tl.A5(square0)

# number of inflations 
ninf = 2
square.it_sub(ninf)
    
#wg = square.integrate_arrow_field(A)
#def minmax(nint):
#    square.it_sub(nint)
#    wg = square.integrate_arrow_field(A)
#    print(max([p[1]['weight'] for p in wg.nodes(data='weight')]))
#    print(min([p[1]['weight'] for p in wg.nodes(data='weight')]))
    
""" real space """

## retrieve the node positions
#list_pos = [(v.real, v.imag) for v in g.nodes()]
## keys: nodes, entries: position of the nodes
#dict_pos = dict(zip(g.nodes(), list_pos))
#
#weight_list = [p[1]['weight'] for p in wg.nodes(data='weight')]
#
#nx.draw_networkx(wg, dict_pos, width=0.01, with_labels = False, node_size = 12, node_color = weight_list)
#plt.axes().set_aspect('equal')
#plt.savefig('arrows.pdf')
#plt.show()

""" perp space """
wg = square.integrate_arrow_field(A)
color = [p[1]['weight'] for p in wg.nodes(data='weight')]
proj = square.perp_proj()
lift, pts = square.lift_tiling(0.)

# node positions in perp space
list_pos = np.array([np.dot(proj, lift[pt]) for pt in wg])
x, y = list_pos.T

# drawing!
sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=1)
plt.colorbar(sc)
plt.axes().set_aspect('equal')
plt.savefig('arrows_perp.pdf')
plt.show()


""" periodized graph """

g = square._graph
pg = QGraph.periodize(g)
## coordinance list
#z = [len(pg.neighbors(pt)) for pt in pg]
#ptToZ = dict(zip(pg.nodes(),z))
#square.plot_perp_field(ptToZ, 15, 1, 8)

""" Ansatz for the wavefunction """
# the wf coefficients as a function of the local environment
coef = {'A':0.480471, 'B':0.442286, 'C':0.404101, 'D1':0.366701, 'D2':0.349018, 'E':0.299556, 'F':0.25341}

# determine the local environments on the tiling
zToEnv = {8:'A', 7:'B', 6:'C', 4:'E', 3:'F'}
# D1 and D2 are distinguished by their local environment
ptToEnv = {}
for pt in pg:
    n = pg.neighbors(pt)
    z = len(n)
    # if pt is a D type site, distinguish between D1 and D2
    if z == 5:
        # D2 sites have in their neighbourhood 2 F sites
        if 3 in [len(pg.neighbors(pn)) for pn in n]:
            ptToEnv[pt] = 'D2'
        else:
            ptToEnv[pt] = 'D1'
    else:
        ptToEnv[pt] = zToEnv[z]
        
varPsi = {pt:coef[ptToEnv[pt]] for pt in pg}
m = min(coef.values())
M = max(coef.values())
square.plot_perp_field(varPsi, 15, m, M)

""" numerical calculation of the ground state """
from scipy.sparse.linalg import eigsh
h = nx.to_scipy_sparse_matrix(pg, dtype=float)
en, wf = eigsh(h, k=1)
ptToWf = dict(zip(pg.nodes(),wf))
minWf = min(wf)
maxWf = max(wf)
#square.plot_perp_field(ptToWf, 6, minWf, maxWf)

color = wf
# save wafunction to data to file
np.save('data/gs_wf_gen_'+str(ninf), wf)
proj = square.perp_proj()
lift, pts = square.lift_tiling(0.)

# node positions in perp space
list_pos = np.array([np.dot(proj, lift[pt]) for pt in pg])
x, y = list_pos.T

# drawing!
sc = plt.scatter(x, y, c=color, edgecolor='', marker='o', s=1.2)
plt.colorbar(sc)
plt.axes().set_aspect('equal')
plt.savefig('gs_wf_gen_'+str(ninf)+'.pdf')
plt.show()


#val, vec = QGraph.diagonalize(pg)
#QGraph.saveplot(pg, "periodic.pdf")
#QGraph.saveplot(g, "free.pdf", 2)