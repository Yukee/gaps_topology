# -*- coding: utf-8 -*-
"""
Created on Fri Nov 25 17:52:30 2016

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
import Tilings as tl
import QuantumGraph as QGraph
import AB_envs as envs

""" import the other stuff """
import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import linregress
from scipy.optimize import curve_fit

cellType = "canonical"
system = tl.A5(envs.squareCanonical)

# mean energy of the gap for several approximants
ns = range(4,8)
GapEnergy = [-1.9326888638629725, -1.8936718341577778, -1.8791049353004352, -1.8752506917454201]
GapEnergy = dict(zip(ns, GapEnergy))

# list of all the eigenvectors
wfs = []
# find the corresponding eigenstate (above the gap)
for n in range(4,8):
    try:
        vec = np.load('/home/nicolas/git/gaps_topology/Ammann-Beenker/data/' +  'state_above_gap_' + cellType + "_n_" + str(n) + ".npy")
    except FileNotFoundError:
        system.it_sub(n)
        cell = QGraph.SquareCell(system._graph)
        E = GapEnergy[n]
        val, vec = cell.state_above(-1., (0.,0.), E)
        print("E = ", val)
        # save energy and eigenvector
        np.save('/home/nicolas/git/gaps_topology/Ammann-Beenker/data/' +  'state_above_gap_' + cellType + "_n_" + str(n), vec)
    # in all cases, append the wavefunction to the list
    wfs.append(vec)
        
"""
compute the fractal exponents
"""
def tau(wfs, q, threshold=None, check=False):
    """
    Return the exponent tau(q)
    """
    
    # list storing the IPRs
    iprs = []
    # list storing the system sizes
    sizes = []
    for wf in wfs:
        # compute the ipr
        ipr = np.sum(wf**2.)**q/np.sum(wf**(2.*q))
        iprs.append(ipr)
        # system size
        sizes.append(len(wf))
    
    logIprs = np.log(iprs)
    # if we set a threshold, keep only sites on which wavefunction is larger than the threshold
    if threshold:
        logS = []
        for wf in wfs:
            # renormalize threshold by the largest coefficient
            threshold *= np.max(wf)
            # count only sites above threshold
            S = np.sum([amp > threshold for amp in np.abs(wf)])
            logS.append(np.log(S))
    else:
        logS = np.log(sizes)
    
    # fit
    slope, intercept, r_value, p_value, std_err = linregress(logS,logIprs)
    
    # if check, print the data + the guessed slope
    if check:
        fig, ax = plt.subplots()
        # plot data
        ax.plot(logS, logIprs, 'og', label=r'data')
        # plot slope
        x = np.linspace(min(logS), max(logS), num = 50)
        y = slope*x + intercept
        ax.plot(x, y, 'r--', label='fit')
        
        legend = ax.legend(loc='lower left', shadow=False)
        plt.xlabel(r"$\log N$")
        plt.ylabel(r"$\chi_q(\psi), q = $" + str(q))
        #plt.title(r"Fractal exponents, groundstate")
        #plt.savefig("fractal_exponents_groundstate.png", dpi=200)
        plt.title(r"Scaling of the q moment of the wavefunction and fit")
        plt.show()
    
    return slope
    
def om(z):
    return (4.+9.*z+4.*z**2+2.*np.sqrt(2)*np.sqrt(2.+9.*z+14.*z**2+9.*z**3+2.*z**4))/z
vom = np.vectorize(om)
# the normalization in the theoretical expression of the exponent
norm = om(1.)

def tau_theo(q, betasq):
    """
    The function to fit
    """
    return np.log(vom(betasq)**q/vom(betasq**q))/np.log(norm)
    
def d_theo(q, betasq):
    return np.log(vom(betasq)**q/vom(betasq**q))/((q-1.)*np.log(norm))
    
# minimum value of q
q1 = 2.
# list of q indices we want to compute the scaling for
qlist = np.arange(q1, 30, 2.)
taulist = []
dlist = []
for q in qlist:
    tauq = tau(wfs, q, None, False)
    taulist.append(tauq)
    dq = (tauq)/(q-1.)
    dlist.append(dq)
    
# fit
betasqfit, err = curve_fit(d_theo, qlist, dlist)
betasqfit = betasqfit[0]

fig, ax = plt.subplots()
# plot numerics
ax.plot(qlist, dlist, 'og', label=r'numerics')
# plot theory
betasquare = betasqfit #1.35809
x = np.linspace(min(qlist), max(qlist), num = 50)
y = np.log(vom(betasquare)**x/vom(betasquare**x))/((x-1.)*np.log(norm))
ax.plot(x, y, 'r--', label='theory')

legend = ax.legend(loc='lower left', shadow=False)
plt.xlabel(r"$q$")
plt.ylabel(r"$D_q$")
#plt.title(r"Fractal exponents, groundstate")
#plt.savefig("fractal_exponents_groundstate.png", dpi=200)
plt.title(r"Fractal exponents, state idos = $1-1/\lambda^2 + 0^+$")
plt.savefig("fractal_exponents_" + cellType + ".png", dpi=200)
plt.show()
plt.close()
