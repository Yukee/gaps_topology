# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 17:04:23 2016

@author: nicolas

Draw the boundaries of the local environments in the internal space, for A5
"""

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
    
# the projection matrix
sq = np.sqrt(2.)
perp = 0.5*np.array([[sq, -1, 0, 1], [0, 1, -sq, 1]])

def proj_graph(graph):
    """ 
    project the graph onto the perp space
    """
    pg = g.copy()
    for p in graph.nodes():
        pp = np.dot(perp, p)
        pg.node[p]['proj'] = pp
        
    return pg
    
def plot(pgraph):
    """
    plot a projected graph
    """
    dict_pos = nx.get_node_attributes(pgraph, 'proj')
    plt.axes().set_aspect('equal')
    nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 0)        

eperp = np.array([[1,0], [-1/sq, 1/sq], [0, -1], [1/sq, 1/sq]])
e = np.array([[1,0], [1/sq, 1/sq], [0,1], [-1/sq,1/sq]])
L = eperp[0] + eperp[1] + eperp[2]
S = eperp[1]
#L = e[0]+e[1]+e[2]
#S = e[1]
    
def translate(vs, t):
    """ 
    translate vectors in the set vs by the vector t
    """
    return [v + t for v in vs]
    
def concatenate(w1, w2):
    """
    concatenate two worms w1 and w2.
    worms are ordered sets of colinear vectors.
    concatenating means translating all vectors of w2 by the largest vector of w1.
    """
    v = w1[-1] # largest vector of w1
    return w1 + translate(w2, v)    
    
def update_worm(w1, w2):
    """
    update a worm according to the silver ratio sequence
    """
    return [w2, concatenate(concatenate(w1, w2), w2)]
    
def worm(n, w0 = [S], w1 = [L]):
    """
    create the nth worm following the silver sequence
    """
    for i in range(n):
        w0, w1 = update_worm(w0, w1)
    return w1
    
n = 2
g = nx.grid_graph([n,n,n,n])
pg = proj_graph(g)
x, y = np.array(worm(6)).transpose()
plt.plot(x, y, 'o')
plot(pg)
