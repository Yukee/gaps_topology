# -*- coding: utf-8 -*-
"""
Created on Fri May 13 11:23:12 2016

@author: nicolas

Compute the IPR numerically on Pavel's approximants, for the Sutherland wavfunction with beta = betaPavel.
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
""" import the other stuff """
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress

import Tilings as tl
import QuantumGraph as QGraph

import collections


e = np.exp(1j*np.pi*np.arange(4)/4)
lb = 1+np.sqrt(2)

def triang(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not math.isclose(np.vdot(ea, eb).real, 0, abs_tol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t2]
    
def IPR(stat, beta):
    print(stat)
    return np.sum(beta**(4*h)*stat[h] for h in stat)/np.sum(beta**(2*h)*stat[h] for h in stat)**2

# start with a square 
tiling = tl.A5(triang(0j, e[0], e[2]))

tiling.it_sub(1)

#plt.plot(1, 0, 'o')

betas = np.sqrt([1.31023580279858,
1.36478815968518,
1.35739594032671,
1.35821790197493,
1.35805783747795,
1.35808037029283])

stats = []
sizes = []
iprs = []
for i in range(6):
    tiling.it_sub(1)
    sizes.append(len(tiling._graph))
    
    wg = tiling.integrate_arrow_field(1j)
    weights = np.array([-p[1]['weight'] for p in wg.nodes(data='weight')])
    heights = set(weights)
    #hist, bin_edges = np.histogram(weights, density=False, bins = len(heights))
    stat = dict(collections.Counter(weights))    
    stats.append(stat)
    
    ipr = IPR(stat, betas[i])
    iprs.append(ipr)
    

logIprs = np.log(iprs)
logS = np.log(sizes)

# fit
slope, intercept, r_value, p_value, std_err = linregress(logS,logIprs)

fig, ax = plt.subplots()
# plot numerics
ax.plot(logS, logIprs, 'og', label=r'numerics')
# plot fit
x = np.linspace(min(logS), max(logS), num = 50)
y = intercept + slope*x
ax.plot(x, y, 'r--', label='fit, scaling = ' + str(np.around(slope,4)))

legend = ax.legend(loc='lower left', shadow=False)
plt.xlabel(r"$\log(N)$ (log number of sites)")
plt.ylabel(r"$log IPR(\psi, N)$ (for $\psi = \beta^h$)")
plt.title("Scaling of the IPR of a mimic ground state.")
plt.savefig("IPR_Pavel.png", dpi=200)
plt.close()

