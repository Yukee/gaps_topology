# -*- coding: utf-8 -*-
"""
Created on Mon May  9 10:58:54 2016

@author: nicolas

Measure lambda numerically
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../Tilings') # prepend the path to the Tiling package
""" import the other stuff """
import math
import numpy as np
import matplotlib.pyplot as plt

import Tilings as tl
import QuantumGraph as QGraph

e = np.exp(1j*np.pi*np.arange(4)/4)
lb = 1+np.sqrt(2)

def square(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not math.isclose(np.vdot(ea, eb).real, 0, abs_tol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
def unpack(plist):
    xy = [(p.real, p.imag) for p in plist]
    return zip(*xy)

# start with a square 
tiling = tl.A5(square(0j, e[0], e[2]))

# number of inflations 
ninf = 1
tiling.it_sub(ninf)

# mark the nodes 
p1 = 0j
p2 = (p1 + e[1] + e[0] + e[2])/lb
p = [p1, p2]
plt.scatter(*unpack(p))

QGraph.plot(tiling._graph, lims=((0, 1), (0,1)))


"""
numerical calculations with the ground state
"""

pg = QGraph.periodize(tiling._graph)

DEC = 15
p1 = np.around(p1, decimals=DEC)
p2 = np.around(p2, decimals=DEC)

ratios = []
rinfs = range(2,8)
for ninf in rinfs:
    # load the numerical ground state
    gs = np.load('data/gs_wf_gen_'+str(ninf)+'.npy').flatten()
    
    # inflate the tiling
    tiling.it_sub(ninf)
    # periodize it
    pg = QGraph.periodize(tiling._graph)
    # find the points p1 and p2, and compute the ratio of their amplitudes
    i = 0
    for p in pg.nodes():
        psi_cur = gs[i]
        if p == p1:
            psi1 = psi_cur
        if p == p2:
            psi2 = psi_cur
        i += 1
    if ninf%2 == 0:
        ratios.append(psi1/psi2)
    else:
        ratios.append(psi2/psi1)
