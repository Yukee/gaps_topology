#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 16:12:52 2017

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../../gaps_topology/Tilings') # prepend the path to the Tiling package
import Tilings as tl
import Penrose_envs as envs
""" import the rest """
import numpy as np
import networkx as nx # graph tools
from scipy.sparse import csr_matrix

def vec(p):
    return np.array([p.real, p.imag])

def affect_order(approx, ninfl):
    """
    Affect to the nodes the "order" attribute that encodes their symmetry.
    /!\ This programm assumes the orientation and coordinates of the apexes of the triangle.
    """
        
    # all bulk sites have symmetry order = 1
    nx.set_node_attributes(approx._graph, 'order', 1.)
    # an attribute whether a node is edge or bulk, and if it is edge, which one
    nx.set_node_attributes(approx._graph, 'edge', "bulk")
    # compute the Z4 coordinates of the apexes of the triangle
    e = envs.e
    A = 0
    B = -e[4]
    C = -e[1]
    # round the nodes to identify them with the graph nodes
    if tl.ROUND:
        A = np.around(A, tl.DEC)
        B = np.around(B, tl.DEC)
        C = np.around(C, tl.DEC)

    # the borders have symmetry order = 1/2
    oe = np.exp(1j*np.pi*(np.arange(5)/5+1./10.))
    ep0 = vec(oe[2])
    ep1 = vec(oe[3])
    ep4 = vec(oe[1])
    transl = vec(B)
    for p in approx._graph:
        pos = vec(p)
        if np.isclose(np.dot(pos, ep1), 0):
            approx._graph.node[p]['order'] = .5
            approx._graph.node[p]['edge'] = '1'
        elif np.isclose(np.dot(pos-transl, ep0), 0):
            approx._graph.node[p]['order'] = .5
            approx._graph.node[p]['edge'] = '2'
        elif np.isclose(np.dot(pos, ep4), 0):
            approx._graph.node[p]['order'] = .5
            approx._graph.node[p]['edge'] = '3'
    # the corners have symmetry order = 1/8 or 1/4
    approx._graph.node[A]['order'] = 3./10.
    approx._graph.node[A]['edge'] = 'corner'
    approx._graph.node[B]['order'] = 1./10.
    approx._graph.node[B]['edge'] = 'corner'
    approx._graph.node[C]['order'] = 1./10.
    approx._graph.node[C]['edge'] = 'corner'
                          
def symmetric_hams(graph):
    """
    Return the on-site and hopping Hamiltonians of the eightfold symmetric reprensetation of an SKK approximant.
    The SKK graph must hold the "order" attribute that encodes the symmetry of a site.
    """
    # return the order of a site
    order = nx.get_node_attributes(graph, "order")
    # return the edge type of a site
    edge = nx.get_node_attributes(graph, "edge")
    """ hopping part of the Hamiltonian """
    # associate a unique integer index to each node
    index = dict(zip(graph.nodes(), range(len(graph))))
    # list of indices of sites linked by an edge
    rows = []
    cols = []
    # jump amplitudes
    hoppings = []
    # loop over edges
    for p1, p2 in graph.edges():
        # we add the indices of the pairs (p1, p2) and (p2, p1)
        rows += [index[p1], index[p2]]
        cols += [index[p2], index[p1]]
    
        s1 = order[p1]
        s2 = order[p2]    
        # if at least one site is bulk
        s12 = 1.
        # check that both sides are edge
        if s1 < 1 and s2 < 1:
            # check that they are on the same edge or that one site is corne
            if edge[p1] == edge[p2] or edge[p1] == 'corner' or edge[p2] == 'corner':
                s12 = 0.5
        hop12 = -s12/np.sqrt(s1*s2)
        hoppings += [hop12, hop12]   
    # construct the hopping part of the Hamiltonian
    L = len(graph)
    Hhop = csr_matrix((hoppings, (rows, cols)), shape=(L,L))
    
    """ on-site part of the Hamiltonian """
    # coordination of the sites
    coords = []
    # loop over sites
    for p in graph:
        neighs = graph.neighbors(p)
        coord = len(neighs)
        # if the site is not bulk
        o = order[p]
        if o < 1:
            ed = edge[p]
            # neighbors which are not along the same edge or at a corner of the triangle
            inner_neighs = [n for n in neighs if edge[n] != ed and edge[n] != "corner"]
            # inner neighbors are reflected and thus counted twice
            coord += len(inner_neighs)
            # the 3 corners of the triangle are fivefold
            if np.isclose(o, 3./10.) or np.isclose(o, 1./10.):
                coord = 5.
        coords.append(coord)
    # cosntruct the on-site Hamiltonian
    ind = range(L)
    Honsite = csr_matrix((coords, (ind, ind)), (L, L))
    
    return Honsite, Hhop

def coord_func_ham(graph):
    V = {3:1., 4:0, 5:0, 6:0, 7:0, 8:0}
    # return the order of a site
    order = nx.get_node_attributes(graph, "order")

    """ on-site part of the Hamiltonian """
    # coordination of the sites
    coords = []
    # loop over sites
    for p in graph:
        neighs = graph.neighbors(p)
        coord = len(neighs)
        # if the site is not bulk
        o = order[p]
        if o < 1:
            inner_neighs = [n for n in neighs if order[n] > 0.9]
            # inner neighbors are reflected and thus counted twice
            coord += len(inner_neighs)
            # the 3 apexes of the triangle are eightfold
            if o < 0.4:
                coord = 8
        coords.append(V[coord])
    # cosntruct the on-site Hamiltonian
    L = len(graph)
    ind = range(L)
    Honsite = csr_matrix((coords, (ind, ind)), (L, L))
    
    return Honsite


def torealspace(graph, psi):
    """
    transform points in the symmetric representation to real space
    """
    s = [p[1]['order'] for p in graph.nodes(data=True)]
    psireal = psi/np.sqrt(s)
    return psireal


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from scipy.sparse.linalg import eigsh
    
    ninfl = 8
    # create the approximant
    e = envs.e
    orig = 0
    prototile = envs.loz(0, -e[1], -e[4])
    prototile = (prototile[0],)
    approx = tl.Penrose(prototile)
    # inflate it
    approx.it_sub(ninfl)
    
    points = np.array([[v.real, v.imag] for v in e])
    oe = np.exp(1j*np.pi*(np.arange(5)/5+1./10.))
    opts = np.array([[v.real, v.imag] for v in oe])
    # set up the edges
    affect_order(approx, ninfl)
#    # retrieve the graph
#    graph = approx._graph
    
    V = 1.5
    Honsite, Hhop = symmetric_hams(approx._graph)
    H = V*Honsite + Hhop
    En, gs = eigsh(H, k=1, which = "SA")
    gs = np.abs(gs.flatten())
    gs = torealspace(approx._graph, gs)

    """
    scatter plot
    """
    x, y = np.transpose([(p.real, p.imag) for p in approx._graph])
    # point size
    s = 10.
    # color is order
    order = nx.get_node_attributes(approx._graph, 'order')
    c = gs#[order[p] for p in approx._graph]
    # plot!
    plt.scatter(x, y, s=s, c=c, edgecolor='', cmap="viridis")
    plt.colorbar()
    plt.axes().set_aspect('equal')
    plt.savefig("SKK_groundstate.png", dpi=200)
    
    """
    networkx plot
    """
#    index = dict(zip(approx._graph.nodes(), range(len(approx._graph))))
#    
#    tau = (1.+np.sqrt(5))*0.5
#    p1 = -e[1]/tau**2
#    if tl.ROUND:
#        p1 = np.around(p1, tl.DEC)
#        p2 = np.around(-e[4], tl.DEC)
#    node_size = [2.*(q == p1 or q == p2) for q in approx._graph]
#    
#    col = []
#    for e in approx._graph.edges():
#        p1, p2 = e
#        i1 = index[p1]
#        i2 = index[p2]
#        t = Hhop[i1, i2]
#        test = np.sqrt(5)
#        if np.isclose(-test, t):
#            col.append('r')
#        else:
#            col.append('b')
#
#    pos = {p:(p.real, p.imag) for p in approx._graph}
#    nx.draw_networkx(approx._graph, pos, with_labels=False, node_size=0., width=.1, node_color=gs, edge_color = col)
#    plt.axes().set_aspect('equal')
#    plt.savefig('penrose_mirror.pdf')
