#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 14:15:16 2017

@author: nicolas
"""

""" import the Tilings package... """
import sys
sys.path.insert(0, '../../gaps_topology/Tilings') # prepend the path to the Tiling package
import Tilings as tl
""" import the rest """
import numpy as np
import networkx as nx # graph tools
from scipy.sparse import csr_matrix
from networkx.algorithms import bipartite

def affect_order(approx, ninfl):
    """
    Affect to the nodes the "order" attribute that encodes their symmetry.
    /!\ This programm assumes the orientation and coordinates of the apexes of the triangle.
    """
        
    # all bulk sites have symmetry order = 1
    nx.set_node_attributes(approx._graph, values=1., name='order')
    # compute the Z4 coordinates of the apexes of the triangle
    Mi = np.linalg.matrix_power(approx._M, ninfl) # inflation matrix
    B0 = np.zeros(4)
    A0 = np.array([1,0,0,0])
    C0 = np.array([1,0,1,0])
    Bi = tuple(np.dot(Mi, B0))
    Ai = tuple(np.dot(Mi, A0))
    Ci = tuple(np.dot(Mi, C0))
    # the borders have symmetry order = 1/2
    pos = nx.get_node_attributes(approx._graph, 'para')
    e = np.eye(4)
    ep0 = np.dot(approx.para(), e[0])
    ep2 = np.dot(approx.para(), e[2])
    ep3 = np.dot(approx.para(), e[3])
    Cip = np.dot(approx.para(), Ci)
    for p in approx._graph:
        para = pos[p]
        if np.isclose(np.dot(para-Cip, ep0), 0) or np.isclose(np.dot(para, ep2), 0) or np.isclose(np.dot(para, ep3), 0):
            approx._graph.node[p]['order'] = .5
    # the corners have symmetry order = 1/8 or 1/4
    approx._graph.node[Bi]['order'] = .125
    approx._graph.node[Ai]['order'] = .25
    approx._graph.node[Ci]['order'] = .125
                          
def symmetric_hams(graph):
    """
    Return the on-site and hopping Hamiltonians of the eightfold symmetric reprensetation of an SKK approximant.
    The SKK graph must hold the "order" attribute that encodes the symmetry of a site.
    """
    # return the order of a site
    order = nx.get_node_attributes(graph, "order")
    """ hopping part of the Hamiltonian """
    # associate a unique integer index to each node
    index = dict(zip(graph.nodes(), range(len(graph))))
    # list of indices of sites linked by an edge
    rows = []
    cols = []
    # jump amplitudes
    hoppings = []
    # loop over edges
    for p1, p2 in graph.edges():
        # we add the indices of the pairs (p1, p2) and (p2, p1)
        rows += [index[p1], index[p2]]
        cols += [index[p2], index[p1]]
    
        s1 = order[p1]
        s2 = order[p2]    
        # if at least one site is bulk
        s12 = 1.
        # if both sites are edge
        if s1 < 1 and s2 < 1:
            s12 = 0.5
        hop12 = -s12/np.sqrt(s1*s2)
        hoppings += [hop12, hop12]   
    # construct the hopping part of the Hamiltonian
    L = len(graph)
    Hhop = csr_matrix((hoppings, (rows, cols)), shape=(L,L))
    
    """ on-site part of the Hamiltonian """
    # coordination of the sites
    coords = []
    # loop over sites
    for p in graph:
        neighs = graph.neighbors(p)
        coord = len(list(neighs))
        # if the site is not bulk
        o = order[p]
        if o < 1:
            inner_neighs = [n for n in neighs if order[n] > 0.9]
            # inner neighbors are reflected and thus counted twice
            coord += len(inner_neighs)
            # the 3 apexes of the triangle are eightfold
            if o < 0.4:
                coord = 8
        coords.append(coord)
    # cosntruct the on-site Hamiltonian
    ind = range(L)
    Honsite = csr_matrix((coords, (ind, ind)), (L, L))
    
    return Honsite, Hhop

def coord_func_ham(graph, V):
    """
    Return an on-site the Hamiltonian depending on the coordination of the sites only.
    V is a dictionary from coordination to the value of the potential, eg
    V = {3:1., 4:0, 5:0, 6:0, 7:0, 8:0}
    """
    # return the order of a site
    order = nx.get_node_attributes(graph, "order")

    """ on-site part of the Hamiltonian """
    # list of potentials
    pots = []
    # loop over sites
    for p in graph:
        neighs = graph.neighbors(p)
        coord = len(neighs)
        # if the site is not bulk
        o = order[p]
        if o < 1:
            inner_neighs = [n for n in neighs if order[n] > 0.9]
            # inner neighbors are reflected and thus counted twice
            coord += len(inner_neighs)
            # the 3 apexes of the triangle are eightfold
            if o < 0.4:
                coord = 8
        pots.append(V[coord])
    # construct the on-site Hamiltonian
    L = len(graph)
    ind = range(L)
    Honsite = csr_matrix((pots, (ind, ind)), (L, L))
    
    return Honsite

def bipartite_hams(graph):
    """
    SKK approximants are balanced bipartite graphs.
    Return two on-site Hamiltonians, each taking value 1 in one of the 2 parts of the graph.
    """
    g1, g2 = bipartite.sets(graph)
    # Hamiltonian of part 1
    h1 = []
    # Hamiltonian of part 2
    h2 = []
    for p in graph:
        if p in g1:
            h1.append(1.)
            h2.append(0.)
        else:
            h1.append(0.)
            h2.append(1.)
    L = len(graph)
    ind = range(L)
    H1 = csr_matrix((h1, (ind, ind)), (L,L))
    H2 = csr_matrix((h2, (ind, ind)), (L,L))
    return H1, H2

def affect_type(graph):
    """
    Affect to each site in graph its type (according to first neighbors environment)
    """
    Honsite, Hhop = symmetric_hams(graph)
    siteType = {3:'F', 4:'E', 6:'C', 7:'B', 8:'A'}
    index = dict(zip(graph.nodes(), range(len(graph))))
    for p in graph:
        neighs = graph[p]
        # the Honsite ham gives the right coordinence
        idx = index[p]
        z = Honsite[idx, idx]
        if z in siteType.keys():
            graph.node[p]['type'] = siteType[z]
        else:
            graph.node[p]['type'] = 'D1'
            if 3 in [Honsite[index[n], index[n]] for n in neighs]:
                graph.node[p]['type'] = 'D2'

def asym_ham(graph, V):
    """
    """
    try:
        graph.node[(0,0,0,0)]['type']
    except KeyError:
        print("Types not yet affected, running affectation method.")
        affect_type(graph)
    # dict to types
    typ = nx.get_node_attributes(graph, 'type')
    # perp proj
    perp = nx.get_node_attributes(graph, 'perp')
    # sequence of potentials
    pots = []
    for p in graph:
        pot = 0.
        neighs = graph.neighbors(p)
        # if p is an F site
        if typ[p] == 'F' and np.abs(perp[p][1]) <= 0.33:
            # vertices linking the node to its neighbors
            pv = np.asarray(p) # converting p to array
            verts = [pv - n for n in neighs]
            vert = np.sum(verts, 0)
            angl = np.arctan2(vert[1], vert[0])
            index = np.floor(4*angl/np.pi) + 3
            if index == 2:
                pot = V
        
        pots.append(pot)
    # construct the on-site Hamiltonian
    L = len(graph)
    ind = range(L)
    Honsite = csr_matrix((pots, (ind, ind)), (L, L))
    return Honsite

def torealspace(graph, psi):
    """
    transform points in the symmetric representation to real space
    """
    s = [p[1]['order'] for p in graph.nodes(data=True)]
    psireal = psi/np.sqrt(s)
    return psireal


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from scipy.sparse.linalg import eigsh
    
    ninfl = 1
    # construct the elementary triangle
    e = np.identity(4)
    orig = np.zeros(4)
    ea = e[0]
    eb = e[2]
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = [(1, (A, B, C))]
    # create the approximant
    approx = tl.A5(t1)
    # inflate it
    approx.it_sub(ninfl)
    # set up the edges
    affect_order(approx, ninfl)
    # retrieve the graph
    graph = approx._graph
    
    V = 0.
    Honsite, Hhop = symmetric_hams(graph)
    H = V*Honsite + Hhop

    En, gs = eigsh(H, k=1, which = "SA")
    gs = np.abs(gs.flatten())
    gs = torealspace(graph, gs)
    
    # construct the system and retrieve node positions
    pos = nx.get_node_attributes(graph, "para")
    x, y = np.transpose([pos[p] for p in graph])
    # point size
    s = 10.
    # plot!
    plt.scatter(x, y, s=s, c=gs, edgecolor='', cmap="viridis")
    plt.colorbar()
    plt.axes().set_aspect('equal')
    plt.savefig("SKK_groundstate.png", dpi=200)
