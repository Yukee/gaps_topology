# -*- coding: utf-8 -*-
"""
Created on Fri Sep  2 15:50:54 2016

@author: nicolas

Periodize the unit cell of an AB tiling.
/!\ the unit cell must be a "real" unit cell, with sites on only half the boundaries. 
In particular, the approximants produced by inflation are not real unit cells (they have extra boundary sites).
"""

import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

import CP as CP

def o(n):
    a, b = 0, 1
    for i in range(n):
        a, b = b, a + 2*b
    return a

def lattice(n):
    a = o(n)
    b = o(n-1)
    return np.array([[a+b,a,0,-a],[0,a,a+b,a]])
    
def unit_cell(p0, n):
    """
    return a region function delimitating a real space unit cell
    whose lower left corner is the point p0
    """
    A, B = lattice(n)
    # length of the unit cell of the nth approximant
    L = np.dot(CP.ABP(n, (0,0))._para, A)[0]
    # add a small length (smaller than 1/sq(2), the bond length), to account for boundary effects
    L -= 0.1/np.sqrt(2)
    
    def reg(p):
        x, y = p - p0
        return  0 <= x <= L and 0 <= y <= L
    return reg

e = np.identity(4, dtype=int) # Z4 basis
hops = np.concatenate((e, -e)) # the 8 possible hops on the tiling

n = 2 # number of inflation
orig = np.array([-0.05,  0.27149859]) # center of the window
tiling = CP.ABP(n, orig)
p0 = (2, 0, -3, -2) # initial point
p0para = np.dot(tiling._para, p0)
tiling.in_region = unit_cell(p0para, n) # compute the points in only one unit cell
g = tiling.compute_graph(p0)

# get the positions of the nodes
pos = np.asarray(list(nx.get_node_attributes(g, 'para').values()))
# find the extreme points
x, y = zip(*pos)
xmin = min(x) # leftmost abscissa
xmax = max(x) # rightmost abscissa
ymin = min(y) # bottommost ordinate
ymax = max(y) # topmost ordinate

# tolerance is one percent of the average distance between two nodes
av_dist = min(xmax-xmin, ymax-ymin)/np.sqrt(len(x))    
tol = 0.01*av_dist

left_nodes = []
right_nodes = []
top_nodes = []
bottom_nodes = []

for p in g.nodes(data=True):
    px, py = p[1]['para']
    if np.isclose(px, xmin, atol=tol):
        left_nodes.append(p[0])
        g.node[p[0]]['boundary'] = 'left'
    elif np.isclose(px, xmax, atol=tol):
        right_nodes.append(p[0])
        g.node[p[0]]['boundary'] = 'right'

    if np.isclose(py, ymin, atol=tol):
        bottom_nodes.append(p[0])
        g.node[p[0]]['boundary'] = 'bottom'
    elif np.isclose(py, ymax, atol=tol):
        top_nodes.append(p[0])
        g.node[p[0]]['boundary'] = 'top'

# compute the position of the boundary nodes        
P = tiling._para
bottom_points = np.dot(P, np.asarray(bottom_nodes).T)
left_points = np.dot(P, np.asarray(left_nodes).T)

# add the extra nodes on the other boundaries
A, B = lattice(n)
right_nodes = np.asarray(left_nodes) + A
top_nodes = np.asarray(bottom_nodes) + B
rp = np.dot(P, right_nodes.T)
tp = np.dot(P, top_nodes.T)
        
pos = nx.get_node_attributes(g, 'para')
nx.draw_networkx(g, pos, width=2., node_size=0., with_labels=False)
boundary_nodes = [30*int('boundary' in g.node[p]) for p in g]
nx.draw_networkx(g, pos, node_size = boundary_nodes, with_labels=False)
#plt.plot(*bottom_points, 'o')
#plt.plot(*left_points, 'o')
#
#plt.plot(*rp, 'o')
#plt.plot(*tp, 'o')