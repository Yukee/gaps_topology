# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 16:47:04 2016

@author: nicolas

The class system for creating and manipulating 2D tilings
"""
CAIRO = False # for compatibility issues cairo may need to be deactivated
if CAIRO:
    import cairo
import math
import networkx as nx # graph tools
import numpy as np
import matplotlib.pyplot as plt

import QuantumGraph as QGraph

# round (or not) the node positions before passing them as keys to nx. Note that rounding changes their type from complex to np.complex128
ROUND = True
DEC = 15 # number of decimals to keep after rounding

class Tiling:
    """ generate quasiperiodic tilings of the plane """
    
    # set of tiles used as basic building blocks of the tiling
    _prototiles = []
    
    def __init__(self, type, init_shapes = None):
        self._type = type
        
        # default basis vectors
        self._basis = np.identity(4)
        
        # the shapes forming the seed of the qp tiling
        self._init_shapes = init_shapes
        # the inflated shapes forming a bit of the qp tiling
        self._shapes = init_shapes
        # the graph associated to the shapes
        self._graph = None
        # the current number of inflations, starting from the _init_shapes
        self._ninf = 0
        
        if CAIRO:
            # default cairo surface
            self._surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 100, 100)
            # default cairo context
            self._cr = cairo.Context(self._surface)
        
    # substitution rules
    def __substitute__(self, shapes):
        raise NotImplementedError('Cannot implement the __substitute__ method for the general Tilings class')
        
    # iterate substitution starting from the seed _init_shapes
    def it_sub(self, nit):
        it_shapes = self._init_shapes
        for i in range(nit):
            it_shapes = self.__substitute__(it_shapes)
        # comment previous and uncomment following to construct from the previous shapes
#        it_shapes = self.__substitute__(self._shapes)
        # update the current shapes
        self._shapes = it_shapes
        # update the number of inflations
        self._ninf = nit
        # update the current graph
        self.__construct_graph__()
    
    # the projection matrices
    def perp_proj(self):
        raise NotImplementedError('Cannot implement the perp_proj method for the general Tilings class')
    
    def para_proj(self):
        raise NotImplementedError('Cannot implement the para_proj method for the general Tilings class')
    
    
    def __create_surface__(self, IMAGE_SIZE, scaling, origin, filename):
        """
        create a Cairo surface of resolution IMAGE_SIZE, and covering a square of half-length scaling centered at origin
        """        
    
        width_in_points, height_in_points = IMAGE_SIZE
        self._surface = cairo.SVGSurface(filename + ".svg", width_in_points, height_in_points)
        self._cr = cairo.Context(self._surface)
        
        # invert the y axis and place the origin at the lower left corner of the image
        self._cr.scale(1., -1.)
        self._cr.translate(0., -IMAGE_SIZE[1])
    
        self._cr.translate(origin[0]/scaling*IMAGE_SIZE[0], origin[1]/scaling*IMAGE_SIZE[1])
        wheelRadius = (1./scaling) * math.sqrt((IMAGE_SIZE[0] / 2.0) ** 2 + (IMAGE_SIZE[1] / 2.0) ** 2)
        self._cr.scale(wheelRadius, wheelRadius)
        
    def save_image(self, filename = None):
        """
        Finish the Cairo surface (it can no longer be modified by Cairo afterwards).
        Save the image to png if a filename is provided. In any case, it is also saved as svg.
        """
        if filename:
            self._surface.write_to_png(filename+".png")
        self._surface.finish()
        
    def recursive_integrate_arrow_field(self, start, arrow_field = "arrow", copy = True):
        """
        Return a graph with weighted nodes, whose value is the integral of the specified arrow field.
        This recursive version is a bit faster than the one below, but suffers from Python's recursion depth limit.
        """
            
        self.__construct_arrowed_graph__()
        if copy:
            # deep copy of the graph
            wg = nx.Graph(self._graph)
        else:
            # shallow copy
            wg = self._graph
        # for the moment, weights are unset
        nx.set_node_attributes(wg, values='unset', name='weight')
        
        # points of the tiling that are already weighted
        weightedpts = set()
        
        def get_weight(p, n):
            # retrieve weight of node p
            wp = wg.node[p]['weight']
            # fix for netwokx >= 2.0
            return wp + self._arrowed_graph.edges[p, n][arrow_field]
        # lift non lifted neighbours of points of the list pts
        def weight(pts):
            # new weighted points
            newpts = []
            for p in pts:
                # list non weighted neighbours of p
                neighs = set(wg.neighbors(p)) - weightedpts
                # weight neighbour n
                for n in neighs:
                    wg.node[n]['weight'] = get_weight(p,n)
                    weightedpts.add(n)
                    newpts.append(n)

            if len(newpts) > 0:
                return weight(newpts)

        # set the origin in 4D to an arbitrary point of d
        wg.node[start]['weight'] = 0
        weightedpts.add(start)

        weight([start])

        return wg
    
    def integrate_arrow_field(self, start, arrow_field = "arrow", copy = True):
        """
        Return a graph with weighted nodes (nonrecursive version), whose value is the integral of the specified arrow field
        """
            
        self.__construct_arrowed_graph__()
        if copy:
            # deep copy of the graph
            wg = nx.Graph(self._graph)
        else:
            # shallow copy
            wg = self._graph
            
        def get_weight(n, p):
            """
            Return the weight of point n knowing that of point p
            """
            # retrieve weight of node p
            wp = wg.node[p]['weight']
            # fix for networkx >= 2.0
            return wp + self._arrowed_graph.edges[p, n][arrow_field]

        # a dict from weightable node one of their neighbors already weighted
        weightable = dict()
        # a set of nodes already weighted
        weighted = set()
        # affect weight 0 to start node
        wg.node[start]['weight'] = 0
        weighted.add(start)
        # add the neighbors of the start nodes to the weightable dict
        weightable.update({n:start for n in wg.neighbors(start)})
        
        while len(weightable) > 0:
            # pick an arbitrary point that can be weighted
            n = next(iter(weightable))
            # remove n from weightable and get weighted node p
            p = weightable.pop(n)
            # weight n
            wg.node[n]["weight"] = get_weight(n, p)
            # add p to weighted
            weighted.add(p)
            # neighbors of n that are not weighted yet
            toweight = {nn:n for nn in wg.neighbors(n) if nn not in weighted}
            # add them to weightable
            weightable.update(toweight)
            
        return wg
    
    def __construct_graph__(self):
        raise NotImplementedError('Cannot implement the __construct_graph__ method for the general Tilings class')
            
    def __construct_arrowed_graph__(self):
        raise NotImplementedError('Cannot implement the __construct_arrowed_graph__ method for the general Tilings class')
        
    # plot the graph associated to the current shapes
    def plot_graph(self, proj='para'):
        # check the graph exists
        if self._graph == "undefined": 
            raise KeyError('No graph is yet constructed. Use the it_sub method to build one.')
        
        dict_pos = nx.get_node_attributes(self._graph, proj)
        # drawing!
        nx.draw_networkx_edges(self._graph, dict_pos)
        plt.axes().set_aspect('equal')
        plt.show()
            
    def plot_field(self, ptToField, proj='para', node_size = 2):
        # check the graph exists
        if self._graph == "undefined": 
            raise KeyError('No graph is yet constructed. Use the it_sub method to build one.')
        
        dict_pos = nx.get_node_attributes(self._graph, proj) 
        # drawing!
        field = [ptToField[pt] for pt in self._graph]
        nx.draw_networkx(self._graph, dict_pos, node_color = field, with_labels = False, node_size = node_size)
        plt.axes().set_aspect('equal')
        plt.show()
        
    def __append_to_edges__(self, shape):
        """
        Add the edges of shape to the current Cairo path
        """
        raise NotImplementedError('Cannot implement the __append_to_edges__ method for the general Tilings class')
        
    def __draw_edges__(self):
        """
        Draw the edges of the current _shapes, using Cairo
        """
        # This linewidth is fine if lengthes of the edges are set to 1
        self._cr.set_line_width(0.05)
        # For Penrose, determine linewidth from size of first triangle
        if self._type == 'Penrose_P3_Tiling':
            color, vertices = self._shapes[0]
            self._cr.set_line_width(abs(vertices[1] - vertices[0]) / 20.0)
        self._cr.set_line_join(cairo.LINE_JOIN_ROUND)

        for shape in self._shapes:
            self.__append_to_edges__(shape)
        self._cr.set_source_rgb(0.2, 0.2, 0.2)
        self._cr.stroke()
        
    def __draw_faces__(self):
        """
        Draw the faces of the current _shapes, using Cairo
        """
        raise NotImplementedError('Cannot implement the __draw_faces__ method for the general Tilings class')
        
    def __draw_decorations__(self):
        """
        Decorate the faces of the current _shapes, using Cairo
        """
        raise NotImplementedError('Cannot implement the __draw_decorations__ method for the general Tilings class')
        
    def __draw_arrows__(self):
        """
        Decorate the edges of the current _shapes, using Cairo
        """
        raise NotImplementedError('Cannot implement the __draw_arrows__ method for the general Tilings class')
        
    def draw(self, resolution, scaling, origin, filename, **kwargs):
        """
        Draw the current shapes using Cairo
        Remark: to save the image (as png or svg), the save_image method has to be called afer this one.
        """
        # check whether there are shapes to draw
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise
        
        # create the Cairo surface on which to draw the shapes
        self.__create_surface__(resolution, scaling, origin, filename)
        
        self.__draw_faces__()
        self.__draw_edges__()

        if "draw_decorations" in kwargs:
            if kwargs["draw_decorations"]:
                self.__draw_decorations__()
        
        
class A5(Tiling):
    
    def __init__(self, init_shapes = []):
        super().__init__("Ammann_Beenker_A5", init_shapes)
        
        self._basis = np.identity(4, dtype=int)
        
        self._M = np.array([[1,1,0,-1],[1,1,1,0],[0,1,1,1],[-1,0,1,1]])
        
    def para_proj(self):
        """
        Return the projection matrix from Z4 to physical space
        for the approximant n
        """
        n = self._ninf
        if n==0:
            return np.zeros((2,4))
        r = self.o(n-1)/self.o(n)
        sgn = (-1)**n #np.sign(1-2*(n%2))

        para = np.array([[1.+r,1.,0,-1.],[0,1.,1.+r,1.]])
        normpara = np.sqrt(4.+sgn/self.o(n)**2)
        para /= normpara
        return para
                
    def perp_proj(self):
        """
        Return the projection matrix from Z4 to internal space
        for the approximant n
        """
        n = self._ninf
        if n==0:
            return np.zeros((2,4))
        r = self.o(n-1)/self.o(n)
        sgn = (-1)**n #np.sign(1-2*(n%2))
        
        perp = np.array([[2., -(1. + r), 0, 1. + r], [0, 1. + r, -2., 1. + r]])
        normperp = np.sqrt(8.+2*sgn/self.o(n)**2)
        perp /= normperp
        return perp
        
    def para(self):
        """
        Return the projection matrix from Z4 to internal space
        for the infinite system
        """
        sq = np.sqrt(2)
        para = np.array([[sq,1.,0,-1.],[0,1.,sq,1.]])*0.5
        return para
    
    # substitution rules
    def __substitute__(self, shapes):                
        result = []
        for color, vertices in shapes:            
            # lozenge
            if color == 0:
                # old vertices
                A, B, C, D = vertices
                # inflated vertices
                MA, MB, MC, MD = np.dot(self._M, np.asarray(vertices).T).T
                # new vertices
                P  = MA + B-A
                Pp = MC + B-C
                Q = MA + D-A
                Qp = MC + D-C
                R = P+Q-MA
                Rp = Pp+Qp-MC
                
                lozUp = (0, (MA, P, R, Q))
                lozDown = (0, (Rp, Pp, MC, Qp))
                lozMid = (0, (MD, R, MB, Rp))
                sqUR = (1, (R, MD, Q))
                sqUL = (1, (R, MB, P))
                sqDL = (1, (Rp, MB, Pp))
                sqDR = (1, (Rp, MD, Qp))

                result += [lozUp, lozDown, lozMid, sqUL, sqUR, sqDL, sqDR]
            # half-square
            else:
                # old vertices
                A, B, C = vertices
                # inflated vertices
                MA, MB, MC = np.dot(self._M, np.asarray(vertices).T).T
                # new vertices
                P = MB+A-B
                S = MA+C-A
                R = (MC+MB+C-B)/2
                Q = R-C+B
                T = Q+A-B
                
                sqU = (1, (T, MA, P))
                sqDL = (1, (T, R, Q))
                sqDR = (1, (R, MC, S))
                lozU = (0, (MA, T, R, S))
                lozD = (0, (T, P, MB, Q))

                result += [lozU, lozD, sqU, sqDL, sqDR]
        return result
    
    def __append_to_edges__(self, shape):
        color, vertices = shape
        
        # parallel projection
        P = self.para()
        # lozenges
        if color == 0:
            A, B, C, D = np.dot(P, np.transpose(vertices)).T
#            self._cr.move_to(A.real, A.imag)
#            self._cr.line_to(B.real, B.imag)
#            self._cr.line_to(C.real, C.imag)
#            self._cr.line_to(D.real, D.imag)
            self._cr.move_to(A[0], A[1])
            self._cr.line_to(B[0], B[1])
            self._cr.line_to(C[0], C[1])
            self._cr.line_to(D[0], D[1])
            self._cr.close_path()
                        
        # half squares      
        if color == 1:
            A, B, C = np.dot(P, np.transpose(vertices)).T
            self._cr.move_to(C[0], C[1])
            self._cr.line_to(A[0], A[1])
            self._cr.line_to(B[0], B[1])
            
    def __draw_faces__(self):
        # transparency
        
        # parallel projection
        P = self.para()

        # Fill the lozenges
        for color, vertices in self._shapes:
            if color == 0:
                A, B, C, D = np.dot(P, np.transpose(vertices)).T
                self._cr.move_to(A[0], A[1])
                self._cr.line_to(B[0], B[1])
                self._cr.line_to(C[0], C[1])
                self._cr.line_to(D[0], D[1])
                self._cr.close_path()
                
        # colors for the thesis
        BostonBlue = np.array([0., 104., 139.])/255
        comp = np.array([200., 35., 0.])/255
	# actually changing them (the names no longer describe the color)
        comp = np.array([0., 104., 139.])/255
        BostonBlue = np.array([0., 0., 0., 0.])
        # colors good for the poster
        red = np.array([139., 35., 0., 255.])/255.
        green = np.array([33., 139., 0., 255.])/255.
               
        self._cr.set_source_rgba(*BostonBlue)
        self._cr.fill()
        
        # Fill the half-squares
        for color, vertices in self._shapes:
            if color == 1:
                A, B , C = np.dot(P, np.transpose(vertices)).T
                self._cr.move_to(A[0], A[1])
                self._cr.line_to(B[0], B[1])
                self._cr.line_to(C[0], C[1])
                self._cr.close_path()
        self._cr.set_source_rgba(*comp)
        self._cr.fill()
        
    # Octonacci numbers
    def o(self, n):
        a, b = 0, 1
        for i in range(n):
            a, b = b, a + 2*b
        return a
        
    # represent the tiling as a networkx graph
    def __construct_graph__(self):
        # check whether there are shapes to operate on
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise
            
        # projection matrices onto physical and internal spaces
        P = self.para()
        Pi = self.perp_proj()
        # using a networkx undirected graph object
        ug = nx.Graph()
        for color, vertices in self._shapes:
            # convert vertices to tuples            
            vertices = [tuple(v) for v in vertices]
            # compute the para and perp projections of each vertex
            nodes = [(v,dict(para=np.dot(P, v), perp=np.dot(Pi, v))) for v in vertices]
            ug.add_nodes_from(nodes)
            # lozenges
            if color == 0:
                A, B, C, D = vertices
                ug.add_edges_from([(B,A),(D,A),(B,C),(D,C)])
            # half-squares
            else:
                A, B, C = vertices
                ug.add_edges_from([(A,B),(C,A)])

        self._graph = ug
        
    def __construct_arrowed_graph__(self):
        # check whether there is a graph to operate on
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise
            
        # projection matrices onto physical and internal spaces
        P = self.para()
        Pi = self.perp_proj()
        # using a networkx directed graph
        ag = nx.DiGraph()
        for color, vertices in self._shapes:
            # convert vertices to tuples            
            vertices = [tuple(v) for v in vertices]
            # compute the para and perp projections of each vertex
            nodes = [(v,dict(para=np.dot(P, v), perp=np.dot(Pi, v))) for v in vertices]
            ag.add_nodes_from(nodes)
            # lozenges
            if color == 0:
                A, B, C, D = vertices
                ag.add_edges_from([(B,A),(D,A),(B,C),(D,C)], arrow = 1)
                ag.add_edges_from([(A,B),(A,D),(C,B),(C,D)], arrow = -1)
            # half-squares
            else:
                A, B, C = vertices
                ag.add_edges_from([(A,B),(C,A)], arrow = 1)
                ag.add_edges_from([(B,A),(A,C)], arrow = -1)

        self._arrowed_graph = ag
        
    """
    Return the pure-hopping with diagonal jumps graph,
    whose adjacency matrix is the hamiltonian.
    t is the first neighbors hopping amplitude
    delta is the ratio diagonal hopping amplitude neighbors hopping amplitude.
    """
    def PHD_graph(self, t, delta):
        # check whether there is a graph to operate on
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise

        phd_g = self._graph.copy()
        nx.set_edge_attributes(phd_g, values=t, name='weight')
        for color, vertices in self._shapes:
            vertices = [tuple(v) for v in vertices]
            # lozenges            
            if color == 0:
                A, B, C, D = vertices
                phd_g.add_edge(B, D, weight = delta)
                
        return phd_g   

    def lattice(self):
        """
        Lattice vectors of the nth approximant
        """
        a = self.o(self._ninf)
        b = self.o(self._ninf-1)
        return np.array([[a+b,a,0,-a],[0,a,a+b,a]])
    
    def in_unit_cell(self, p):
        """
        Modify the positions p so that it falls in the canonical unit cell of the nth approximant
        """
        # lattice vectors
        A, B = self.lattice()
        # linear length of the unit cell
        L = np.sqrt(np.dot(A,A)) + 0.1
        # projection onto physical space
        pp = np.dot(self.para_proj(), p)
        # number of translations needed in both directions
        nx, ny = np.floor(pp/L)
        
        return p - nx*A - ny*B
        
    def phason_flip(self, graph, axis):
        """
        Update graph to make points along axis jump.
        axis is an integer from 0 to 7, where 0 is the leftmost window edge and we run counterclockwise.
        ninf is the order of the approximant (needed to ensure points stay within the unit cell).
        """
        # unit basis
        e = np.identity(4)
        # return the 4D vectors linking a jumped site to its new neighbors
        neighs = {0:(e[1],-e[0],-e[3]),1:(-e[0],-e[3],e[2]),2:(-e[3],e[2],-e[1]),
                  3:(e[2],-e[1],e[0]),4:(-e[1],e[0],e[3]),5:(e[0],e[3],-e[2]),6:(e[3],-e[2],e[1]),7:(-e[2],e[1],-e[0])}
        
        # projection matrices
        P = self.para()
        Pi = self.perp_proj()

        # check that axis is indeed a number
        if type(axis) is not int:
            raise RuntimeError('axis has not the correct type')
        # edge nodes along wanted axis
        theta = 2.*axis*np.pi/8.
        eds = QGraph.edge_nodes(graph, theta, 'perp')[0]
    
        # return the 4D position given the label
        position = nx.get_node_attributes(graph, 'pos')
        # return the label given the position
        lbl = dict((tuple(v),k) for k,v in position.items())
    
        # 4D vectors linking a jumped site to its new neighbors
        ns = neighs[axis]
        # translation vector from one edge to the opposing one
        a = sum(ns)
        # positions of the jumped nodes
        new_pos = [position[edge_node] + a for edge_node in eds]
        # correct the positions so that they fall in the unit cell
        new_pos = list(map(lambda p: self.in_unit_cell(p), new_pos))
        # positions of the neighbors of the jumped nodes
        new_neighs = [[(en, lbl[tuple(np - v)]) for v in ns if tuple(np - v) in lbl.keys()] for en, np in zip(eds, new_pos)]
    
        # update the graph
        for label, pos, neighs in zip(eds, new_pos, new_neighs):
            # change the position label
            graph.node[label]['pos'] = pos
            # recompute the projections and change them
            graph.node[label]['para'] = np.dot(P, pos)
            graph.node[label]['perp'] = np.dot(Pi, pos)
            # remove the old neighbors
            old_edges = map(lambda n: (label,n), graph.neighbors(label))
            graph.remove_edges_from(old_edges)
            # add the new neighbors
            graph.add_edges_from(neighs)
        
        # return the points that have been fliped
        return eds
    

    def Rmat(self, ax):
        """
        Return the 4D reflexion matrix wrt axis ax
        """
        # return one of the 8 possible unit vectors (4 basis vectors, 2 signs)
        ei = lambda i: (-1)**((i) // 4)*self._basis[(i)%4]
        return np.array([ei(2*ax-i) for i in range(4)]).T

    
    def reflect(self, graph, ax):
        """
        Return a new A5 graph which is the union of the input A5 graph and of its 4D reflection wrt axis ax.
        /!\ When computing the projections, the slope is assumed to be that of the approximant self._ninf
        """
        # reflexion matrix
        R = self.Rmat(ax)
        # reflect nodes
        rnodes = np.dot(R, np.transpose(graph.nodes()))
        # map from (old) nodes to (new) reflected nodes
        oldToNew = dict(zip(graph.nodes(), map(tuple, rnodes.T)))
        # shallow copy of the old graph
        rgraph = nx.Graph(graph)
        # affect the new nodes to the new graph
        nx.relabel_nodes(rgraph, oldToNew, copy=False)
        # recompute the projections
        P = self.para()
        Pi = self.perp_proj()
        rpara = {p:np.dot(P, p) for p in rgraph.nodes()}
        nx.set_node_attributes(rgraph, values=rpara, name='para')
        rperp = {p:np.dot(Pi, p) for p in rgraph.nodes()}
        nx.set_node_attributes(rgraph, values=rperp, name='perp')
        # union of old and new graphs (compose merges identical nodes)
        ugraph = nx.compose(graph, rgraph)
        return ugraph
    
        
class Penrose(Tiling):
    def __init__(self, init_shapes = []):
        super().__init__("Penrose_P3_Tiling", init_shapes)
        
        self._basis = np.identity(5)
        
        self._om = (math.sqrt(5) - 1.) / 2.
        
        # colors of the faces
#        self._lightred = (0.697, 0.425, 0.333)
#        self._lightblue = (0.333, 0.605, 0.697)
        self._lightred = np.array([0., 0., 0., 0.]) # thin rhombuses are transparent
        self._lightblue = np.array([0., 104., 139.])/255 # fat are light blue
        # color of the decorations
        self._lightbrown = (115/255, 90/255, 67/255)
        
        # extra colors (used for Tim's video)
#        self._lightred = np.array([36.1, 67.8, 53.3])/100.
#        self._lightblue =  np.array([98, 64.7, 52.2])/100.
                                   
        # projection matrix from 5D to physical space
        n = np.arange(5)
        self._para = np.array([np.cos(2*n*np.pi/5.), np.sin(2*n*np.pi/5.)])
        # permutation
        perm = np.array([[1,0,0,0,0],[0,0,1,0,0],[0,0,0,0,1],[0,1,0,0,0],[0,0,0,1,0]])
        # projection from 5D to internal
        self._perp = np.dot(self._para, perm)
        
#    def para(self):
#        n = self._ninf
#        tau1 = (self.fib(n+2) + self.fib(n))/(self.fib(n+1) + self.fib(n-1))
#        tau2 = (self.fib(n-1) + self.fib(n-3))/(self.fib(n) + self.fib(n-2))
#        tau3 = 1. + (self.fib(n-1)*self.fib(2*n))/(self.fib(n+1)*self.fib(2*n-1))
#        s = np.sin(2.*np.pi*n/5.)
#        para = np.array([[1., 0.5*(tau1-1.), -0.5*tau1, -0.5*tau1, 0.5*(tau1-1.)], [0., s, (tau2-1.)*s, (1.-tau3)*s, (tau3-tau2-1.)*s]])
#        return para
#    
#    def perp(self):
#        para = self.para()
#        perm = np.array([[1,0,0,0,0],[0,0,1,0,0],[0,0,0,0,1],[0,1,0,0,0],[0,0,0,1,0]])
#        perp = np.dot(para, perm)
#        return perp
#    
#    def perp2(self):
#        b = np.array([[-1, -1, 0, 1, 1], [1, 0, -1, -1, 1]])
#        a = np.array([[0, -1, 0, 1, 0], [1, 0, -1, 0, 0]])
#        for i in range(self._ninf):
#            a, b = b, a + b
#        return a
    
    def fib(self, n):
        a, b = 0, 1
        for i in range(n):
            a, b = b, a + b
        return a

    def __substitute__(self, shapes):
        """
        Penrose substitution rules for the tiles.
        """
        
        result = []
        for color, vertices in shapes:
            # Thin triangle
            if color == 0:
                
                A, B, C = vertices
                
                P = A + (B - A)*self._om

                result += [(0, (C, P, B)), (1, (P, C, A))]
            # Fat triangle
            else:

                A, B, C = vertices

                Q = B + (A - B)*self._om
                R = B + (C - B)*self._om
                
                result += [(1, (R, C, A)), (1, (Q, R, B)), (0, (R, Q, A))]
        return result
        
    def __append_to_edges__(self, shape):
        color, vertices = shape
        A, B, C = vertices
        self._cr.move_to(B.real, B.imag)
        self._cr.line_to(A.real, A.imag)
        self._cr.line_to(C.real, C.imag)
                    
    def __draw_faces__(self):
        # Fill the thin triangles
        for color, vertices in self._shapes:
            if color == 0:
                A, B, C = vertices
                self._cr.move_to(A.real, A.imag)
                self._cr.line_to(B.real, B.imag)
                self._cr.line_to(C.real, C.imag)
                self._cr.close_path()
        self._cr.set_source_rgba(*self._lightred)
        self._cr.fill()
        
        # Fill the fat triangles
        for color, vertices in self._shapes:
            if color == 1:
                A, B , C = vertices
                self._cr.move_to(A.real, A.imag)
                self._cr.line_to(B.real, B.imag)
                self._cr.line_to(C.real, C.imag)
                self._cr.close_path()
        self._cr.set_source_rgba(*self._lightblue)
        self._cr.fill()
        
    def __draw_arrows__(self):
        
        self._cr.arrow()
        
    def __draw_decorations__(self):
        
        def angle(cmplx):
            th = np.angle(cmplx)
            if th < 0:
                th += 2*np.pi
            return th
            
        def inner(z1, z2):
            return z1.real*z2.imag - z2.real*z1.imag
        
        # retrieve the edge_length from an arbitrarily chosen shape of the tiling
        color, vertices = self._shapes[0]
        edge_length = abs(vertices[1] - vertices[0])
        D = 0.8*edge_length # distance 
        d = edge_length - D
        delta = 0.05*edge_length
        
        self._cr.set_line_width(0.03*edge_length)
    
        # Decorate the triangles
        for color, vertices in self._shapes:
            A, B, C = vertices
            
            # double line decoration
            if color == 0:
                r1 = d
                r2 = d+delta
            elif color == 1:
                r1 = D
                r2 = D-delta
            cent = B
            z1 = C-cent
            z2 = A-cent
            th1 = angle(z2)
            th2 = angle(z1)
            if inner(z1,z2)>0:
                self._cr.arc_negative(cent.real, cent.imag, r1, th1, th2)
                self._cr.new_sub_path()
                self._cr.arc_negative(cent.real, cent.imag, r2, th1, th2)
            else:
                self._cr.arc(cent.real, cent.imag, r1, th1, th2)
                self._cr.new_sub_path()
                self._cr.arc(cent.real, cent.imag, r2, th1, th2)
            self._cr.stroke()
            
            # single line decoration
            cent = C
            z1 = B-cent
            z2 = A-cent
            th1 = angle(z1)
            th2 = angle(z2)
            if inner(z1,z2)>0:
                self._cr.arc(cent.real, cent.imag, d, th1, th2)
            else:
                self._cr.arc_negative(cent.real, cent.imag, d, th1, th2)
            self._cr.stroke()
        
    def __construct_graph__(self):
        """
        Construct a NetworkX Graph representing the tiling
        """
        # check whether there are shapes to operate on
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise
            
        # using a networkx undirected graph object
        ug = nx.Graph()
        for color, vertices in self._shapes:
            if ROUND: vertices = np.around(vertices, decimals=DEC)
            A, B, C = vertices
            ug.add_edges_from([(A,B),(A,C)])

        self._graph = ug
        
    def __construct_arrowed_graph__(self):
        """
        Construct a directed NetworkX Graph representing the tiling decorated with simple arrows and double arrows
        """
        # check whether there is a graph to operate on
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise
        
        # using a networkx directed graph
        ag = nx.DiGraph()
        for color, vertices in self._shapes:
            # thin triangles
            if color == 0:
                if ROUND: vertices = np.around(vertices, decimals=DEC)
                A, B, C = vertices
                ag.add_edge(A,C, single_arrow = -1, double_arrow = 0)
                ag.add_edge(C,A, single_arrow = 1, double_arrow = 0)
                ag.add_edge(A,B, single_arrow = 0, double_arrow = -1)
                ag.add_edge(B,A, single_arrow = 0, double_arrow = 1)
            # fat triangles
            else:
                if ROUND: vertices = np.around(vertices, decimals=DEC)
                A, B, C = vertices
                ag.add_edge(A,C, single_arrow = -1, double_arrow = 0)
                ag.add_edge(C,A, single_arrow = 1, double_arrow = 0)
                ag.add_edge(A,B, single_arrow = 0, double_arrow = 1)
                ag.add_edge(B,A, single_arrow = 0, double_arrow = -1)

        self._arrowed_graph = ag
        
    def __get_5D_vec__(self, p1, p2):
        """
        Return the 5D vector connecting the lifts of nodes p1 and p2
        """
        # list of 4D base vectors
        e = self._basis
        # angle between vector p2 - p1 and the horizontal, ranging between -pi and pi
        th = np.angle(p2 - p1)
        # p1 -> p2 or p2 -> p1
        sgn = np.sign(th)
        # angles close to 0 or to +- pi are changed to +- 0
        if np.isclose(th, 0., atol = 1e-05): 
            th = 0.
            sgn = 1
        elif np.isclose(abs(th), np.pi, atol = 1e-09) or np.isclose(abs(th), -np.pi, atol = 1e-09): 
            th = 0.
            sgn = -1
        # transform the angle into an integer between 0 and 4
        coord = int(abs(np.round(th*5/(np.pi))))
        # map each measured edge orientation to the corresponding 5D vector
        to5Dvec = {(1,0):e[0],(1,1):-e[3],(1,2):e[1],(1,3):-e[4],(1,4):e[2],(-1,0):-e[0],(-1,4):e[3],(-1,3):-e[1],(-1,2):e[4],(-1,1):-e[2]}
        return to5Dvec[(sgn,coord)]
        
    def __lift__(self):
        """
        Lift the tiling to 5D space
        """
        # point of the tiling that are already lifted
        liftedpts = set()

        # a dictionary linking pt of the tiling d to their lifts
        to5D = {}

        # lift non lifted neighbours of points of the list pts
        def lift_edges(pts):
            # new lifted points
            newpts = []
            for p in pts:
                # get coords of p in 4D
                p5D = to5D[p]
                # list non lifted neighbours of p
                neighs = set(self._graph[p]) - liftedpts
                # lift neighbour n
                for n in neighs:
                    to5D[n] = p5D + self.__get_5D_vec__(p, n)
                    newpts.append(n)
                    liftedpts.add(n)

            if len(newpts) > 0:
                return lift_edges(newpts)

        # set the origin in 4D to an arbitrary point of d
        pt0 = self._graph.nodes()[0]
        lp0 = np.zeros(5)
        to5D[pt0] = lp0
        liftedpts.add(pt0)
        # lift!
        lift_edges([pt0])
        # write lift data to the graph
        nx.set_node_attributes(self._graph, values=to5D,  name="lift")
        
    def lift(self):
        """
        Return a lift of the tiling in 5D space
        """
        # lift the tiling if not already done
        if "lift" not in self._graph.node[0]:
            self.__lift__()
        return nx.get_node_attributes(self._graph, "lift")
    
    def __perp_index__(self):
        """
        Compute to which perp plane each tiling point belongs
        """
        # lift the tiling if not done yet
        if "lift" not in self._graph.node[0]:
            self.__lift__()
        # compute the plane index for each point
        plane_index = [np.sum(data["lift"]) for p, data in self._graph.nodes(data = True)]
        # set the plane index between 0 and 3
        pmin = np.min(plane_index)
        plane_index = {p:int(perp-pmin) for p, perp in zip(self._graph.nodes(), plane_index)}
        # add the plane index data to the graph
        nx.set_node_attributes(self._graph, values=plane_index, name="perp_index")
        
    def planes(self):
        """
        Return the nodes belonging to one of the 4 planes in internal space
        """
        # affect its index to each point if not done yet
        if "perp_index" not in self._graph.node[0]:
            self.__perp_index__()
        # partition the node list into 4 lists according to plane index
        planes = [[] for n in range(4)]
        for p, data in self._graph.nodes(data = True):
            planes[data["perp_index"]].append(p)
        return planes

                
class Fibonacci(Tiling):
    def __init__(self, init_shapes = []):
        super().__init__("Fibonacci_chain", init_shapes)
        
        self._basis = np.identity(2)

    # golden ratio and its inverse
    _sq5 = math.sqrt(5)
    _tau = (_sq5+1.)/2.
    _om = (_sq5-1.)/2.

    _prototiles = []
    # short tile
    s = math.sqrt(_om/_sq5)
    _prototiles.append((0, (0, s)))

    # long tile
    c = math.sqrt(_tau/_sq5)
    _prototiles.append((1, (0, c)))
    
    # substitution rules
    def __substitute__(self, shapes):
        result = []
        for color, vertices in shapes:
            left, right = vertices # left and right ends of the tile
            # short -> long
            if color == 0:
                result += [(1, vertices)]
            # long -> long + short
            if color == 1:
                newLong = (left, left + (right - left)/(1.+self._om))
                newShort = (newLong[1], right)
                result += [(1, newLong), (0, newShort)]
        return result
        
    # represent the tiling as a networkx graph
    def __construct_graph__(self):
        # check whether there are shapes to operate on
        try: 
            self._shapes[0]
        except IndexError: 
            print('No shapes are yet stored in _shapes')
            raise
            
        # using a networkx undirected graph object
        ug = nx.Graph()
        i = 0 # iterator over the nodes
        ug.add_node(i) # first node
        for color, vertices in self._shapes:
            i += 1 # increment node iterator
            ug.add_node(i) # add next node
            # short
            if color == 0:
                ug.add_edge(i-1, i, weight='S')
            # long
            if color == 1:
                ug.add_edge(i-1, i, weight='L')

        self._graph = ug
        
class FibonacciProduct(Fibonacci):
    def __init__(self, init_shapes = []):
        super().__init__(init_shapes)
        self._type = "Fibonacci_cartesian_product"
                
    def it_sub(self, x_ninf, y_ninf):
        x_fib = Fibonacci(self._init_shapes)        
        x_fib.it_sub(x_ninf)            
        x_graph = x_fib._graph
        y_fib = Fibonacci(self._init_shapes)
        y_fib.it_sub(y_ninf)
        y_graph = y_fib._graph
                
        self._graph = nx.cartesian_product(x_graph, y_graph)
        
class FibonacciLabyrinth(Fibonacci):
    def __init__(self, init_shapes = []):
        super().__init__(init_shapes)
        self._type = "Fibonacci_labyrinth_product"
                
    def it_sub(self, x_ninf, y_ninf):
        x_fib = Fibonacci(self._init_shapes)        
        x_fib.it_sub(x_ninf)            
        x_graph = x_fib._graph
        y_fib = Fibonacci(self._init_shapes)
        y_fib.it_sub(y_ninf)
        y_graph = y_fib._graph
        
        self._graph = nx.tensor_product(x_graph, y_graph)
        # iterate over graph to remove half of the nodes
        white = [n for n in self._graph.nodes() if n[0]*n[1]%2==1]
        self._graph.remove_nodes_from(white)
        

###############################################################################
################    Utility functions #########################################
###############################################################################
if __name__ == "__main__":
    """
    Here we test how much time it take to integrate the arrow field using direct and recursive methods
    """
    import AB_envs as envs
    from timeit import default_timer as timer # speed test
    from scipy.stats import linregress

    a = A5(envs.squareCanonical)
    a.it_sub(4)
    wg = a.integrate_arrow_field((0,0,0,0))
    h = nx.get_node_attributes(wg, "weight")
    wg2 = a.nonrecursive_integrate_arrow_field((0,0,0,0))
    h2 = nx.get_node_attributes(wg2, "weight")
    
    times_recur = []
    times_dir = []
    sizes = []
    for n in range(3, 8):
        a.it_sub(n)
        start = timer()
        a.integrate_arrow_field((0,0,0,0))
        end = timer()
        times_recur.append(end - start)

        start = timer()
        a.nonrecursive_integrate_arrow_field((0,0,0,0))
        end = timer()
        times_dir.append(end - start)
        
        sizes.append(len(a._graph))
        
    logS = np.log(sizes)
    logT = np.log(times_recur)
    logTdir = np.log(times_dir)
    slope, intercept, r_value, p_value, std_err = linregress(logS,logT)
    slope_dir, intercept_dir, r_value, p_value, std_err = linregress(logS,logTdir)

    fig, ax = plt.subplots()
    # plot numerics
    ax.plot(logS, logT, 'o', label=r'recursive')
    ax.plot(logS, logTdir, 'o', label=r'direct')
    # plot fit
    x = np.linspace(min(logS), max(logS), num = 50)
    y = intercept + slope*x
    ax.plot(x, y, '-', label='fit recursive = ' + str(np.around(slope,4)))
    y_dir = intercept_dir + slope_dir*x
    ax.plot(x, y_dir, '-', label='fit direct = ' + str(np.around(slope_dir,4)))

    legend = ax.legend(loc='lower right', shadow=False)
    plt.xlabel(r"$\log(N)$ (log number of sites)")
    plt.ylabel(r"$log(h(N)) $ (log integration time)")
#    plt.legend(("recursive", "direct"))
    plt.title("Time to integrate the arrow field.")
    plt.savefig("heights_construction_time.pdf", dpi=200)
    plt.show()
    plt.close()
