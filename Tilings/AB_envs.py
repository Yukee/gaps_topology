# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 18:05:17 2016

@author: nicolas
"""

import Tilings as tl
import numpy as np


# create a tiling whose basic tile is a square
e = np.identity(4)
def square0(orig, ea, eb):
    """
    create a square with edges along ea and eb
    the arrows of the square are directed by ea and eb
    """
    if not np.isclose(np.vdot(ea, eb).real, 0, atol = 1e-10):
        raise RuntimeError("The specified edges cannot be used to create a square " + str(np.vdot(ea, eb).real))
    
    B = orig
    A = orig + ea
    C = orig + ea + eb
    t1 = (1, (A, B, C))
    
    A = orig + eb
    t2 = (1, (A, B, C))
    
    return [t1, t2]
    
def loz0(orig, ea, eb):
    """ 
    create a lozenge with edges along ei and ej
    """
    A = orig
    B = orig + ea
    C = orig + ea + eb
    D = orig + eb

    return [(0, (A, B, C, D))]


squareCanonical = square0(np.zeros(4),e[0],e[2])

"""
Canonical unit cell doubled in both directions
"""

tiles0 = []
tiles0 += square0(np.zeros(4),e[0],e[2])
tiles0 += square0(e[0],e[0],e[2])
tiles0 += square0(e[0]+e[2],e[0],e[2])
tiles0 += square0(e[2],e[0],e[2])
    
doubledSquaresCanonical = tiles0    
del tiles0

"""
Pavel's unit cell
"""

tiles0 = []
tiles0 += square0(np.zeros(4),e[0],e[2])
# reflexion about e2
tiles0 += square0(2*e[0],-e[0],e[2])
# reflexion about e0 and e2
tiles0 += square0(2*e[0]+2*e[2],-e[0],-e[2])
# reflextion about e2
tiles0 += square0(2*e[2],e[0],-e[2])
    
doubledSquaresPavel = tiles0
del tiles0

"""
eighfold environment
"""

eightfold = []
A = np.zeros(4)

# return one of the 8 possible unit vectors (4 basis vectors, 2 signs)
ei = lambda i: (-1)**((i) // 4)*e[(i)%4]
# create the wheel
for i in range(8):
    ea = ei(i)
    eb = ei(i+1)
    # inner lozenges
    eightfold += loz0(A, ea, eb)
    # squares
    eightfold += square0(ea+eb+ei(i+2), -ei(i), -ei(i+2))
    # outer lozenges
    eightfold += loz0(ea+eb+ei(i+2), ei(i+3), ei(i+4))
    
"""
Canonical unit cell
"""
#system = tl.A5(square0(np.zeros(4),e[0],e[2]))

if __name__ == "__main__":
    import QuantumGraph as QGraph

    n = 1
    system = tl.A5(doubledSquaresPavel)
    system.it_sub(n)
    lb = 1.+np.sqrt(2)
    L = 12.
    QGraph.plot(system._graph, lims=((-L,L),(-L,L)), weights=1.)
