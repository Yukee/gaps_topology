# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 11:51:51 2016

@author: nicolas

Implementation of a very simple algorithm for cut and project.
"""

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

"""
Initialisation
"""

class CP():
    """
    Implementation of a very simple algorithm for cut and project. (virtual class)
    """
    
    def __init__(self, para, perp, basis):
        # projection matrices
        self._perp = perp
        self._para = para
        
        # graph of the tiling
        self._g = nx.Graph()
        
        # basis vectors of the higher dimensional lattice
        self._basis = basis
        self._vecs = np.array(list(basis) + list(-basis))
        
        # list of to-work-on points
        self._todo = []
        
    def in_window(self, p):
        """
        Return True if the perpendicular projection p is in the window
        """
        raise NotImplementedError()
        
    def in_region(self, p):
        """
        Return True if the parallel projection p is in the region
        """
        raise NotImplementedError()
    
    def update(self, p):
        """
        Remove p from the list of to-work-on points.
        Compute the valid neighbors of a valid point p.
        Add the valid neighbors to the graph.
        Add the valid neighbors to the todo list.
        """
        # remove p from the todo list /!\ it is assumed to be the point in last position /!\
        self._todo.pop()
        
        # add the valid neighbors
        for vb in self._vecs:
            # compute a neighbor
            n = p + vb
            # compute its projections
            para = np.dot(self._para, n)
            perp = np.dot(self._perp, n)
            # if the neighbor is valid
            if self.in_region(para) and self.in_window(perp):
                # add it to the todo list
                if tuple(n) not in self._g.nodes():
                    self._todo.append(n)

                # add it to the graph
                self._g.add_edge(tuple(p), tuple(n))
                # add the projection data to the nodes attributes
                self._g.node[tuple(n)]['para'] = para
                self._g.node[tuple(n)]['perp'] = perp
                
    def compute_graph(self, p0):
        """
        Tile the allowed region, return the resulting graph.
        """
        # compute the projections of the initial point
        para = np.dot(self._para, p0)
        perp = np.dot(self._perp, p0)
        if not (self.in_region(para) and self.in_window(perp)):
            raise KeyError("The initial point {0} is not in the window or in the region".format(p0)) 
        self._todo.append(p0)
        while self._todo:
            self.update(self._todo[-1])
            
        return self._g

class Rauzy(CP):
    """
    Rauzy cut and project tiling.
    """
    
    def __init__(self):
    
        self._alpha = 1.8392867552141612
        self._alpha2 = self._alpha**2
        self._alpha3 = self._alpha**3
        
        # projection matrices    
        perp = np.array([self._alpha2,self._alpha,1.])
        para = np.array([[0,-1.,self._alpha], [-1.-self._alpha2,self._alpha3,self._alpha2]])
        
        basis = np.identity(3, dtype=int)
        
        super().__init__(para, perp, basis) # add here the arguments to initialise the parent class

        
    def in_window(self, p):
        """
        Return True if the perpendicular projection p is in the window
        """
        return 0 <= p < self._alpha3
        
    def in_region(self, p):
        """
        Return True if the parallel projection p is in the region
        """
        L = 100.
        return 0 <= p[0] < L and 0 <= p[1] < L
        
        
class AB(CP):
    """
    Ammann-Beenker cut and project tiling.
    By default the physical region is a square of size L.
    """
    
    def __init__(self):
    
        self._sq = np.sqrt(2.)
        
        # projection matrices    
        perp = 0.5*np.array([[self._sq, -1, 0, 1], [0, 1, -self._sq, 1]])

        para = 0.5*np.array([[self._sq, 1, 0, -1], [0, 1, self._sq, 1]])
        
        basis = np.identity(4, dtype=int)
        
        self._L = 20. # by default the physical region is a square of size L
        
        super().__init__(para, perp, basis) # add here the arguments to initialise the parent class

        
    def in_rect(self, L, p):
        return -L/2 <= p[0] < L/2 and -L/2 <= p[1] < L/2
        
    def in_window(self, p):
        """
        Return True if the perpendicular projection p is in the window
        """
        prot = np.array([p[0]/self._sq - p[1]/self._sq,p[0]/self._sq + p[1]/self._sq])
        w = (2+np.sqrt(2))/2
        return self.in_rect(w, p) and self.in_rect(w, prot)
        
    def in_region(self, p):
        """
        Return True if the parallel projection p is in the region
        """
        return -self._L/2 <= p[0] < self._L/2 and -self._L/2 <= p[1] < self._L/2
        
class ABP(CP):
    """
    Periodic approximant to the Ammann-Beenker cut and project tiling.
    """
    
    def __init__(self, n, shift):
        """
        n is the number of the approximant
        """
        self._s = shift # a translation vector of the window
    
        self._sq = np.sqrt(2.)
        
        # projection matrices    
        r = self.o(n-1)/self.o(n)
        sgn = (-1)**n #np.sign(1-2*(n%2))
        
        perp = np.array([[2., -(1. + r), 0, 1. + r], [0, 1. + r, -2., 1. + r]])
        normperp = np.sqrt(8.+2*sgn/self.o(n)**2)
        perp /= normperp
        
#        para = np.array([[1.+r,1.,0,-1.],[0,1.,1.+r,1.]])
#        normpara = np.sqrt(4.+sgn/self.o(n)**2)
#        para /= normpara
        sq = np.sqrt(2)
        para = np.array([[sq,1.,0,-1.],[0,1.,sq,1.]])*0.5
        
        basis = np.identity(4, dtype=int)
        
        super().__init__(para, perp, basis) # add here the arguments to initialise the parent class
        
        self._L1 = 2*self.o(n+1)/self.norm(n) # horiz and vert bounds
        self._L2 = np.sqrt(2)*(self.o(n)+self.o(n+1))/self.norm(n) # diagonal bounds
        
    def o(self, n):
        a, b = 0, 1
        for i in range(n):
            a, b = b, a + 2*b
        return a
        
    def norm(self, n):
        """
        return the norm of the perpendicular space generating vectors (e_x' and e_y' in octology)
        """
        return np.sqrt(8.*self.o(n)**2+2.*(-1)**n)
        
    def in_rect(self, L, p):
        return -L/2. <= p[0] < L/2. and -L/2. <= p[1] < L/2.
        
    def in_window(self, p):
        """
        Return True if the perpendicular projection p is in the window
        """
        ps = p - self._s
        prot = np.array([ps[0]/self._sq - ps[1]/self._sq,ps[0]/self._sq + ps[1]/self._sq])
        #return abs(p[0]) < self._Lx/2 and abs(p[1]) < self._Lx/2 and abs(p[0]+p[1]) < self._Ly/self._sq and abs(p[0]-p[1]) < self._Ly/self._sq
        return self.in_rect(self._L1, ps) and self.in_rect(self._L2, prot)
        
    def in_region(self, p):
        """
        Return True if the parallel projection p is in the region
        """
        L = 10.
        return -L/2 <= p[0] < L/2 and -L/2 <= p[1] < L/2

        
""" 
Test section
"""

if __name__ == '__main__':
#    r = ABP(4)
    
    def heart_region(p):
        x, y = p/15.
        return (x**2 + y**2 - 1)**3 - x**2*y**3 <= 0
    
    r = AB()
    r.in_region = heart_region
    g = r.compute_graph(np.array([0,0,0,0]))
    
    proj = r._para
    
    def proj_graph(graph):
        """ 
        project the graph onto the perp space
        """
        pg = g.copy()
        for p in graph.nodes():
            pp = np.dot(proj, p)
            pg.node[p]['proj'] = pp
            
        return pg
        
    def plot(pgraph, lims = None):
        """
        plot a projected graph
        """
        dict_pos = nx.get_node_attributes(pgraph, 'proj')
        plt.axes().set_aspect('equal')
        
        if lims:
            plt.xlim(lims[0])
            plt.ylim(lims[1])
        
        nx.draw_networkx(pgraph, dict_pos, with_labels = False, node_size= 0)      
        plt.savefig("cp.pdf")
    
    pg = proj_graph(g)
    plot(pg, ((-20,20), (-16,20)))  
    